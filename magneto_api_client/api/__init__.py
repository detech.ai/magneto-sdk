# flake8: noqa

# import apis into api package
from magneto_api_client.api.agents_api import AgentsApi
from magneto_api_client.api.audit_log_api import AuditLogApi
from magneto_api_client.api.blueprints_api import BlueprintsApi
from magneto_api_client.api.entities_api import EntitiesApi
from magneto_api_client.api.flow_runs_api import FlowRunsApi
from magneto_api_client.api.flows_api import FlowsApi
from magneto_api_client.api.health_check_api import HealthCheckApi
from magneto_api_client.api.legacy_plugins_api import LegacyPluginsApi
from magneto_api_client.api.legacy_users_api import LegacyUsersApi
from magneto_api_client.api.model_query_api import ModelQueryApi
from magneto_api_client.api.organizations_api import OrganizationsApi
from magneto_api_client.api.scorecards_api import ScorecardsApi
from magneto_api_client.api.suggestions_api import SuggestionsApi
from magneto_api_client.api.tasks_api import TasksApi
from magneto_api_client.api.time_series_api import TimeSeriesApi

