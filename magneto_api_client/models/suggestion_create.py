# coding: utf-8

"""
    Magneto API

    Magneto API is the RESTful API for the Rely.io platform.

    The version of the OpenAPI document: v0.0.1
    Contact: contact@rely.io
    Generated by OpenAPI Generator (https://openapi-generator.tech)

    Do not edit the class manually.
"""  # noqa: E501


from __future__ import annotations
import pprint
import re  # noqa: F401
import json

from datetime import datetime
from pydantic import BaseModel, ConfigDict, Field, StrictBool, StrictStr, field_validator
from typing import Any, ClassVar, Dict, List, Optional
from typing_extensions import Annotated
from magneto_api_client.models.origin_type import OriginType
from magneto_api_client.models.source_type import SourceType
from magneto_api_client.models.target_type import TargetType
from typing import Optional, Set
from typing_extensions import Self

class SuggestionCreate(BaseModel):
    """
    Suggestion Create Model.
    """ # noqa: E501
    id: Optional[Annotated[str, Field(strict=True)]] = None
    title: Annotated[str, Field(min_length=1, strict=True)]
    description: Optional[StrictStr] = None
    origin_type: OriginType = Field(alias="originType")
    origin_id: StrictStr = Field(alias="originId")
    source_type: SourceType = Field(alias="sourceType")
    source_id: StrictStr = Field(alias="sourceId")
    source_context: Optional[Dict[str, Any]] = Field(default=None, alias="sourceContext")
    target_type: TargetType = Field(alias="targetType")
    target_id: StrictStr = Field(alias="targetId")
    target_context: Optional[Dict[str, Any]] = Field(default=None, alias="targetContext")
    target_id_override: Optional[StrictStr] = Field(default=None, alias="targetIdOverride")
    data: Optional[Dict[str, Any]] = None
    ignored: Optional[StrictBool] = False
    auto_apply: Optional[StrictBool] = Field(default=False, alias="autoApply")
    last_applied_at: Optional[datetime] = Field(default=None, alias="lastAppliedAt")
    additional_properties: Dict[str, Any] = {}
    __properties: ClassVar[List[str]] = ["id", "title", "description", "originType", "originId", "sourceType", "sourceId", "sourceContext", "targetType", "targetId", "targetContext", "targetIdOverride", "data", "ignored", "autoApply", "lastAppliedAt"]

    @field_validator('id')
    def id_validate_regular_expression(cls, value):
        """Validates the regular expression"""
        if value is None:
            return value

        if not re.match(r"^([a-z0-9_.-]+)$", value):
            raise ValueError(r"must validate the regular expression /^([a-z0-9_.-]+)$/")
        return value

    model_config = ConfigDict(
        populate_by_name=True,
        validate_assignment=True,
        protected_namespaces=(),
    )


    def to_str(self) -> str:
        """Returns the string representation of the model using alias"""
        return pprint.pformat(self.model_dump(by_alias=True))

    def to_json(self) -> str:
        """Returns the JSON representation of the model using alias"""
        # TODO: pydantic v2: use .model_dump_json(by_alias=True, exclude_unset=True) instead
        return json.dumps(self.to_dict())

    @classmethod
    def from_json(cls, json_str: str) -> Optional[Self]:
        """Create an instance of SuggestionCreate from a JSON string"""
        return cls.from_dict(json.loads(json_str))

    def to_dict(self) -> Dict[str, Any]:
        """Return the dictionary representation of the model using alias.

        This has the following differences from calling pydantic's
        `self.model_dump(by_alias=True)`:

        * `None` is only added to the output dict for nullable fields that
          were set at model initialization. Other fields with value `None`
          are ignored.
        * Fields in `self.additional_properties` are added to the output dict.
        """
        excluded_fields: Set[str] = set([
            "additional_properties",
        ])

        _dict = self.model_dump(
            by_alias=True,
            exclude=excluded_fields,
            exclude_none=True,
        )
        # puts key-value pairs in additional_properties in the top level
        if self.additional_properties is not None:
            for _key, _value in self.additional_properties.items():
                _dict[_key] = _value

        return _dict

    @classmethod
    def from_dict(cls, obj: Optional[Dict[str, Any]]) -> Optional[Self]:
        """Create an instance of SuggestionCreate from a dict"""
        if obj is None:
            return None

        if not isinstance(obj, dict):
            return cls.model_validate(obj)

        _obj = cls.model_validate({
            "id": obj.get("id"),
            "title": obj.get("title"),
            "description": obj.get("description"),
            "originType": obj.get("originType"),
            "originId": obj.get("originId"),
            "sourceType": obj.get("sourceType"),
            "sourceId": obj.get("sourceId"),
            "sourceContext": obj.get("sourceContext"),
            "targetType": obj.get("targetType"),
            "targetId": obj.get("targetId"),
            "targetContext": obj.get("targetContext"),
            "targetIdOverride": obj.get("targetIdOverride"),
            "data": obj.get("data"),
            "ignored": obj.get("ignored") if obj.get("ignored") is not None else False,
            "autoApply": obj.get("autoApply") if obj.get("autoApply") is not None else False,
            "lastAppliedAt": obj.get("lastAppliedAt")
        })
        # store additional fields in additional_properties
        for _key in obj.keys():
            if _key not in cls.__properties:
                _obj.additional_properties[_key] = obj.get(_key)

        return _obj


