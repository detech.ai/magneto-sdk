# coding: utf-8

"""
    Magneto API

    Magneto API is the RESTful API for the Rely.io platform.

    The version of the OpenAPI document: v0.0.1
    Contact: contact@rely.io
    Generated by OpenAPI Generator (https://openapi-generator.tech)

    Do not edit the class manually.
"""  # noqa: E501


from __future__ import annotations
import pprint
import re  # noqa: F401
import json

from datetime import datetime
from pydantic import BaseModel, ConfigDict, Field, StrictBool, StrictInt, StrictStr, field_validator
from typing import Any, ClassVar, Dict, List, Optional
from typing_extensions import Annotated
from magneto_api_client.models.entity_relation import EntityRelation
from magneto_api_client.models.entity_source import EntitySource
from typing import Optional, Set
from typing_extensions import Self

class EntityRead(BaseModel):
    """
    Entity Read Model.
    """ # noqa: E501
    id: Annotated[str, Field(strict=True)]
    blueprint_id: Annotated[str, Field(strict=True)] = Field(alias="blueprintId")
    title: Annotated[str, Field(min_length=1, strict=True)]
    description: Optional[StrictStr] = None
    is_active: Optional[StrictBool] = Field(default=True, alias="isActive")
    is_hidden: Optional[StrictBool] = Field(default=False, alias="isHidden")
    properties: Optional[Dict[str, Any]] = None
    relations: Optional[Dict[str, EntityRelation]] = None
    sources: Optional[Dict[str, EntitySource]] = None
    created_at: datetime = Field(alias="createdAt")
    created_by: Optional[StrictInt] = Field(default=None, alias="createdBy")
    updated_at: datetime = Field(alias="updatedAt")
    updated_by: Optional[StrictInt] = Field(default=None, alias="updatedBy")
    __properties: ClassVar[List[str]] = ["id", "blueprintId", "title", "description", "isActive", "isHidden", "properties", "relations", "sources", "createdAt", "createdBy", "updatedAt", "updatedBy"]

    @field_validator('id')
    def id_validate_regular_expression(cls, value):
        """Validates the regular expression"""
        if not re.match(r"^([a-z0-9_.-]+)$", value):
            raise ValueError(r"must validate the regular expression /^([a-z0-9_.-]+)$/")
        return value

    @field_validator('blueprint_id')
    def blueprint_id_validate_regular_expression(cls, value):
        """Validates the regular expression"""
        if not re.match(r"^([a-z0-9_.-]+)$", value):
            raise ValueError(r"must validate the regular expression /^([a-z0-9_.-]+)$/")
        return value

    model_config = ConfigDict(
        populate_by_name=True,
        validate_assignment=True,
        protected_namespaces=(),
    )


    def to_str(self) -> str:
        """Returns the string representation of the model using alias"""
        return pprint.pformat(self.model_dump(by_alias=True))

    def to_json(self) -> str:
        """Returns the JSON representation of the model using alias"""
        # TODO: pydantic v2: use .model_dump_json(by_alias=True, exclude_unset=True) instead
        return json.dumps(self.to_dict())

    @classmethod
    def from_json(cls, json_str: str) -> Optional[Self]:
        """Create an instance of EntityRead from a JSON string"""
        return cls.from_dict(json.loads(json_str))

    def to_dict(self) -> Dict[str, Any]:
        """Return the dictionary representation of the model using alias.

        This has the following differences from calling pydantic's
        `self.model_dump(by_alias=True)`:

        * `None` is only added to the output dict for nullable fields that
          were set at model initialization. Other fields with value `None`
          are ignored.
        """
        excluded_fields: Set[str] = set([
        ])

        _dict = self.model_dump(
            by_alias=True,
            exclude=excluded_fields,
            exclude_none=True,
        )
        # override the default output from pydantic by calling `to_dict()` of each value in relations (dict)
        _field_dict = {}
        if self.relations:
            for _key_relations in self.relations:
                if self.relations[_key_relations]:
                    _field_dict[_key_relations] = self.relations[_key_relations].to_dict()
            _dict['relations'] = _field_dict
        # override the default output from pydantic by calling `to_dict()` of each value in sources (dict)
        _field_dict = {}
        if self.sources:
            for _key_sources in self.sources:
                if self.sources[_key_sources]:
                    _field_dict[_key_sources] = self.sources[_key_sources].to_dict()
            _dict['sources'] = _field_dict
        return _dict

    @classmethod
    def from_dict(cls, obj: Optional[Dict[str, Any]]) -> Optional[Self]:
        """Create an instance of EntityRead from a dict"""
        if obj is None:
            return None

        if not isinstance(obj, dict):
            return cls.model_validate(obj)

        _obj = cls.model_validate({
            "id": obj.get("id"),
            "blueprintId": obj.get("blueprintId"),
            "title": obj.get("title"),
            "description": obj.get("description"),
            "isActive": obj.get("isActive") if obj.get("isActive") is not None else True,
            "isHidden": obj.get("isHidden") if obj.get("isHidden") is not None else False,
            "properties": obj.get("properties"),
            "relations": dict(
                (_k, EntityRelation.from_dict(_v))
                for _k, _v in obj["relations"].items()
            )
            if obj.get("relations") is not None
            else None,
            "sources": dict(
                (_k, EntitySource.from_dict(_v))
                for _k, _v in obj["sources"].items()
            )
            if obj.get("sources") is not None
            else None,
            "createdAt": obj.get("createdAt"),
            "createdBy": obj.get("createdBy"),
            "updatedAt": obj.get("updatedAt"),
            "updatedBy": obj.get("updatedBy")
        })
        return _obj


