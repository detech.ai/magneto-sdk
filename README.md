# magneto-api-client
Magneto API is the RESTful API for the Rely.io platform.

This Python package is automatically generated by the [OpenAPI Generator](https://openapi-generator.tech) project:

- API version: v0.0.1
- Package version: 1.0.0
- Generator version: 7.12.0
- Build package: org.openapitools.codegen.languages.PythonClientCodegen

## Requirements.

Python 3.8+

## Installation & Usage
### pip install

If the python package is hosted on a repository, you can install directly using:

```sh
pip install git+https://github.com/GIT_USER_ID/GIT_REPO_ID.git
```
(you may need to run `pip` with root permission: `sudo pip install git+https://github.com/GIT_USER_ID/GIT_REPO_ID.git`)

Then import the package:
```python
import magneto_api_client
```

### Setuptools

Install via [Setuptools](http://pypi.python.org/pypi/setuptools).

```sh
python setup.py install --user
```
(or `sudo python setup.py install` to install the package for all users)

Then import the package:
```python
import magneto_api_client
```

### Tests

Execute `pytest` to run the tests.

## Getting Started

Please follow the [installation procedure](#installation--usage) and then run the following:

```python

import magneto_api_client
from magneto_api_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = magneto_api_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: bearerAuth
configuration = magneto_api_client.Configuration(
    access_token = os.environ["BEARER_TOKEN"]
)


# Enter a context with an instance of the API client
async with magneto_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = magneto_api_client.AgentsApi(api_client)
    type_ = magneto_api_client.AgentType() # AgentType |  (optional)
    status = magneto_api_client.AgentStatus() # AgentStatus |  (optional)
    cursor = 'cursor_example' # str |  (optional)
    size = 50 # int |  (optional) (default to 50)
    include_total = False # bool |  (optional) (default to False)

    try:
        # Get Agents
        api_response = await api_instance.get_agents_api_v1_agents_get(type_=type_, status=status, cursor=cursor, size=size, include_total=include_total)
        print("The response of AgentsApi->get_agents_api_v1_agents_get:\n")
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling AgentsApi->get_agents_api_v1_agents_get: %s\n" % e)

```

## Documentation for API Endpoints

All URIs are relative to *http://localhost*

Class | Method | HTTP request | Description
------------ | ------------- | ------------- | -------------
*AgentsApi* | [**get_agents_api_v1_agents_get**](docs/AgentsApi.md#get_agents_api_v1_agents_get) | **GET** /api/v1/agents | Get Agents
*AgentsApi* | [**register_agent_api_v1_agents_post**](docs/AgentsApi.md#register_agent_api_v1_agents_post) | **POST** /api/v1/agents | Register Agent
*AgentsApi* | [**update_agent_heartbeat_api_v1_agents_id_heartbeat_post**](docs/AgentsApi.md#update_agent_heartbeat_api_v1_agents_id_heartbeat_post) | **POST** /api/v1/agents/{id_}/heartbeat | Update Agent Heartbeat
*AuditLogApi* | [**create_audit_log_entry_api_v1_audit_log_post**](docs/AuditLogApi.md#create_audit_log_entry_api_v1_audit_log_post) | **POST** /api/v1/audit-log | Create Audit Log Entry
*AuditLogApi* | [**get_audit_log_api_v1_audit_log_get**](docs/AuditLogApi.md#get_audit_log_api_v1_audit_log_get) | **GET** /api/v1/audit-log | Get Audit Log
*AuditLogApi* | [**get_audit_log_changes_api_v1_audit_log_sid_changes_get**](docs/AuditLogApi.md#get_audit_log_changes_api_v1_audit_log_sid_changes_get) | **GET** /api/v1/audit-log/{sid}/changes | Get Audit Log Changes
*BlueprintsApi* | [**create_blueprint_api_v1_blueprints_post**](docs/BlueprintsApi.md#create_blueprint_api_v1_blueprints_post) | **POST** /api/v1/blueprints | Create Blueprint
*BlueprintsApi* | [**create_blueprint_calculation_properties_api_v1_blueprints_id_calculation_properties_post**](docs/BlueprintsApi.md#create_blueprint_calculation_properties_api_v1_blueprints_id_calculation_properties_post) | **POST** /api/v1/blueprints/{id_}/calculationProperties | Create Blueprint Calculation Properties
*BlueprintsApi* | [**create_blueprint_reference_properties_api_v1_blueprints_id_reference_properties_post**](docs/BlueprintsApi.md#create_blueprint_reference_properties_api_v1_blueprints_id_reference_properties_post) | **POST** /api/v1/blueprints/{id_}/referenceProperties | Create Blueprint Reference Properties
*BlueprintsApi* | [**create_blueprint_relations_api_v1_blueprints_id_relations_post**](docs/BlueprintsApi.md#create_blueprint_relations_api_v1_blueprints_id_relations_post) | **POST** /api/v1/blueprints/{id_}/relations | Create Blueprint Relations
*BlueprintsApi* | [**create_blueprint_schema_properties_api_v1_blueprints_id_schema_properties_post**](docs/BlueprintsApi.md#create_blueprint_schema_properties_api_v1_blueprints_id_schema_properties_post) | **POST** /api/v1/blueprints/{id_}/schemaProperties | Create Blueprint Schema Properties
*BlueprintsApi* | [**create_or_update_blueprint_api_v1_blueprints_put**](docs/BlueprintsApi.md#create_or_update_blueprint_api_v1_blueprints_put) | **PUT** /api/v1/blueprints | Create Or Update Blueprint
*BlueprintsApi* | [**create_or_update_blueprints_bulk_api_v1_blueprints_bulk_put**](docs/BlueprintsApi.md#create_or_update_blueprints_bulk_api_v1_blueprints_bulk_put) | **PUT** /api/v1/blueprints/bulk | Create Or Update Blueprints Bulk
*BlueprintsApi* | [**delete_blueprint_api_v1_blueprints_id_delete**](docs/BlueprintsApi.md#delete_blueprint_api_v1_blueprints_id_delete) | **DELETE** /api/v1/blueprints/{id_} | Delete Blueprint
*BlueprintsApi* | [**delete_blueprint_calculation_property_api_v1_blueprints_id_calculation_properties_calculation_property_key_delete**](docs/BlueprintsApi.md#delete_blueprint_calculation_property_api_v1_blueprints_id_calculation_properties_calculation_property_key_delete) | **DELETE** /api/v1/blueprints/{id_}/calculationProperties/{calculation_property_key} | Delete Blueprint Calculation Property
*BlueprintsApi* | [**delete_blueprint_reference_property_api_v1_blueprints_id_reference_properties_ref_property_key_delete**](docs/BlueprintsApi.md#delete_blueprint_reference_property_api_v1_blueprints_id_reference_properties_ref_property_key_delete) | **DELETE** /api/v1/blueprints/{id_}/referenceProperties/{ref_property_key} | Delete Blueprint Reference Property
*BlueprintsApi* | [**delete_blueprint_relations_api_v1_blueprints_id_relations_relation_key_delete**](docs/BlueprintsApi.md#delete_blueprint_relations_api_v1_blueprints_id_relations_relation_key_delete) | **DELETE** /api/v1/blueprints/{id_}/relations/{relation_key} | Delete Blueprint Relations
*BlueprintsApi* | [**delete_blueprint_schema_properties_api_v1_blueprints_id_schema_properties_schema_property_key_delete**](docs/BlueprintsApi.md#delete_blueprint_schema_properties_api_v1_blueprints_id_schema_properties_schema_property_key_delete) | **DELETE** /api/v1/blueprints/{id_}/schemaProperties/{schema_property_key} | Delete Blueprint Schema Properties
*BlueprintsApi* | [**get_blueprint_api_v1_blueprints_id_get**](docs/BlueprintsApi.md#get_blueprint_api_v1_blueprints_id_get) | **GET** /api/v1/blueprints/{id_} | Get Blueprint
*BlueprintsApi* | [**get_blueprint_calculation_properties_api_v1_blueprints_id_calculation_properties_get**](docs/BlueprintsApi.md#get_blueprint_calculation_properties_api_v1_blueprints_id_calculation_properties_get) | **GET** /api/v1/blueprints/{id_}/calculationProperties | Get Blueprint Calculation Properties
*BlueprintsApi* | [**get_blueprint_reference_properties_api_v1_blueprints_id_reference_properties_get**](docs/BlueprintsApi.md#get_blueprint_reference_properties_api_v1_blueprints_id_reference_properties_get) | **GET** /api/v1/blueprints/{id_}/referenceProperties | Get Blueprint Reference Properties
*BlueprintsApi* | [**get_blueprint_relations_api_v1_blueprints_id_relations_get**](docs/BlueprintsApi.md#get_blueprint_relations_api_v1_blueprints_id_relations_get) | **GET** /api/v1/blueprints/{id_}/relations | Get Blueprint Relations
*BlueprintsApi* | [**get_blueprint_schema_properties_api_v1_blueprints_id_schema_properties_get**](docs/BlueprintsApi.md#get_blueprint_schema_properties_api_v1_blueprints_id_schema_properties_get) | **GET** /api/v1/blueprints/{id_}/schemaProperties | Get Blueprint Schema Properties
*BlueprintsApi* | [**get_blueprints_api_v1_blueprints_get**](docs/BlueprintsApi.md#get_blueprints_api_v1_blueprints_get) | **GET** /api/v1/blueprints | Get Blueprints
*BlueprintsApi* | [**update_blueprint_api_v1_blueprints_id_put**](docs/BlueprintsApi.md#update_blueprint_api_v1_blueprints_id_put) | **PUT** /api/v1/blueprints/{id_} | Update Blueprint
*BlueprintsApi* | [**update_blueprint_calculation_properties_api_v1_blueprints_id_calculation_properties_put**](docs/BlueprintsApi.md#update_blueprint_calculation_properties_api_v1_blueprints_id_calculation_properties_put) | **PUT** /api/v1/blueprints/{id_}/calculationProperties | Update Blueprint Calculation Properties
*BlueprintsApi* | [**update_blueprint_reference_properties_api_v1_blueprints_id_reference_properties_put**](docs/BlueprintsApi.md#update_blueprint_reference_properties_api_v1_blueprints_id_reference_properties_put) | **PUT** /api/v1/blueprints/{id_}/referenceProperties | Update Blueprint Reference Properties
*BlueprintsApi* | [**update_blueprint_relations_api_v1_blueprints_id_relations_put**](docs/BlueprintsApi.md#update_blueprint_relations_api_v1_blueprints_id_relations_put) | **PUT** /api/v1/blueprints/{id_}/relations | Update Blueprint Relations
*BlueprintsApi* | [**update_blueprint_schema_properties_api_v1_blueprints_id_schema_properties_put**](docs/BlueprintsApi.md#update_blueprint_schema_properties_api_v1_blueprints_id_schema_properties_put) | **PUT** /api/v1/blueprints/{id_}/schemaProperties | Update Blueprint Schema Properties
*EntitiesApi* | [**create_entity_api_v1_entities_post**](docs/EntitiesApi.md#create_entity_api_v1_entities_post) | **POST** /api/v1/entities | Create Entity
*EntitiesApi* | [**create_entity_properties_api_v1_entities_id_properties_post**](docs/EntitiesApi.md#create_entity_properties_api_v1_entities_id_properties_post) | **POST** /api/v1/entities/{id_}/properties | Create Entity Properties
*EntitiesApi* | [**create_or_update_entities_bulk_api_v1_entities_bulk_put**](docs/EntitiesApi.md#create_or_update_entities_bulk_api_v1_entities_bulk_put) | **PUT** /api/v1/entities/bulk | Create Or Update Entities Bulk
*EntitiesApi* | [**delete_entity_api_v1_entities_id_delete**](docs/EntitiesApi.md#delete_entity_api_v1_entities_id_delete) | **DELETE** /api/v1/entities/{id_} | Delete Entity
*EntitiesApi* | [**delete_entity_properties_api_v1_entities_id_properties_property_key_delete**](docs/EntitiesApi.md#delete_entity_properties_api_v1_entities_id_properties_property_key_delete) | **DELETE** /api/v1/entities/{id_}/properties/{property_key} | Delete Entity Properties
*EntitiesApi* | [**evaluate_temporary_calculation_properties_api_v1_entities_id_calculation_properties_test_post**](docs/EntitiesApi.md#evaluate_temporary_calculation_properties_api_v1_entities_id_calculation_properties_test_post) | **POST** /api/v1/entities/{id_}/calculationProperties/test | Evaluate Temporary Calculation Properties
*EntitiesApi* | [**get_aggregated_calculation_property_by_key_api_v1_entities_calculation_properties_get**](docs/EntitiesApi.md#get_aggregated_calculation_property_by_key_api_v1_entities_calculation_properties_get) | **GET** /api/v1/entities/calculationProperties | Get Aggregated Calculation Property By Key
*EntitiesApi* | [**get_entities_api_v1_entities_get**](docs/EntitiesApi.md#get_entities_api_v1_entities_get) | **GET** /api/v1/entities | Get Entities
*EntitiesApi* | [**get_entity_api_v1_entities_id_get**](docs/EntitiesApi.md#get_entity_api_v1_entities_id_get) | **GET** /api/v1/entities/{id_} | Get Entity
*EntitiesApi* | [**get_entity_calculation_properties_api_v1_entities_id_calculation_properties_get**](docs/EntitiesApi.md#get_entity_calculation_properties_api_v1_entities_id_calculation_properties_get) | **GET** /api/v1/entities/{id_}/calculationProperties | Get Entity Calculation Properties
*EntitiesApi* | [**get_entity_calculation_properties_by_key_api_v1_entities_id_calculation_properties_calculation_property_key_get**](docs/EntitiesApi.md#get_entity_calculation_properties_by_key_api_v1_entities_id_calculation_properties_calculation_property_key_get) | **GET** /api/v1/entities/{id_}/calculationProperties/{calculation_property_key} | Get Entity Calculation Properties By Key
*EntitiesApi* | [**get_entity_properties_api_v1_entities_id_properties_get**](docs/EntitiesApi.md#get_entity_properties_api_v1_entities_id_properties_get) | **GET** /api/v1/entities/{id_}/properties | Get Entity Properties
*EntitiesApi* | [**get_entity_property_api_v1_entities_id_properties_property_key_get**](docs/EntitiesApi.md#get_entity_property_api_v1_entities_id_properties_property_key_get) | **GET** /api/v1/entities/{id_}/properties/{property_key} | Get Entity Property
*EntitiesApi* | [**get_relations_api_v1_entities_id_relations_get**](docs/EntitiesApi.md#get_relations_api_v1_entities_id_relations_get) | **GET** /api/v1/entities/{id_}/relations | Get Relations
*EntitiesApi* | [**search_entities_api_v1_entities_search_post**](docs/EntitiesApi.md#search_entities_api_v1_entities_search_post) | **POST** /api/v1/entities/search | Search Entities
*EntitiesApi* | [**update_entity_properties_api_v1_entities_id_properties_put**](docs/EntitiesApi.md#update_entity_properties_api_v1_entities_id_properties_put) | **PUT** /api/v1/entities/{id_}/properties | Update Entity Properties
*EntitiesApi* | [**upsert_entity_api_v1_entities_id_put**](docs/EntitiesApi.md#upsert_entity_api_v1_entities_id_put) | **PUT** /api/v1/entities/{id_} | Upsert Entity
*EntitiesApi* | [**validate_entity_api_v1_entities_validate_post**](docs/EntitiesApi.md#validate_entity_api_v1_entities_validate_post) | **POST** /api/v1/entities/validate | Validate Entity
*FlowRunsApi* | [**approve_flow_run_api_v1_flow_runs_id_approve_post**](docs/FlowRunsApi.md#approve_flow_run_api_v1_flow_runs_id_approve_post) | **POST** /api/v1/flow-runs/{id_}/approve | Approve Flow Run
*FlowRunsApi* | [**create_flow_run_api_v1_flow_runs_post**](docs/FlowRunsApi.md#create_flow_run_api_v1_flow_runs_post) | **POST** /api/v1/flow-runs | Create Flow Run
*FlowRunsApi* | [**delete_flow_run_api_v1_flow_runs_id_delete**](docs/FlowRunsApi.md#delete_flow_run_api_v1_flow_runs_id_delete) | **DELETE** /api/v1/flow-runs/{id_} | Delete Flow Run
*FlowRunsApi* | [**deny_flow_run_api_v1_flow_runs_id_deny_post**](docs/FlowRunsApi.md#deny_flow_run_api_v1_flow_runs_id_deny_post) | **POST** /api/v1/flow-runs/{id_}/deny | Deny Flow Run
*FlowRunsApi* | [**get_flow_run_api_v1_flow_runs_id_get**](docs/FlowRunsApi.md#get_flow_run_api_v1_flow_runs_id_get) | **GET** /api/v1/flow-runs/{id_} | Get Flow Run
*FlowRunsApi* | [**get_flow_runs_api_v1_flow_runs_get**](docs/FlowRunsApi.md#get_flow_runs_api_v1_flow_runs_get) | **GET** /api/v1/flow-runs | Get Flow Runs
*FlowRunsApi* | [**upsert_flow_run_api_v1_flow_runs_id_put**](docs/FlowRunsApi.md#upsert_flow_run_api_v1_flow_runs_id_put) | **PUT** /api/v1/flow-runs/{id_} | Upsert Flow Run
*FlowsApi* | [**create_flow_api_v1_flows_post**](docs/FlowsApi.md#create_flow_api_v1_flows_post) | **POST** /api/v1/flows | Create Flow
*FlowsApi* | [**delete_flow_api_v1_flows_id_delete**](docs/FlowsApi.md#delete_flow_api_v1_flows_id_delete) | **DELETE** /api/v1/flows/{id_} | Delete Flow
*FlowsApi* | [**get_flow_api_v1_flows_id_get**](docs/FlowsApi.md#get_flow_api_v1_flows_id_get) | **GET** /api/v1/flows/{id_} | Get Flow
*FlowsApi* | [**get_flows_api_v1_flows_get**](docs/FlowsApi.md#get_flows_api_v1_flows_get) | **GET** /api/v1/flows | Get Flows
*FlowsApi* | [**upsert_flow_api_v1_flows_id_put**](docs/FlowsApi.md#upsert_flow_api_v1_flows_id_put) | **PUT** /api/v1/flows/{id_} | Upsert Flow
*HealthCheckApi* | [**healthcheck_api_v1_health_get**](docs/HealthCheckApi.md#healthcheck_api_v1_health_get) | **GET** /api/v1/health | Perform a Health Check
*HealthCheckApi* | [**healthcheck_velini_api_v1_health_velini_get**](docs/HealthCheckApi.md#healthcheck_velini_api_v1_health_velini_get) | **GET** /api/v1/health/velini | Perform a Health Check by Velini
*LegacyPluginsApi* | [**create_plugin_api_v1_legacy_plugins_post**](docs/LegacyPluginsApi.md#create_plugin_api_v1_legacy_plugins_post) | **POST** /api/v1/legacy/plugins | Create Plugin
*LegacyPluginsApi* | [**delete_plugin_api_v1_legacy_plugins_id_delete**](docs/LegacyPluginsApi.md#delete_plugin_api_v1_legacy_plugins_id_delete) | **DELETE** /api/v1/legacy/plugins/{id_} | Delete Plugin
*LegacyPluginsApi* | [**get_plugin_api_v1_legacy_plugins_id_get**](docs/LegacyPluginsApi.md#get_plugin_api_v1_legacy_plugins_id_get) | **GET** /api/v1/legacy/plugins/{id_} | Get Plugin
*LegacyPluginsApi* | [**get_plugin_blueprints_api_v1_legacy_plugins_data_source_blueprints_get**](docs/LegacyPluginsApi.md#get_plugin_blueprints_api_v1_legacy_plugins_data_source_blueprints_get) | **GET** /api/v1/legacy/plugins/{data_source}/blueprints | Get Plugin Blueprints
*LegacyPluginsApi* | [**get_plugin_token_api_v1_legacy_plugins_id_token_get**](docs/LegacyPluginsApi.md#get_plugin_token_api_v1_legacy_plugins_id_token_get) | **GET** /api/v1/legacy/plugins/{id_}/token | Get Plugin Token
*LegacyPluginsApi* | [**get_plugins_api_v1_legacy_plugins_get**](docs/LegacyPluginsApi.md#get_plugins_api_v1_legacy_plugins_get) | **GET** /api/v1/legacy/plugins | Get Plugins
*LegacyPluginsApi* | [**update_plugin_api_v1_legacy_plugins_id_put**](docs/LegacyPluginsApi.md#update_plugin_api_v1_legacy_plugins_id_put) | **PUT** /api/v1/legacy/plugins/{id_} | Update Plugin
*LegacyPluginsApi* | [**update_plugin_files_to_check_api_v1_legacy_plugins_id_files_to_check_put**](docs/LegacyPluginsApi.md#update_plugin_files_to_check_api_v1_legacy_plugins_id_files_to_check_put) | **PUT** /api/v1/legacy/plugins/{id_}/files-to-check | Update Plugin Files To Check
*LegacyPluginsApi* | [**validate_query_plugin_api_v1_legacy_plugins_id_validate_query_post**](docs/LegacyPluginsApi.md#validate_query_plugin_api_v1_legacy_plugins_id_validate_query_post) | **POST** /api/v1/legacy/plugins/{id_}/validate-query | Validate Query Plugin
*LegacyUsersApi* | [**create_and_invite_user_api_v1_legacy_users_invite_post**](docs/LegacyUsersApi.md#create_and_invite_user_api_v1_legacy_users_invite_post) | **POST** /api/v1/legacy/users/invite | Create And Invite User
*LegacyUsersApi* | [**create_or_update_self_user_entity_api_v1_legacy_users_me_put**](docs/LegacyUsersApi.md#create_or_update_self_user_entity_api_v1_legacy_users_me_put) | **PUT** /api/v1/legacy/users/me | Create Or Update Self User Entity
*LegacyUsersApi* | [**create_or_update_view_config_api_v1_legacy_users_me_view_configs_put**](docs/LegacyUsersApi.md#create_or_update_view_config_api_v1_legacy_users_me_view_configs_put) | **PUT** /api/v1/legacy/users/me/view-configs | Create Or Update View Config
*LegacyUsersApi* | [**delete_user_api_v1_legacy_users_id_delete**](docs/LegacyUsersApi.md#delete_user_api_v1_legacy_users_id_delete) | **DELETE** /api/v1/legacy/users/{id_} | Delete User
*LegacyUsersApi* | [**get_long_lived_token_api_v1_legacy_users_token_get**](docs/LegacyUsersApi.md#get_long_lived_token_api_v1_legacy_users_token_get) | **GET** /api/v1/legacy/users/token | Get Long Lived Token
*LegacyUsersApi* | [**get_self_user_api_v1_legacy_users_me_get**](docs/LegacyUsersApi.md#get_self_user_api_v1_legacy_users_me_get) | **GET** /api/v1/legacy/users/me | Get Self User
*LegacyUsersApi* | [**get_service_account_token_api_v1_legacy_users_service_accounts_type_get**](docs/LegacyUsersApi.md#get_service_account_token_api_v1_legacy_users_service_accounts_type_get) | **GET** /api/v1/legacy/users/service-accounts/{type_} | Get Service Account Token
*LegacyUsersApi* | [**get_users_api_v1_legacy_users_get**](docs/LegacyUsersApi.md#get_users_api_v1_legacy_users_get) | **GET** /api/v1/legacy/users | Get Users
*LegacyUsersApi* | [**get_view_config_api_v1_legacy_users_me_view_configs_get**](docs/LegacyUsersApi.md#get_view_config_api_v1_legacy_users_me_view_configs_get) | **GET** /api/v1/legacy/users/me/view-configs | Get View Config
*LegacyUsersApi* | [**update_user_api_v1_legacy_users_id_put**](docs/LegacyUsersApi.md#update_user_api_v1_legacy_users_id_put) | **PUT** /api/v1/legacy/users/{id_} | Update User
*ModelQueryApi* | [**execute_query_api_v1_model_query_post**](docs/ModelQueryApi.md#execute_query_api_v1_model_query_post) | **POST** /api/v1/model/query | Execute Query
*OrganizationsApi* | [**create_or_update_view_config_api_v1_organizations_self_view_configs_put**](docs/OrganizationsApi.md#create_or_update_view_config_api_v1_organizations_self_view_configs_put) | **PUT** /api/v1/organizations/self/view-configs | Create Or Update View Config
*OrganizationsApi* | [**create_organization_api_v1_organizations_post**](docs/OrganizationsApi.md#create_organization_api_v1_organizations_post) | **POST** /api/v1/organizations | Create Organization
*OrganizationsApi* | [**delete_organization_me_api_v1_organizations_self_delete**](docs/OrganizationsApi.md#delete_organization_me_api_v1_organizations_self_delete) | **DELETE** /api/v1/organizations/self | Delete Organization Me
*OrganizationsApi* | [**disable_organization_me_api_v1_organizations_self_disable_post**](docs/OrganizationsApi.md#disable_organization_me_api_v1_organizations_self_disable_post) | **POST** /api/v1/organizations/self/disable | Disable Organization Me
*OrganizationsApi* | [**enable_organization_me_api_v1_organizations_self_enable_post**](docs/OrganizationsApi.md#enable_organization_me_api_v1_organizations_self_enable_post) | **POST** /api/v1/organizations/self/enable | Enable Organization Me
*OrganizationsApi* | [**get_organization_me_api_v1_organizations_self_get**](docs/OrganizationsApi.md#get_organization_me_api_v1_organizations_self_get) | **GET** /api/v1/organizations/self | Get Organization Me
*OrganizationsApi* | [**get_view_config_api_v1_organizations_self_view_configs_get**](docs/OrganizationsApi.md#get_view_config_api_v1_organizations_self_view_configs_get) | **GET** /api/v1/organizations/self/view-configs | Get View Config
*ScorecardsApi* | [**create_scorecard_api_v1_scorecards_post**](docs/ScorecardsApi.md#create_scorecard_api_v1_scorecards_post) | **POST** /api/v1/scorecards | Create Scorecard
*ScorecardsApi* | [**delete_scorecard_api_v1_scorecards_id_delete**](docs/ScorecardsApi.md#delete_scorecard_api_v1_scorecards_id_delete) | **DELETE** /api/v1/scorecards/{id_} | Delete Scorecard
*ScorecardsApi* | [**evaluate_scorecard_api_v1_scorecards_id_evaluate_post**](docs/ScorecardsApi.md#evaluate_scorecard_api_v1_scorecards_id_evaluate_post) | **POST** /api/v1/scorecards/{id_}/evaluate | Evaluate Scorecard
*ScorecardsApi* | [**get_scorecard_api_v1_scorecards_id_get**](docs/ScorecardsApi.md#get_scorecard_api_v1_scorecards_id_get) | **GET** /api/v1/scorecards/{id_} | Get Scorecard
*ScorecardsApi* | [**get_scorecard_insights_api_v1_scorecards_id_insights_get**](docs/ScorecardsApi.md#get_scorecard_insights_api_v1_scorecards_id_insights_get) | **GET** /api/v1/scorecards/{id_}/insights | Get Scorecard Insights
*ScorecardsApi* | [**get_scorecard_insights_by_entity_api_v1_scorecards_insights_get**](docs/ScorecardsApi.md#get_scorecard_insights_by_entity_api_v1_scorecards_insights_get) | **GET** /api/v1/scorecards/insights | Get Scorecard Insights By Entity
*ScorecardsApi* | [**get_scorecard_results_api_v1_scorecards_results_get**](docs/ScorecardsApi.md#get_scorecard_results_api_v1_scorecards_results_get) | **GET** /api/v1/scorecards/results | Get Scorecard Results
*ScorecardsApi* | [**get_scorecard_results_by_scorecard_api_v1_scorecards_id_results_get**](docs/ScorecardsApi.md#get_scorecard_results_by_scorecard_api_v1_scorecards_id_results_get) | **GET** /api/v1/scorecards/{id_}/results | Get Scorecard Results By Scorecard
*ScorecardsApi* | [**get_scorecards_api_v1_scorecards_get**](docs/ScorecardsApi.md#get_scorecards_api_v1_scorecards_get) | **GET** /api/v1/scorecards | Get Scorecards
*ScorecardsApi* | [**update_scorecard_result_api_v1_scorecards_results_post**](docs/ScorecardsApi.md#update_scorecard_result_api_v1_scorecards_results_post) | **POST** /api/v1/scorecards/results | Update Scorecard Result
*ScorecardsApi* | [**upsert_scorecard_api_v1_scorecards_id_put**](docs/ScorecardsApi.md#upsert_scorecard_api_v1_scorecards_id_put) | **PUT** /api/v1/scorecards/{id_} | Upsert Scorecard
*SuggestionsApi* | [**apply_suggestion_api_v1_suggestions_id_apply_post**](docs/SuggestionsApi.md#apply_suggestion_api_v1_suggestions_id_apply_post) | **POST** /api/v1/suggestions/{id_}/apply | Apply Suggestion
*SuggestionsApi* | [**apply_suggestions_bulk_api_v1_suggestions_apply_bulk_post**](docs/SuggestionsApi.md#apply_suggestions_bulk_api_v1_suggestions_apply_bulk_post) | **POST** /api/v1/suggestions/apply/bulk | Apply Suggestions Bulk
*SuggestionsApi* | [**create_or_update_suggestions_bulk_api_v1_suggestions_bulk_post**](docs/SuggestionsApi.md#create_or_update_suggestions_bulk_api_v1_suggestions_bulk_post) | **POST** /api/v1/suggestions/bulk | Create Or Update Suggestions Bulk
*SuggestionsApi* | [**create_suggestion_api_v1_suggestions_post**](docs/SuggestionsApi.md#create_suggestion_api_v1_suggestions_post) | **POST** /api/v1/suggestions | Create Suggestion
*SuggestionsApi* | [**delete_suggestion_api_v1_suggestions_id_delete**](docs/SuggestionsApi.md#delete_suggestion_api_v1_suggestions_id_delete) | **DELETE** /api/v1/suggestions/{id_} | Delete Suggestion
*SuggestionsApi* | [**get_suggestion_api_v1_suggestions_id_get**](docs/SuggestionsApi.md#get_suggestion_api_v1_suggestions_id_get) | **GET** /api/v1/suggestions/{id_} | Get Suggestion
*SuggestionsApi* | [**get_suggestions_api_v1_suggestions_get**](docs/SuggestionsApi.md#get_suggestions_api_v1_suggestions_get) | **GET** /api/v1/suggestions | Get Suggestions
*SuggestionsApi* | [**upsert_suggestion_api_v1_suggestions_id_put**](docs/SuggestionsApi.md#upsert_suggestion_api_v1_suggestions_id_put) | **PUT** /api/v1/suggestions/{id_} | Upsert Suggestion
*TasksApi* | [**create_entities_upsert_task_api_v1_tasks_entities_upsert_post**](docs/TasksApi.md#create_entities_upsert_task_api_v1_tasks_entities_upsert_post) | **POST** /api/v1/tasks/entities/upsert | Create Entities Upsert Task
*TasksApi* | [**create_task_api_v1_tasks_post**](docs/TasksApi.md#create_task_api_v1_tasks_post) | **POST** /api/v1/tasks | Create Task
*TasksApi* | [**get_task_by_id_api_v1_tasks_id_get**](docs/TasksApi.md#get_task_by_id_api_v1_tasks_id_get) | **GET** /api/v1/tasks/{id_} | Get Task By Id
*TimeSeriesApi* | [**timeseries_query_api_v1_timeseries_query_post**](docs/TimeSeriesApi.md#timeseries_query_api_v1_timeseries_query_post) | **POST** /api/v1/timeseries/query | Timeseries Query
*TimeSeriesApi* | [**timeseries_write_api_v1_timeseries_write_post**](docs/TimeSeriesApi.md#timeseries_write_api_v1_timeseries_write_post) | **POST** /api/v1/timeseries/write | Timeseries Write


## Documentation For Models

 - [ActionDetails](docs/ActionDetails.md)
 - [ActionPhase](docs/ActionPhase.md)
 - [AgentCreate](docs/AgentCreate.md)
 - [AgentRead](docs/AgentRead.md)
 - [AgentStatus](docs/AgentStatus.md)
 - [AgentType](docs/AgentType.md)
 - [Aggregation](docs/Aggregation.md)
 - [AggregationCalculateBy](docs/AggregationCalculateBy.md)
 - [AggregationFilter](docs/AggregationFilter.md)
 - [AggregationFunction](docs/AggregationFunction.md)
 - [AuditLog](docs/AuditLog.md)
 - [AuditLogCreate](docs/AuditLogCreate.md)
 - [BlueprintCreate](docs/BlueprintCreate.md)
 - [BlueprintOptions](docs/BlueprintOptions.md)
 - [BlueprintPropertiesGroup](docs/BlueprintPropertiesGroup.md)
 - [BlueprintRead](docs/BlueprintRead.md)
 - [BlueprintRelation](docs/BlueprintRelation.md)
 - [BlueprintUpdate](docs/BlueprintUpdate.md)
 - [CalculationProperty](docs/CalculationProperty.md)
 - [CalculationPropertyMethod](docs/CalculationPropertyMethod.md)
 - [CalculationPropertyOutputType](docs/CalculationPropertyOutputType.md)
 - [Condition](docs/Condition.md)
 - [CursorPageAgentRead](docs/CursorPageAgentRead.md)
 - [CursorPageAny](docs/CursorPageAny.md)
 - [CursorPageAuditLog](docs/CursorPageAuditLog.md)
 - [CursorPageBlueprintRead](docs/CursorPageBlueprintRead.md)
 - [CursorPageEntityReadExtended](docs/CursorPageEntityReadExtended.md)
 - [CursorPageFlowRead](docs/CursorPageFlowRead.md)
 - [CursorPageFlowRunRead](docs/CursorPageFlowRunRead.md)
 - [CursorPageScorecardRead](docs/CursorPageScorecardRead.md)
 - [CursorPageScorecardResultRead](docs/CursorPageScorecardResultRead.md)
 - [CursorPageSuggestionRead](docs/CursorPageSuggestionRead.md)
 - [Entity](docs/Entity.md)
 - [EntityCalculationPropertyAllIntervals](docs/EntityCalculationPropertyAllIntervals.md)
 - [EntityCalculationPropertyBase](docs/EntityCalculationPropertyBase.md)
 - [EntityCalculationPropertyRead](docs/EntityCalculationPropertyRead.md)
 - [EntityCreate](docs/EntityCreate.md)
 - [EntityRead](docs/EntityRead.md)
 - [EntityReadExtended](docs/EntityReadExtended.md)
 - [EntityReferenceProperty](docs/EntityReferenceProperty.md)
 - [EntityRelation](docs/EntityRelation.md)
 - [EntityScorecardsInsights](docs/EntityScorecardsInsights.md)
 - [EntitySearchQuery](docs/EntitySearchQuery.md)
 - [EntitySource](docs/EntitySource.md)
 - [EntitySourceType](docs/EntitySourceType.md)
 - [EntityUpdate](docs/EntityUpdate.md)
 - [EntityValidationResponse](docs/EntityValidationResponse.md)
 - [EventAction](docs/EventAction.md)
 - [EventResource](docs/EventResource.md)
 - [FetchStatus](docs/FetchStatus.md)
 - [FilterOp](docs/FilterOp.md)
 - [FlowAction](docs/FlowAction.md)
 - [FlowActionType](docs/FlowActionType.md)
 - [FlowCreate](docs/FlowCreate.md)
 - [FlowOutput](docs/FlowOutput.md)
 - [FlowRead](docs/FlowRead.md)
 - [FlowRunCreate](docs/FlowRunCreate.md)
 - [FlowRunRead](docs/FlowRunRead.md)
 - [FlowRunUpdate](docs/FlowRunUpdate.md)
 - [FlowTrigger](docs/FlowTrigger.md)
 - [FlowTriggerEventData](docs/FlowTriggerEventData.md)
 - [FlowTriggerType](docs/FlowTriggerType.md)
 - [FlowType](docs/FlowType.md)
 - [FlowUpdate](docs/FlowUpdate.md)
 - [HTTPValidationError](docs/HTTPValidationError.md)
 - [Heartbeat](docs/Heartbeat.md)
 - [InsightsMetadata](docs/InsightsMetadata.md)
 - [JsonSchema](docs/JsonSchema.md)
 - [LogEntry](docs/LogEntry.md)
 - [LogEntryLevel](docs/LogEntryLevel.md)
 - [OrganizationCreate](docs/OrganizationCreate.md)
 - [OrganizationRead](docs/OrganizationRead.md)
 - [OrganizationStatus](docs/OrganizationStatus.md)
 - [OrganizationViewConfigCreate](docs/OrganizationViewConfigCreate.md)
 - [OrganizationViewConfigRead](docs/OrganizationViewConfigRead.md)
 - [OriginType](docs/OriginType.md)
 - [PluginCapability](docs/PluginCapability.md)
 - [PluginLegacyCreate](docs/PluginLegacyCreate.md)
 - [PluginLegacyFileToCheck](docs/PluginLegacyFileToCheck.md)
 - [PluginLegacyFilesToCheckUpdate](docs/PluginLegacyFilesToCheckUpdate.md)
 - [PluginLegacyRead](docs/PluginLegacyRead.md)
 - [PluginLegacyUpdate](docs/PluginLegacyUpdate.md)
 - [PluginLegacyValidateQueryInput](docs/PluginLegacyValidateQueryInput.md)
 - [PluginLegacyValidateQueryOutput](docs/PluginLegacyValidateQueryOutput.md)
 - [PluginTokenRead](docs/PluginTokenRead.md)
 - [QueryDataFormat](docs/QueryDataFormat.md)
 - [QueryPayload](docs/QueryPayload.md)
 - [RankEntities](docs/RankEntities.md)
 - [RankInsight](docs/RankInsight.md)
 - [ReferenceProperty](docs/ReferenceProperty.md)
 - [ReferencePropertyCreateOrUpdate](docs/ReferencePropertyCreateOrUpdate.md)
 - [ReferencepropertiesValue](docs/ReferencepropertiesValue.md)
 - [RuleInsight](docs/RuleInsight.md)
 - [RuleResults](docs/RuleResults.md)
 - [RunOutput](docs/RunOutput.md)
 - [SchemaProperties](docs/SchemaProperties.md)
 - [ScorecardCreate](docs/ScorecardCreate.md)
 - [ScorecardInsights](docs/ScorecardInsights.md)
 - [ScorecardInsightsData](docs/ScorecardInsightsData.md)
 - [ScorecardRank](docs/ScorecardRank.md)
 - [ScorecardRead](docs/ScorecardRead.md)
 - [ScorecardResultRead](docs/ScorecardResultRead.md)
 - [ScorecardResultRule](docs/ScorecardResultRule.md)
 - [ScorecardResultUpdate](docs/ScorecardResultUpdate.md)
 - [ScorecardRule](docs/ScorecardRule.md)
 - [ScorecardUpdate](docs/ScorecardUpdate.md)
 - [ServiceAccountType](docs/ServiceAccountType.md)
 - [SourceType](docs/SourceType.md)
 - [StatusPhase](docs/StatusPhase.md)
 - [SuggestionApply](docs/SuggestionApply.md)
 - [SuggestionApplyBulk](docs/SuggestionApplyBulk.md)
 - [SuggestionApplyResult](docs/SuggestionApplyResult.md)
 - [SuggestionCreate](docs/SuggestionCreate.md)
 - [SuggestionRead](docs/SuggestionRead.md)
 - [SuggestionUpdate](docs/SuggestionUpdate.md)
 - [Tag](docs/Tag.md)
 - [TargetType](docs/TargetType.md)
 - [TaskCreate](docs/TaskCreate.md)
 - [TaskData](docs/TaskData.md)
 - [TaskErrorInfo](docs/TaskErrorInfo.md)
 - [TaskRead](docs/TaskRead.md)
 - [TaskStatus](docs/TaskStatus.md)
 - [TaskType](docs/TaskType.md)
 - [TimeInterval](docs/TimeInterval.md)
 - [TimeseriesDatapoint](docs/TimeseriesDatapoint.md)
 - [TimeseriesInnerInner](docs/TimeseriesInnerInner.md)
 - [TimeseriesQuery](docs/TimeseriesQuery.md)
 - [TimeseriesWrite](docs/TimeseriesWrite.md)
 - [Type](docs/Type.md)
 - [UnmappedPropertyValue](docs/UnmappedPropertyValue.md)
 - [UserEntityUpdate](docs/UserEntityUpdate.md)
 - [UserLegacyInviteCreate](docs/UserLegacyInviteCreate.md)
 - [UserLegacyRead](docs/UserLegacyRead.md)
 - [UserLegacyRoleType](docs/UserLegacyRoleType.md)
 - [UserLegacyStatus](docs/UserLegacyStatus.md)
 - [UserLegacyUpdate](docs/UserLegacyUpdate.md)
 - [UserLegacyViewConfigCreate](docs/UserLegacyViewConfigCreate.md)
 - [UserLegacyViewConfigRead](docs/UserLegacyViewConfigRead.md)
 - [UserTokenCreate](docs/UserTokenCreate.md)
 - [ValidationError](docs/ValidationError.md)
 - [ValidationErrorLocInner](docs/ValidationErrorLocInner.md)
 - [Value](docs/Value.md)


<a id="documentation-for-authorization"></a>
## Documentation For Authorization


Authentication schemes defined for the API:
<a id="bearerAuth"></a>
### bearerAuth

- **Type**: Bearer authentication


## Author

contact@rely.io


