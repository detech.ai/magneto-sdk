# magneto_api_client.BlueprintsApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**create_blueprint_api_v1_blueprints_post**](BlueprintsApi.md#create_blueprint_api_v1_blueprints_post) | **POST** /api/v1/blueprints | Create Blueprint
[**create_blueprint_calculation_properties_api_v1_blueprints_id_calculation_properties_post**](BlueprintsApi.md#create_blueprint_calculation_properties_api_v1_blueprints_id_calculation_properties_post) | **POST** /api/v1/blueprints/{id_}/calculationProperties | Create Blueprint Calculation Properties
[**create_blueprint_reference_properties_api_v1_blueprints_id_reference_properties_post**](BlueprintsApi.md#create_blueprint_reference_properties_api_v1_blueprints_id_reference_properties_post) | **POST** /api/v1/blueprints/{id_}/referenceProperties | Create Blueprint Reference Properties
[**create_blueprint_relations_api_v1_blueprints_id_relations_post**](BlueprintsApi.md#create_blueprint_relations_api_v1_blueprints_id_relations_post) | **POST** /api/v1/blueprints/{id_}/relations | Create Blueprint Relations
[**create_blueprint_schema_properties_api_v1_blueprints_id_schema_properties_post**](BlueprintsApi.md#create_blueprint_schema_properties_api_v1_blueprints_id_schema_properties_post) | **POST** /api/v1/blueprints/{id_}/schemaProperties | Create Blueprint Schema Properties
[**create_or_update_blueprint_api_v1_blueprints_put**](BlueprintsApi.md#create_or_update_blueprint_api_v1_blueprints_put) | **PUT** /api/v1/blueprints | Create Or Update Blueprint
[**create_or_update_blueprints_bulk_api_v1_blueprints_bulk_put**](BlueprintsApi.md#create_or_update_blueprints_bulk_api_v1_blueprints_bulk_put) | **PUT** /api/v1/blueprints/bulk | Create Or Update Blueprints Bulk
[**delete_blueprint_api_v1_blueprints_id_delete**](BlueprintsApi.md#delete_blueprint_api_v1_blueprints_id_delete) | **DELETE** /api/v1/blueprints/{id_} | Delete Blueprint
[**delete_blueprint_calculation_property_api_v1_blueprints_id_calculation_properties_calculation_property_key_delete**](BlueprintsApi.md#delete_blueprint_calculation_property_api_v1_blueprints_id_calculation_properties_calculation_property_key_delete) | **DELETE** /api/v1/blueprints/{id_}/calculationProperties/{calculation_property_key} | Delete Blueprint Calculation Property
[**delete_blueprint_reference_property_api_v1_blueprints_id_reference_properties_ref_property_key_delete**](BlueprintsApi.md#delete_blueprint_reference_property_api_v1_blueprints_id_reference_properties_ref_property_key_delete) | **DELETE** /api/v1/blueprints/{id_}/referenceProperties/{ref_property_key} | Delete Blueprint Reference Property
[**delete_blueprint_relations_api_v1_blueprints_id_relations_relation_key_delete**](BlueprintsApi.md#delete_blueprint_relations_api_v1_blueprints_id_relations_relation_key_delete) | **DELETE** /api/v1/blueprints/{id_}/relations/{relation_key} | Delete Blueprint Relations
[**delete_blueprint_schema_properties_api_v1_blueprints_id_schema_properties_schema_property_key_delete**](BlueprintsApi.md#delete_blueprint_schema_properties_api_v1_blueprints_id_schema_properties_schema_property_key_delete) | **DELETE** /api/v1/blueprints/{id_}/schemaProperties/{schema_property_key} | Delete Blueprint Schema Properties
[**get_blueprint_api_v1_blueprints_id_get**](BlueprintsApi.md#get_blueprint_api_v1_blueprints_id_get) | **GET** /api/v1/blueprints/{id_} | Get Blueprint
[**get_blueprint_calculation_properties_api_v1_blueprints_id_calculation_properties_get**](BlueprintsApi.md#get_blueprint_calculation_properties_api_v1_blueprints_id_calculation_properties_get) | **GET** /api/v1/blueprints/{id_}/calculationProperties | Get Blueprint Calculation Properties
[**get_blueprint_reference_properties_api_v1_blueprints_id_reference_properties_get**](BlueprintsApi.md#get_blueprint_reference_properties_api_v1_blueprints_id_reference_properties_get) | **GET** /api/v1/blueprints/{id_}/referenceProperties | Get Blueprint Reference Properties
[**get_blueprint_relations_api_v1_blueprints_id_relations_get**](BlueprintsApi.md#get_blueprint_relations_api_v1_blueprints_id_relations_get) | **GET** /api/v1/blueprints/{id_}/relations | Get Blueprint Relations
[**get_blueprint_schema_properties_api_v1_blueprints_id_schema_properties_get**](BlueprintsApi.md#get_blueprint_schema_properties_api_v1_blueprints_id_schema_properties_get) | **GET** /api/v1/blueprints/{id_}/schemaProperties | Get Blueprint Schema Properties
[**get_blueprints_api_v1_blueprints_get**](BlueprintsApi.md#get_blueprints_api_v1_blueprints_get) | **GET** /api/v1/blueprints | Get Blueprints
[**update_blueprint_api_v1_blueprints_id_put**](BlueprintsApi.md#update_blueprint_api_v1_blueprints_id_put) | **PUT** /api/v1/blueprints/{id_} | Update Blueprint
[**update_blueprint_calculation_properties_api_v1_blueprints_id_calculation_properties_put**](BlueprintsApi.md#update_blueprint_calculation_properties_api_v1_blueprints_id_calculation_properties_put) | **PUT** /api/v1/blueprints/{id_}/calculationProperties | Update Blueprint Calculation Properties
[**update_blueprint_reference_properties_api_v1_blueprints_id_reference_properties_put**](BlueprintsApi.md#update_blueprint_reference_properties_api_v1_blueprints_id_reference_properties_put) | **PUT** /api/v1/blueprints/{id_}/referenceProperties | Update Blueprint Reference Properties
[**update_blueprint_relations_api_v1_blueprints_id_relations_put**](BlueprintsApi.md#update_blueprint_relations_api_v1_blueprints_id_relations_put) | **PUT** /api/v1/blueprints/{id_}/relations | Update Blueprint Relations
[**update_blueprint_schema_properties_api_v1_blueprints_id_schema_properties_put**](BlueprintsApi.md#update_blueprint_schema_properties_api_v1_blueprints_id_schema_properties_put) | **PUT** /api/v1/blueprints/{id_}/schemaProperties | Update Blueprint Schema Properties


# **create_blueprint_api_v1_blueprints_post**
> BlueprintRead create_blueprint_api_v1_blueprints_post(blueprint_create, user_editable=user_editable)

Create Blueprint

### Example

* Bearer Authentication (bearerAuth):

```python
import magneto_api_client
from magneto_api_client.models.blueprint_create import BlueprintCreate
from magneto_api_client.models.blueprint_read import BlueprintRead
from magneto_api_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = magneto_api_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: bearerAuth
configuration = magneto_api_client.Configuration(
    access_token = os.environ["BEARER_TOKEN"]
)

# Enter a context with an instance of the API client
async with magneto_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = magneto_api_client.BlueprintsApi(api_client)
    blueprint_create = magneto_api_client.BlueprintCreate() # BlueprintCreate | 
    user_editable = True # bool |  (optional) (default to True)

    try:
        # Create Blueprint
        api_response = await api_instance.create_blueprint_api_v1_blueprints_post(blueprint_create, user_editable=user_editable)
        print("The response of BlueprintsApi->create_blueprint_api_v1_blueprints_post:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling BlueprintsApi->create_blueprint_api_v1_blueprints_post: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **blueprint_create** | [**BlueprintCreate**](BlueprintCreate.md)|  | 
 **user_editable** | **bool**|  | [optional] [default to True]

### Return type

[**BlueprintRead**](BlueprintRead.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**201** | Successful Response |  -  |
**422** | Validation Error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **create_blueprint_calculation_properties_api_v1_blueprints_id_calculation_properties_post**
> Dict[str, CalculationProperty] create_blueprint_calculation_properties_api_v1_blueprints_id_calculation_properties_post(id_, request_body)

Create Blueprint Calculation Properties

### Example

* Bearer Authentication (bearerAuth):

```python
import magneto_api_client
from magneto_api_client.models.calculation_property import CalculationProperty
from magneto_api_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = magneto_api_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: bearerAuth
configuration = magneto_api_client.Configuration(
    access_token = os.environ["BEARER_TOKEN"]
)

# Enter a context with an instance of the API client
async with magneto_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = magneto_api_client.BlueprintsApi(api_client)
    id_ = 'id__example' # str | 
    request_body = {'key': magneto_api_client.CalculationProperty()} # Dict[str, CalculationProperty] | 

    try:
        # Create Blueprint Calculation Properties
        api_response = await api_instance.create_blueprint_calculation_properties_api_v1_blueprints_id_calculation_properties_post(id_, request_body)
        print("The response of BlueprintsApi->create_blueprint_calculation_properties_api_v1_blueprints_id_calculation_properties_post:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling BlueprintsApi->create_blueprint_calculation_properties_api_v1_blueprints_id_calculation_properties_post: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id_** | **str**|  | 
 **request_body** | [**Dict[str, CalculationProperty]**](CalculationProperty.md)|  | 

### Return type

[**Dict[str, CalculationProperty]**](CalculationProperty.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful Response |  -  |
**422** | Validation Error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **create_blueprint_reference_properties_api_v1_blueprints_id_reference_properties_post**
> Dict[str, ReferenceProperty] create_blueprint_reference_properties_api_v1_blueprints_id_reference_properties_post(id_, request_body)

Create Blueprint Reference Properties

### Example

* Bearer Authentication (bearerAuth):

```python
import magneto_api_client
from magneto_api_client.models.reference_property import ReferenceProperty
from magneto_api_client.models.reference_property_create_or_update import ReferencePropertyCreateOrUpdate
from magneto_api_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = magneto_api_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: bearerAuth
configuration = magneto_api_client.Configuration(
    access_token = os.environ["BEARER_TOKEN"]
)

# Enter a context with an instance of the API client
async with magneto_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = magneto_api_client.BlueprintsApi(api_client)
    id_ = 'id__example' # str | 
    request_body = {'key': magneto_api_client.ReferencePropertyCreateOrUpdate()} # Dict[str, ReferencePropertyCreateOrUpdate] | 

    try:
        # Create Blueprint Reference Properties
        api_response = await api_instance.create_blueprint_reference_properties_api_v1_blueprints_id_reference_properties_post(id_, request_body)
        print("The response of BlueprintsApi->create_blueprint_reference_properties_api_v1_blueprints_id_reference_properties_post:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling BlueprintsApi->create_blueprint_reference_properties_api_v1_blueprints_id_reference_properties_post: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id_** | **str**|  | 
 **request_body** | [**Dict[str, ReferencePropertyCreateOrUpdate]**](ReferencePropertyCreateOrUpdate.md)|  | 

### Return type

[**Dict[str, ReferenceProperty]**](ReferenceProperty.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful Response |  -  |
**422** | Validation Error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **create_blueprint_relations_api_v1_blueprints_id_relations_post**
> Dict[str, BlueprintRelation] create_blueprint_relations_api_v1_blueprints_id_relations_post(id_, request_body)

Create Blueprint Relations

### Example

* Bearer Authentication (bearerAuth):

```python
import magneto_api_client
from magneto_api_client.models.blueprint_relation import BlueprintRelation
from magneto_api_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = magneto_api_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: bearerAuth
configuration = magneto_api_client.Configuration(
    access_token = os.environ["BEARER_TOKEN"]
)

# Enter a context with an instance of the API client
async with magneto_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = magneto_api_client.BlueprintsApi(api_client)
    id_ = 'id__example' # str | 
    request_body = {'key': magneto_api_client.BlueprintRelation()} # Dict[str, BlueprintRelation] | 

    try:
        # Create Blueprint Relations
        api_response = await api_instance.create_blueprint_relations_api_v1_blueprints_id_relations_post(id_, request_body)
        print("The response of BlueprintsApi->create_blueprint_relations_api_v1_blueprints_id_relations_post:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling BlueprintsApi->create_blueprint_relations_api_v1_blueprints_id_relations_post: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id_** | **str**|  | 
 **request_body** | [**Dict[str, BlueprintRelation]**](BlueprintRelation.md)|  | 

### Return type

[**Dict[str, BlueprintRelation]**](BlueprintRelation.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful Response |  -  |
**422** | Validation Error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **create_blueprint_schema_properties_api_v1_blueprints_id_schema_properties_post**
> Dict[str, JsonSchema] create_blueprint_schema_properties_api_v1_blueprints_id_schema_properties_post(id_, request_body)

Create Blueprint Schema Properties

### Example

* Bearer Authentication (bearerAuth):

```python
import magneto_api_client
from magneto_api_client.models.json_schema import JsonSchema
from magneto_api_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = magneto_api_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: bearerAuth
configuration = magneto_api_client.Configuration(
    access_token = os.environ["BEARER_TOKEN"]
)

# Enter a context with an instance of the API client
async with magneto_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = magneto_api_client.BlueprintsApi(api_client)
    id_ = 'id__example' # str | 
    request_body = {'key': magneto_api_client.JsonSchema()} # Dict[str, JsonSchema] | 

    try:
        # Create Blueprint Schema Properties
        api_response = await api_instance.create_blueprint_schema_properties_api_v1_blueprints_id_schema_properties_post(id_, request_body)
        print("The response of BlueprintsApi->create_blueprint_schema_properties_api_v1_blueprints_id_schema_properties_post:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling BlueprintsApi->create_blueprint_schema_properties_api_v1_blueprints_id_schema_properties_post: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id_** | **str**|  | 
 **request_body** | [**Dict[str, JsonSchema]**](JsonSchema.md)|  | 

### Return type

[**Dict[str, JsonSchema]**](JsonSchema.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful Response |  -  |
**422** | Validation Error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **create_or_update_blueprint_api_v1_blueprints_put**
> BlueprintRead create_or_update_blueprint_api_v1_blueprints_put(blueprint_create, user_editable=user_editable)

Create Or Update Blueprint

### Example

* Bearer Authentication (bearerAuth):

```python
import magneto_api_client
from magneto_api_client.models.blueprint_create import BlueprintCreate
from magneto_api_client.models.blueprint_read import BlueprintRead
from magneto_api_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = magneto_api_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: bearerAuth
configuration = magneto_api_client.Configuration(
    access_token = os.environ["BEARER_TOKEN"]
)

# Enter a context with an instance of the API client
async with magneto_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = magneto_api_client.BlueprintsApi(api_client)
    blueprint_create = magneto_api_client.BlueprintCreate() # BlueprintCreate | 
    user_editable = True # bool |  (optional) (default to True)

    try:
        # Create Or Update Blueprint
        api_response = await api_instance.create_or_update_blueprint_api_v1_blueprints_put(blueprint_create, user_editable=user_editable)
        print("The response of BlueprintsApi->create_or_update_blueprint_api_v1_blueprints_put:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling BlueprintsApi->create_or_update_blueprint_api_v1_blueprints_put: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **blueprint_create** | [**BlueprintCreate**](BlueprintCreate.md)|  | 
 **user_editable** | **bool**|  | [optional] [default to True]

### Return type

[**BlueprintRead**](BlueprintRead.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**201** | Successful Response |  -  |
**422** | Validation Error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **create_or_update_blueprints_bulk_api_v1_blueprints_bulk_put**
> List[BlueprintRead] create_or_update_blueprints_bulk_api_v1_blueprints_bulk_put(blueprint_create, user_editable=user_editable)

Create Or Update Blueprints Bulk

### Example

* Bearer Authentication (bearerAuth):

```python
import magneto_api_client
from magneto_api_client.models.blueprint_create import BlueprintCreate
from magneto_api_client.models.blueprint_read import BlueprintRead
from magneto_api_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = magneto_api_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: bearerAuth
configuration = magneto_api_client.Configuration(
    access_token = os.environ["BEARER_TOKEN"]
)

# Enter a context with an instance of the API client
async with magneto_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = magneto_api_client.BlueprintsApi(api_client)
    blueprint_create = [magneto_api_client.BlueprintCreate()] # List[BlueprintCreate] | 
    user_editable = True # bool |  (optional) (default to True)

    try:
        # Create Or Update Blueprints Bulk
        api_response = await api_instance.create_or_update_blueprints_bulk_api_v1_blueprints_bulk_put(blueprint_create, user_editable=user_editable)
        print("The response of BlueprintsApi->create_or_update_blueprints_bulk_api_v1_blueprints_bulk_put:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling BlueprintsApi->create_or_update_blueprints_bulk_api_v1_blueprints_bulk_put: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **blueprint_create** | [**List[BlueprintCreate]**](BlueprintCreate.md)|  | 
 **user_editable** | **bool**|  | [optional] [default to True]

### Return type

[**List[BlueprintRead]**](BlueprintRead.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**201** | Successful Response |  -  |
**422** | Validation Error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **delete_blueprint_api_v1_blueprints_id_delete**
> delete_blueprint_api_v1_blueprints_id_delete(id_)

Delete Blueprint

### Example

* Bearer Authentication (bearerAuth):

```python
import magneto_api_client
from magneto_api_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = magneto_api_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: bearerAuth
configuration = magneto_api_client.Configuration(
    access_token = os.environ["BEARER_TOKEN"]
)

# Enter a context with an instance of the API client
async with magneto_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = magneto_api_client.BlueprintsApi(api_client)
    id_ = 'id__example' # str | 

    try:
        # Delete Blueprint
        await api_instance.delete_blueprint_api_v1_blueprints_id_delete(id_)
    except Exception as e:
        print("Exception when calling BlueprintsApi->delete_blueprint_api_v1_blueprints_id_delete: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id_** | **str**|  | 

### Return type

void (empty response body)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**204** | Successful Response |  -  |
**422** | Validation Error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **delete_blueprint_calculation_property_api_v1_blueprints_id_calculation_properties_calculation_property_key_delete**
> delete_blueprint_calculation_property_api_v1_blueprints_id_calculation_properties_calculation_property_key_delete(id_, calculation_property_key)

Delete Blueprint Calculation Property

### Example

* Bearer Authentication (bearerAuth):

```python
import magneto_api_client
from magneto_api_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = magneto_api_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: bearerAuth
configuration = magneto_api_client.Configuration(
    access_token = os.environ["BEARER_TOKEN"]
)

# Enter a context with an instance of the API client
async with magneto_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = magneto_api_client.BlueprintsApi(api_client)
    id_ = 'id__example' # str | 
    calculation_property_key = 'calculation_property_key_example' # str | 

    try:
        # Delete Blueprint Calculation Property
        await api_instance.delete_blueprint_calculation_property_api_v1_blueprints_id_calculation_properties_calculation_property_key_delete(id_, calculation_property_key)
    except Exception as e:
        print("Exception when calling BlueprintsApi->delete_blueprint_calculation_property_api_v1_blueprints_id_calculation_properties_calculation_property_key_delete: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id_** | **str**|  | 
 **calculation_property_key** | **str**|  | 

### Return type

void (empty response body)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**204** | Successful Response |  -  |
**422** | Validation Error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **delete_blueprint_reference_property_api_v1_blueprints_id_reference_properties_ref_property_key_delete**
> delete_blueprint_reference_property_api_v1_blueprints_id_reference_properties_ref_property_key_delete(id_, ref_property_key)

Delete Blueprint Reference Property

### Example

* Bearer Authentication (bearerAuth):

```python
import magneto_api_client
from magneto_api_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = magneto_api_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: bearerAuth
configuration = magneto_api_client.Configuration(
    access_token = os.environ["BEARER_TOKEN"]
)

# Enter a context with an instance of the API client
async with magneto_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = magneto_api_client.BlueprintsApi(api_client)
    id_ = 'id__example' # str | 
    ref_property_key = 'ref_property_key_example' # str | 

    try:
        # Delete Blueprint Reference Property
        await api_instance.delete_blueprint_reference_property_api_v1_blueprints_id_reference_properties_ref_property_key_delete(id_, ref_property_key)
    except Exception as e:
        print("Exception when calling BlueprintsApi->delete_blueprint_reference_property_api_v1_blueprints_id_reference_properties_ref_property_key_delete: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id_** | **str**|  | 
 **ref_property_key** | **str**|  | 

### Return type

void (empty response body)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**204** | Successful Response |  -  |
**422** | Validation Error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **delete_blueprint_relations_api_v1_blueprints_id_relations_relation_key_delete**
> delete_blueprint_relations_api_v1_blueprints_id_relations_relation_key_delete(id_, relation_key)

Delete Blueprint Relations

### Example

* Bearer Authentication (bearerAuth):

```python
import magneto_api_client
from magneto_api_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = magneto_api_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: bearerAuth
configuration = magneto_api_client.Configuration(
    access_token = os.environ["BEARER_TOKEN"]
)

# Enter a context with an instance of the API client
async with magneto_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = magneto_api_client.BlueprintsApi(api_client)
    id_ = 'id__example' # str | 
    relation_key = 'relation_key_example' # str | 

    try:
        # Delete Blueprint Relations
        await api_instance.delete_blueprint_relations_api_v1_blueprints_id_relations_relation_key_delete(id_, relation_key)
    except Exception as e:
        print("Exception when calling BlueprintsApi->delete_blueprint_relations_api_v1_blueprints_id_relations_relation_key_delete: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id_** | **str**|  | 
 **relation_key** | **str**|  | 

### Return type

void (empty response body)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**204** | Successful Response |  -  |
**422** | Validation Error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **delete_blueprint_schema_properties_api_v1_blueprints_id_schema_properties_schema_property_key_delete**
> delete_blueprint_schema_properties_api_v1_blueprints_id_schema_properties_schema_property_key_delete(id_, schema_property_key)

Delete Blueprint Schema Properties

### Example

* Bearer Authentication (bearerAuth):

```python
import magneto_api_client
from magneto_api_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = magneto_api_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: bearerAuth
configuration = magneto_api_client.Configuration(
    access_token = os.environ["BEARER_TOKEN"]
)

# Enter a context with an instance of the API client
async with magneto_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = magneto_api_client.BlueprintsApi(api_client)
    id_ = 'id__example' # str | 
    schema_property_key = 'schema_property_key_example' # str | 

    try:
        # Delete Blueprint Schema Properties
        await api_instance.delete_blueprint_schema_properties_api_v1_blueprints_id_schema_properties_schema_property_key_delete(id_, schema_property_key)
    except Exception as e:
        print("Exception when calling BlueprintsApi->delete_blueprint_schema_properties_api_v1_blueprints_id_schema_properties_schema_property_key_delete: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id_** | **str**|  | 
 **schema_property_key** | **str**|  | 

### Return type

void (empty response body)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**204** | Successful Response |  -  |
**422** | Validation Error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_blueprint_api_v1_blueprints_id_get**
> BlueprintRead get_blueprint_api_v1_blueprints_id_get(id_, include_reference_schema=include_reference_schema)

Get Blueprint

### Example

* Bearer Authentication (bearerAuth):

```python
import magneto_api_client
from magneto_api_client.models.blueprint_read import BlueprintRead
from magneto_api_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = magneto_api_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: bearerAuth
configuration = magneto_api_client.Configuration(
    access_token = os.environ["BEARER_TOKEN"]
)

# Enter a context with an instance of the API client
async with magneto_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = magneto_api_client.BlueprintsApi(api_client)
    id_ = 'id__example' # str | 
    include_reference_schema = False # bool |  (optional) (default to False)

    try:
        # Get Blueprint
        api_response = await api_instance.get_blueprint_api_v1_blueprints_id_get(id_, include_reference_schema=include_reference_schema)
        print("The response of BlueprintsApi->get_blueprint_api_v1_blueprints_id_get:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling BlueprintsApi->get_blueprint_api_v1_blueprints_id_get: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id_** | **str**|  | 
 **include_reference_schema** | **bool**|  | [optional] [default to False]

### Return type

[**BlueprintRead**](BlueprintRead.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful Response |  -  |
**422** | Validation Error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_blueprint_calculation_properties_api_v1_blueprints_id_calculation_properties_get**
> Dict[str, CalculationProperty] get_blueprint_calculation_properties_api_v1_blueprints_id_calculation_properties_get(id_)

Get Blueprint Calculation Properties

### Example

* Bearer Authentication (bearerAuth):

```python
import magneto_api_client
from magneto_api_client.models.calculation_property import CalculationProperty
from magneto_api_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = magneto_api_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: bearerAuth
configuration = magneto_api_client.Configuration(
    access_token = os.environ["BEARER_TOKEN"]
)

# Enter a context with an instance of the API client
async with magneto_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = magneto_api_client.BlueprintsApi(api_client)
    id_ = 'id__example' # str | 

    try:
        # Get Blueprint Calculation Properties
        api_response = await api_instance.get_blueprint_calculation_properties_api_v1_blueprints_id_calculation_properties_get(id_)
        print("The response of BlueprintsApi->get_blueprint_calculation_properties_api_v1_blueprints_id_calculation_properties_get:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling BlueprintsApi->get_blueprint_calculation_properties_api_v1_blueprints_id_calculation_properties_get: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id_** | **str**|  | 

### Return type

[**Dict[str, CalculationProperty]**](CalculationProperty.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful Response |  -  |
**422** | Validation Error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_blueprint_reference_properties_api_v1_blueprints_id_reference_properties_get**
> Dict[str, ReferenceProperty] get_blueprint_reference_properties_api_v1_blueprints_id_reference_properties_get(id_)

Get Blueprint Reference Properties

### Example

* Bearer Authentication (bearerAuth):

```python
import magneto_api_client
from magneto_api_client.models.reference_property import ReferenceProperty
from magneto_api_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = magneto_api_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: bearerAuth
configuration = magneto_api_client.Configuration(
    access_token = os.environ["BEARER_TOKEN"]
)

# Enter a context with an instance of the API client
async with magneto_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = magneto_api_client.BlueprintsApi(api_client)
    id_ = 'id__example' # str | 

    try:
        # Get Blueprint Reference Properties
        api_response = await api_instance.get_blueprint_reference_properties_api_v1_blueprints_id_reference_properties_get(id_)
        print("The response of BlueprintsApi->get_blueprint_reference_properties_api_v1_blueprints_id_reference_properties_get:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling BlueprintsApi->get_blueprint_reference_properties_api_v1_blueprints_id_reference_properties_get: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id_** | **str**|  | 

### Return type

[**Dict[str, ReferenceProperty]**](ReferenceProperty.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful Response |  -  |
**422** | Validation Error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_blueprint_relations_api_v1_blueprints_id_relations_get**
> Dict[str, BlueprintRelation] get_blueprint_relations_api_v1_blueprints_id_relations_get(id_)

Get Blueprint Relations

### Example

* Bearer Authentication (bearerAuth):

```python
import magneto_api_client
from magneto_api_client.models.blueprint_relation import BlueprintRelation
from magneto_api_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = magneto_api_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: bearerAuth
configuration = magneto_api_client.Configuration(
    access_token = os.environ["BEARER_TOKEN"]
)

# Enter a context with an instance of the API client
async with magneto_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = magneto_api_client.BlueprintsApi(api_client)
    id_ = 'id__example' # str | 

    try:
        # Get Blueprint Relations
        api_response = await api_instance.get_blueprint_relations_api_v1_blueprints_id_relations_get(id_)
        print("The response of BlueprintsApi->get_blueprint_relations_api_v1_blueprints_id_relations_get:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling BlueprintsApi->get_blueprint_relations_api_v1_blueprints_id_relations_get: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id_** | **str**|  | 

### Return type

[**Dict[str, BlueprintRelation]**](BlueprintRelation.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful Response |  -  |
**422** | Validation Error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_blueprint_schema_properties_api_v1_blueprints_id_schema_properties_get**
> Dict[str, JsonSchema] get_blueprint_schema_properties_api_v1_blueprints_id_schema_properties_get(id_)

Get Blueprint Schema Properties

### Example

* Bearer Authentication (bearerAuth):

```python
import magneto_api_client
from magneto_api_client.models.json_schema import JsonSchema
from magneto_api_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = magneto_api_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: bearerAuth
configuration = magneto_api_client.Configuration(
    access_token = os.environ["BEARER_TOKEN"]
)

# Enter a context with an instance of the API client
async with magneto_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = magneto_api_client.BlueprintsApi(api_client)
    id_ = 'id__example' # str | 

    try:
        # Get Blueprint Schema Properties
        api_response = await api_instance.get_blueprint_schema_properties_api_v1_blueprints_id_schema_properties_get(id_)
        print("The response of BlueprintsApi->get_blueprint_schema_properties_api_v1_blueprints_id_schema_properties_get:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling BlueprintsApi->get_blueprint_schema_properties_api_v1_blueprints_id_schema_properties_get: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id_** | **str**|  | 

### Return type

[**Dict[str, JsonSchema]**](JsonSchema.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful Response |  -  |
**422** | Validation Error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_blueprints_api_v1_blueprints_get**
> CursorPageBlueprintRead get_blueprints_api_v1_blueprints_get(include_reference_schema=include_reference_schema, exclude_plugin_blueprints=exclude_plugin_blueprints, filter_plugin_blueprints=filter_plugin_blueprints, search_query=search_query, filters=filters, sorts=sorts, cursor=cursor, size=size, include_total=include_total)

Get Blueprints

### Example

* Bearer Authentication (bearerAuth):

```python
import magneto_api_client
from magneto_api_client.models.cursor_page_blueprint_read import CursorPageBlueprintRead
from magneto_api_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = magneto_api_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: bearerAuth
configuration = magneto_api_client.Configuration(
    access_token = os.environ["BEARER_TOKEN"]
)

# Enter a context with an instance of the API client
async with magneto_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = magneto_api_client.BlueprintsApi(api_client)
    include_reference_schema = False # bool |  (optional) (default to False)
    exclude_plugin_blueprints = False # bool |  (optional) (default to False)
    filter_plugin_blueprints = 'filter_plugin_blueprints_example' # str | Filter plugin blueprints by data source (optional)
    search_query = 'search_query_example' # str |  (optional)
    filters = ['filters_example'] # List[str] |  (optional)
    sorts = ['sorts_example'] # List[str] |  (optional)
    cursor = 'cursor_example' # str |  (optional)
    size = 50 # int |  (optional) (default to 50)
    include_total = False # bool |  (optional) (default to False)

    try:
        # Get Blueprints
        api_response = await api_instance.get_blueprints_api_v1_blueprints_get(include_reference_schema=include_reference_schema, exclude_plugin_blueprints=exclude_plugin_blueprints, filter_plugin_blueprints=filter_plugin_blueprints, search_query=search_query, filters=filters, sorts=sorts, cursor=cursor, size=size, include_total=include_total)
        print("The response of BlueprintsApi->get_blueprints_api_v1_blueprints_get:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling BlueprintsApi->get_blueprints_api_v1_blueprints_get: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **include_reference_schema** | **bool**|  | [optional] [default to False]
 **exclude_plugin_blueprints** | **bool**|  | [optional] [default to False]
 **filter_plugin_blueprints** | **str**| Filter plugin blueprints by data source | [optional] 
 **search_query** | **str**|  | [optional] 
 **filters** | [**List[str]**](str.md)|  | [optional] 
 **sorts** | [**List[str]**](str.md)|  | [optional] 
 **cursor** | **str**|  | [optional] 
 **size** | **int**|  | [optional] [default to 50]
 **include_total** | **bool**|  | [optional] [default to False]

### Return type

[**CursorPageBlueprintRead**](CursorPageBlueprintRead.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful Response |  -  |
**422** | Validation Error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **update_blueprint_api_v1_blueprints_id_put**
> BlueprintRead update_blueprint_api_v1_blueprints_id_put(id_, blueprint_update)

Update Blueprint

### Example

* Bearer Authentication (bearerAuth):

```python
import magneto_api_client
from magneto_api_client.models.blueprint_read import BlueprintRead
from magneto_api_client.models.blueprint_update import BlueprintUpdate
from magneto_api_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = magneto_api_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: bearerAuth
configuration = magneto_api_client.Configuration(
    access_token = os.environ["BEARER_TOKEN"]
)

# Enter a context with an instance of the API client
async with magneto_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = magneto_api_client.BlueprintsApi(api_client)
    id_ = 'id__example' # str | 
    blueprint_update = magneto_api_client.BlueprintUpdate() # BlueprintUpdate | 

    try:
        # Update Blueprint
        api_response = await api_instance.update_blueprint_api_v1_blueprints_id_put(id_, blueprint_update)
        print("The response of BlueprintsApi->update_blueprint_api_v1_blueprints_id_put:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling BlueprintsApi->update_blueprint_api_v1_blueprints_id_put: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id_** | **str**|  | 
 **blueprint_update** | [**BlueprintUpdate**](BlueprintUpdate.md)|  | 

### Return type

[**BlueprintRead**](BlueprintRead.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful Response |  -  |
**422** | Validation Error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **update_blueprint_calculation_properties_api_v1_blueprints_id_calculation_properties_put**
> Dict[str, CalculationProperty] update_blueprint_calculation_properties_api_v1_blueprints_id_calculation_properties_put(id_, request_body)

Update Blueprint Calculation Properties

### Example

* Bearer Authentication (bearerAuth):

```python
import magneto_api_client
from magneto_api_client.models.calculation_property import CalculationProperty
from magneto_api_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = magneto_api_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: bearerAuth
configuration = magneto_api_client.Configuration(
    access_token = os.environ["BEARER_TOKEN"]
)

# Enter a context with an instance of the API client
async with magneto_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = magneto_api_client.BlueprintsApi(api_client)
    id_ = 'id__example' # str | 
    request_body = {'key': magneto_api_client.CalculationProperty()} # Dict[str, CalculationProperty] | 

    try:
        # Update Blueprint Calculation Properties
        api_response = await api_instance.update_blueprint_calculation_properties_api_v1_blueprints_id_calculation_properties_put(id_, request_body)
        print("The response of BlueprintsApi->update_blueprint_calculation_properties_api_v1_blueprints_id_calculation_properties_put:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling BlueprintsApi->update_blueprint_calculation_properties_api_v1_blueprints_id_calculation_properties_put: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id_** | **str**|  | 
 **request_body** | [**Dict[str, CalculationProperty]**](CalculationProperty.md)|  | 

### Return type

[**Dict[str, CalculationProperty]**](CalculationProperty.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful Response |  -  |
**422** | Validation Error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **update_blueprint_reference_properties_api_v1_blueprints_id_reference_properties_put**
> Dict[str, ReferenceProperty] update_blueprint_reference_properties_api_v1_blueprints_id_reference_properties_put(id_, request_body)

Update Blueprint Reference Properties

### Example

* Bearer Authentication (bearerAuth):

```python
import magneto_api_client
from magneto_api_client.models.reference_property import ReferenceProperty
from magneto_api_client.models.reference_property_create_or_update import ReferencePropertyCreateOrUpdate
from magneto_api_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = magneto_api_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: bearerAuth
configuration = magneto_api_client.Configuration(
    access_token = os.environ["BEARER_TOKEN"]
)

# Enter a context with an instance of the API client
async with magneto_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = magneto_api_client.BlueprintsApi(api_client)
    id_ = 'id__example' # str | 
    request_body = {'key': magneto_api_client.ReferencePropertyCreateOrUpdate()} # Dict[str, ReferencePropertyCreateOrUpdate] | 

    try:
        # Update Blueprint Reference Properties
        api_response = await api_instance.update_blueprint_reference_properties_api_v1_blueprints_id_reference_properties_put(id_, request_body)
        print("The response of BlueprintsApi->update_blueprint_reference_properties_api_v1_blueprints_id_reference_properties_put:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling BlueprintsApi->update_blueprint_reference_properties_api_v1_blueprints_id_reference_properties_put: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id_** | **str**|  | 
 **request_body** | [**Dict[str, ReferencePropertyCreateOrUpdate]**](ReferencePropertyCreateOrUpdate.md)|  | 

### Return type

[**Dict[str, ReferenceProperty]**](ReferenceProperty.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful Response |  -  |
**422** | Validation Error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **update_blueprint_relations_api_v1_blueprints_id_relations_put**
> Dict[str, BlueprintRelation] update_blueprint_relations_api_v1_blueprints_id_relations_put(id_, request_body)

Update Blueprint Relations

### Example

* Bearer Authentication (bearerAuth):

```python
import magneto_api_client
from magneto_api_client.models.blueprint_relation import BlueprintRelation
from magneto_api_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = magneto_api_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: bearerAuth
configuration = magneto_api_client.Configuration(
    access_token = os.environ["BEARER_TOKEN"]
)

# Enter a context with an instance of the API client
async with magneto_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = magneto_api_client.BlueprintsApi(api_client)
    id_ = 'id__example' # str | 
    request_body = {'key': magneto_api_client.BlueprintRelation()} # Dict[str, BlueprintRelation] | 

    try:
        # Update Blueprint Relations
        api_response = await api_instance.update_blueprint_relations_api_v1_blueprints_id_relations_put(id_, request_body)
        print("The response of BlueprintsApi->update_blueprint_relations_api_v1_blueprints_id_relations_put:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling BlueprintsApi->update_blueprint_relations_api_v1_blueprints_id_relations_put: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id_** | **str**|  | 
 **request_body** | [**Dict[str, BlueprintRelation]**](BlueprintRelation.md)|  | 

### Return type

[**Dict[str, BlueprintRelation]**](BlueprintRelation.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful Response |  -  |
**422** | Validation Error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **update_blueprint_schema_properties_api_v1_blueprints_id_schema_properties_put**
> Dict[str, JsonSchema] update_blueprint_schema_properties_api_v1_blueprints_id_schema_properties_put(id_, request_body)

Update Blueprint Schema Properties

### Example

* Bearer Authentication (bearerAuth):

```python
import magneto_api_client
from magneto_api_client.models.json_schema import JsonSchema
from magneto_api_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = magneto_api_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: bearerAuth
configuration = magneto_api_client.Configuration(
    access_token = os.environ["BEARER_TOKEN"]
)

# Enter a context with an instance of the API client
async with magneto_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = magneto_api_client.BlueprintsApi(api_client)
    id_ = 'id__example' # str | 
    request_body = {'key': magneto_api_client.JsonSchema()} # Dict[str, JsonSchema] | 

    try:
        # Update Blueprint Schema Properties
        api_response = await api_instance.update_blueprint_schema_properties_api_v1_blueprints_id_schema_properties_put(id_, request_body)
        print("The response of BlueprintsApi->update_blueprint_schema_properties_api_v1_blueprints_id_schema_properties_put:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling BlueprintsApi->update_blueprint_schema_properties_api_v1_blueprints_id_schema_properties_put: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id_** | **str**|  | 
 **request_body** | [**Dict[str, JsonSchema]**](JsonSchema.md)|  | 

### Return type

[**Dict[str, JsonSchema]**](JsonSchema.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful Response |  -  |
**422** | Validation Error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

