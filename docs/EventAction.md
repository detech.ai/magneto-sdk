# EventAction

An enumeration.

## Enum

* `CREATE` (value: `'create'`)

* `UPDATE` (value: `'update'`)

* `DELETE` (value: `'delete'`)

* `EVALUATECALCULATIONPROPERTIES` (value: `'evaluateCalculationProperties'`)

* `ADDEDASSOURCE` (value: `'addedAsSource'`)

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


