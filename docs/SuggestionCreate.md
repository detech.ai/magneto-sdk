# SuggestionCreate

Suggestion Create Model.

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** |  | [optional] 
**title** | **str** |  | 
**description** | **str** |  | [optional] 
**origin_type** | [**OriginType**](OriginType.md) |  | 
**origin_id** | **str** |  | 
**source_type** | [**SourceType**](SourceType.md) |  | 
**source_id** | **str** |  | 
**source_context** | **object** |  | [optional] 
**target_type** | [**TargetType**](TargetType.md) |  | 
**target_id** | **str** |  | 
**target_context** | **object** |  | [optional] 
**target_id_override** | **str** |  | [optional] 
**data** | **object** |  | [optional] 
**ignored** | **bool** |  | [optional] [default to False]
**auto_apply** | **bool** |  | [optional] [default to False]
**last_applied_at** | **datetime** |  | [optional] 

## Example

```python
from magneto_api_client.models.suggestion_create import SuggestionCreate

# TODO update the JSON string below
json = "{}"
# create an instance of SuggestionCreate from a JSON string
suggestion_create_instance = SuggestionCreate.from_json(json)
# print the JSON string representation of the object
print(SuggestionCreate.to_json())

# convert the object into a dict
suggestion_create_dict = suggestion_create_instance.to_dict()
# create an instance of SuggestionCreate from a dict
suggestion_create_from_dict = SuggestionCreate.from_dict(suggestion_create_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


