# ScorecardUpdate

Scorecard Update Model.

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** |  | [optional] 
**title** | **str** |  | 
**description** | **str** |  | [optional] 
**is_active** | **bool** |  | [optional] [default to True]
**blueprint_id** | **str** |  | 
**ranks** | [**List[ScorecardRank]**](ScorecardRank.md) |  | [optional] 
**median_rank** | **str** |  | [optional] 

## Example

```python
from magneto_api_client.models.scorecard_update import ScorecardUpdate

# TODO update the JSON string below
json = "{}"
# create an instance of ScorecardUpdate from a JSON string
scorecard_update_instance = ScorecardUpdate.from_json(json)
# print the JSON string representation of the object
print(ScorecardUpdate.to_json())

# convert the object into a dict
scorecard_update_dict = scorecard_update_instance.to_dict()
# create an instance of ScorecardUpdate from a dict
scorecard_update_from_dict = ScorecardUpdate.from_dict(scorecard_update_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


