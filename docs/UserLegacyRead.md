# UserLegacyRead


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **str** |  | 
**email** | **str** |  | 
**id** | **int** |  | 
**last_login** | **datetime** |  | [optional] 
**date_joined** | **datetime** |  | 
**status** | [**UserLegacyStatus**](UserLegacyStatus.md) |  | 
**role** | [**UserLegacyRoleType**](UserLegacyRoleType.md) |  | 

## Example

```python
from magneto_api_client.models.user_legacy_read import UserLegacyRead

# TODO update the JSON string below
json = "{}"
# create an instance of UserLegacyRead from a JSON string
user_legacy_read_instance = UserLegacyRead.from_json(json)
# print the JSON string representation of the object
print(UserLegacyRead.to_json())

# convert the object into a dict
user_legacy_read_dict = user_legacy_read_instance.to_dict()
# create an instance of UserLegacyRead from a dict
user_legacy_read_from_dict = UserLegacyRead.from_dict(user_legacy_read_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


