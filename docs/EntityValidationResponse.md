# EntityValidationResponse

Base class for models that exclude unset fields.  **Note**: This overrides any `exclude_unset` kwarg passed for the model and its nested models.

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**valid** | **bool** |  | 
**errors** | **List[object]** |  | [optional] 

## Example

```python
from magneto_api_client.models.entity_validation_response import EntityValidationResponse

# TODO update the JSON string below
json = "{}"
# create an instance of EntityValidationResponse from a JSON string
entity_validation_response_instance = EntityValidationResponse.from_json(json)
# print the JSON string representation of the object
print(EntityValidationResponse.to_json())

# convert the object into a dict
entity_validation_response_dict = entity_validation_response_instance.to_dict()
# create an instance of EntityValidationResponse from a dict
entity_validation_response_from_dict = EntityValidationResponse.from_dict(entity_validation_response_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


