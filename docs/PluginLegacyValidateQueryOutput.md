# PluginLegacyValidateQueryOutput


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**is_valid** | **bool** |  | 
**errors** | **List[str]** |  | [optional] 
**warnings** | **List[str]** |  | [optional] 

## Example

```python
from magneto_api_client.models.plugin_legacy_validate_query_output import PluginLegacyValidateQueryOutput

# TODO update the JSON string below
json = "{}"
# create an instance of PluginLegacyValidateQueryOutput from a JSON string
plugin_legacy_validate_query_output_instance = PluginLegacyValidateQueryOutput.from_json(json)
# print the JSON string representation of the object
print(PluginLegacyValidateQueryOutput.to_json())

# convert the object into a dict
plugin_legacy_validate_query_output_dict = plugin_legacy_validate_query_output_instance.to_dict()
# create an instance of PluginLegacyValidateQueryOutput from a dict
plugin_legacy_validate_query_output_from_dict = PluginLegacyValidateQueryOutput.from_dict(plugin_legacy_validate_query_output_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


