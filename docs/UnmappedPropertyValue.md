# UnmappedPropertyValue

An enumeration.

## Enum

* `FORBID` (value: `'forbid'`)

* `IGNORE` (value: `'ignore'`)

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


