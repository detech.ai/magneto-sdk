# EntityCreate

Entity Create Model.

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** |  | 
**blueprint_id** | **str** |  | 
**title** | **str** |  | 
**description** | **str** |  | [optional] 
**is_active** | **bool** |  | [optional] [default to True]
**is_hidden** | **bool** |  | [optional] [default to False]
**properties** | **object** |  | [optional] 
**relations** | [**Dict[str, EntityRelation]**](EntityRelation.md) |  | [optional] 
**sources** | [**Dict[str, EntitySource]**](EntitySource.md) |  | [optional] 

## Example

```python
from magneto_api_client.models.entity_create import EntityCreate

# TODO update the JSON string below
json = "{}"
# create an instance of EntityCreate from a JSON string
entity_create_instance = EntityCreate.from_json(json)
# print the JSON string representation of the object
print(EntityCreate.to_json())

# convert the object into a dict
entity_create_dict = entity_create_instance.to_dict()
# create an instance of EntityCreate from a dict
entity_create_from_dict = EntityCreate.from_dict(entity_create_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


