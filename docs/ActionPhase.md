# ActionPhase

An enumeration.

## Enum

* `RUNNING` (value: `'running'`)

* `SUCCEEDED` (value: `'succeeded'`)

* `FAILED` (value: `'failed'`)

* `SKIPPED` (value: `'skipped'`)

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


