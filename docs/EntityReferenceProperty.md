# EntityReferenceProperty


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**entity_id** | **str** |  | [optional] 
**value** | **object** |  | [optional] 

## Example

```python
from magneto_api_client.models.entity_reference_property import EntityReferenceProperty

# TODO update the JSON string below
json = "{}"
# create an instance of EntityReferenceProperty from a JSON string
entity_reference_property_instance = EntityReferenceProperty.from_json(json)
# print the JSON string representation of the object
print(EntityReferenceProperty.to_json())

# convert the object into a dict
entity_reference_property_dict = entity_reference_property_instance.to_dict()
# create an instance of EntityReferenceProperty from a dict
entity_reference_property_from_dict = EntityReferenceProperty.from_dict(entity_reference_property_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


