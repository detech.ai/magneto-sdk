# FlowTriggerType

An enumeration.

## Enum

* `ONEVENT` (value: `'onEvent'`)

* `MANUAL` (value: `'manual'`)

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


