# ScorecardResultUpdate

Scorecard Result Update Model.

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**scorecard_id** | **str** |  | 
**entity_id** | **str** |  | 
**current_rank** | **str** |  | 
**rules** | [**Dict[str, ScorecardResultRule]**](ScorecardResultRule.md) |  | 

## Example

```python
from magneto_api_client.models.scorecard_result_update import ScorecardResultUpdate

# TODO update the JSON string below
json = "{}"
# create an instance of ScorecardResultUpdate from a JSON string
scorecard_result_update_instance = ScorecardResultUpdate.from_json(json)
# print the JSON string representation of the object
print(ScorecardResultUpdate.to_json())

# convert the object into a dict
scorecard_result_update_dict = scorecard_result_update_instance.to_dict()
# create an instance of ScorecardResultUpdate from a dict
scorecard_result_update_from_dict = ScorecardResultUpdate.from_dict(scorecard_result_update_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


