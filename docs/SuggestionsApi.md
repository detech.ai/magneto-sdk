# magneto_api_client.SuggestionsApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**apply_suggestion_api_v1_suggestions_id_apply_post**](SuggestionsApi.md#apply_suggestion_api_v1_suggestions_id_apply_post) | **POST** /api/v1/suggestions/{id_}/apply | Apply Suggestion
[**apply_suggestions_bulk_api_v1_suggestions_apply_bulk_post**](SuggestionsApi.md#apply_suggestions_bulk_api_v1_suggestions_apply_bulk_post) | **POST** /api/v1/suggestions/apply/bulk | Apply Suggestions Bulk
[**create_or_update_suggestions_bulk_api_v1_suggestions_bulk_post**](SuggestionsApi.md#create_or_update_suggestions_bulk_api_v1_suggestions_bulk_post) | **POST** /api/v1/suggestions/bulk | Create Or Update Suggestions Bulk
[**create_suggestion_api_v1_suggestions_post**](SuggestionsApi.md#create_suggestion_api_v1_suggestions_post) | **POST** /api/v1/suggestions | Create Suggestion
[**delete_suggestion_api_v1_suggestions_id_delete**](SuggestionsApi.md#delete_suggestion_api_v1_suggestions_id_delete) | **DELETE** /api/v1/suggestions/{id_} | Delete Suggestion
[**get_suggestion_api_v1_suggestions_id_get**](SuggestionsApi.md#get_suggestion_api_v1_suggestions_id_get) | **GET** /api/v1/suggestions/{id_} | Get Suggestion
[**get_suggestions_api_v1_suggestions_get**](SuggestionsApi.md#get_suggestions_api_v1_suggestions_get) | **GET** /api/v1/suggestions | Get Suggestions
[**upsert_suggestion_api_v1_suggestions_id_put**](SuggestionsApi.md#upsert_suggestion_api_v1_suggestions_id_put) | **PUT** /api/v1/suggestions/{id_} | Upsert Suggestion


# **apply_suggestion_api_v1_suggestions_id_apply_post**
> SuggestionApplyResult apply_suggestion_api_v1_suggestions_id_apply_post(id_, dry_run=dry_run, enable_auto_apply=enable_auto_apply, delete_after_apply=delete_after_apply, suggestion_apply=suggestion_apply)

Apply Suggestion

### Example

* Bearer Authentication (bearerAuth):

```python
import magneto_api_client
from magneto_api_client.models.suggestion_apply import SuggestionApply
from magneto_api_client.models.suggestion_apply_result import SuggestionApplyResult
from magneto_api_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = magneto_api_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: bearerAuth
configuration = magneto_api_client.Configuration(
    access_token = os.environ["BEARER_TOKEN"]
)

# Enter a context with an instance of the API client
async with magneto_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = magneto_api_client.SuggestionsApi(api_client)
    id_ = 'id__example' # str | 
    dry_run = False # bool |  (optional) (default to False)
    enable_auto_apply = True # bool |  (optional) (default to True)
    delete_after_apply = False # bool |  (optional) (default to False)
    suggestion_apply = magneto_api_client.SuggestionApply() # SuggestionApply |  (optional)

    try:
        # Apply Suggestion
        api_response = await api_instance.apply_suggestion_api_v1_suggestions_id_apply_post(id_, dry_run=dry_run, enable_auto_apply=enable_auto_apply, delete_after_apply=delete_after_apply, suggestion_apply=suggestion_apply)
        print("The response of SuggestionsApi->apply_suggestion_api_v1_suggestions_id_apply_post:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling SuggestionsApi->apply_suggestion_api_v1_suggestions_id_apply_post: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id_** | **str**|  | 
 **dry_run** | **bool**|  | [optional] [default to False]
 **enable_auto_apply** | **bool**|  | [optional] [default to True]
 **delete_after_apply** | **bool**|  | [optional] [default to False]
 **suggestion_apply** | [**SuggestionApply**](SuggestionApply.md)|  | [optional] 

### Return type

[**SuggestionApplyResult**](SuggestionApplyResult.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful Response |  -  |
**422** | Validation Error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **apply_suggestions_bulk_api_v1_suggestions_apply_bulk_post**
> List[SuggestionApplyResult] apply_suggestions_bulk_api_v1_suggestions_apply_bulk_post(suggestion_apply_bulk, dry_run=dry_run, enable_auto_apply=enable_auto_apply, delete_after_apply=delete_after_apply)

Apply Suggestions Bulk

### Example

* Bearer Authentication (bearerAuth):

```python
import magneto_api_client
from magneto_api_client.models.suggestion_apply_bulk import SuggestionApplyBulk
from magneto_api_client.models.suggestion_apply_result import SuggestionApplyResult
from magneto_api_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = magneto_api_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: bearerAuth
configuration = magneto_api_client.Configuration(
    access_token = os.environ["BEARER_TOKEN"]
)

# Enter a context with an instance of the API client
async with magneto_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = magneto_api_client.SuggestionsApi(api_client)
    suggestion_apply_bulk = magneto_api_client.SuggestionApplyBulk() # SuggestionApplyBulk | 
    dry_run = False # bool |  (optional) (default to False)
    enable_auto_apply = True # bool |  (optional) (default to True)
    delete_after_apply = False # bool |  (optional) (default to False)

    try:
        # Apply Suggestions Bulk
        api_response = await api_instance.apply_suggestions_bulk_api_v1_suggestions_apply_bulk_post(suggestion_apply_bulk, dry_run=dry_run, enable_auto_apply=enable_auto_apply, delete_after_apply=delete_after_apply)
        print("The response of SuggestionsApi->apply_suggestions_bulk_api_v1_suggestions_apply_bulk_post:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling SuggestionsApi->apply_suggestions_bulk_api_v1_suggestions_apply_bulk_post: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **suggestion_apply_bulk** | [**SuggestionApplyBulk**](SuggestionApplyBulk.md)|  | 
 **dry_run** | **bool**|  | [optional] [default to False]
 **enable_auto_apply** | **bool**|  | [optional] [default to True]
 **delete_after_apply** | **bool**|  | [optional] [default to False]

### Return type

[**List[SuggestionApplyResult]**](SuggestionApplyResult.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful Response |  -  |
**422** | Validation Error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **create_or_update_suggestions_bulk_api_v1_suggestions_bulk_post**
> List[SuggestionRead] create_or_update_suggestions_bulk_api_v1_suggestions_bulk_post(suggestion_create)

Create Or Update Suggestions Bulk

### Example

* Bearer Authentication (bearerAuth):

```python
import magneto_api_client
from magneto_api_client.models.suggestion_create import SuggestionCreate
from magneto_api_client.models.suggestion_read import SuggestionRead
from magneto_api_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = magneto_api_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: bearerAuth
configuration = magneto_api_client.Configuration(
    access_token = os.environ["BEARER_TOKEN"]
)

# Enter a context with an instance of the API client
async with magneto_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = magneto_api_client.SuggestionsApi(api_client)
    suggestion_create = [magneto_api_client.SuggestionCreate()] # List[SuggestionCreate] | 

    try:
        # Create Or Update Suggestions Bulk
        api_response = await api_instance.create_or_update_suggestions_bulk_api_v1_suggestions_bulk_post(suggestion_create)
        print("The response of SuggestionsApi->create_or_update_suggestions_bulk_api_v1_suggestions_bulk_post:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling SuggestionsApi->create_or_update_suggestions_bulk_api_v1_suggestions_bulk_post: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **suggestion_create** | [**List[SuggestionCreate]**](SuggestionCreate.md)|  | 

### Return type

[**List[SuggestionRead]**](SuggestionRead.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**201** | Successful Response |  -  |
**422** | Validation Error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **create_suggestion_api_v1_suggestions_post**
> SuggestionRead create_suggestion_api_v1_suggestions_post(suggestion_create)

Create Suggestion

### Example

* Bearer Authentication (bearerAuth):

```python
import magneto_api_client
from magneto_api_client.models.suggestion_create import SuggestionCreate
from magneto_api_client.models.suggestion_read import SuggestionRead
from magneto_api_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = magneto_api_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: bearerAuth
configuration = magneto_api_client.Configuration(
    access_token = os.environ["BEARER_TOKEN"]
)

# Enter a context with an instance of the API client
async with magneto_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = magneto_api_client.SuggestionsApi(api_client)
    suggestion_create = magneto_api_client.SuggestionCreate() # SuggestionCreate | 

    try:
        # Create Suggestion
        api_response = await api_instance.create_suggestion_api_v1_suggestions_post(suggestion_create)
        print("The response of SuggestionsApi->create_suggestion_api_v1_suggestions_post:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling SuggestionsApi->create_suggestion_api_v1_suggestions_post: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **suggestion_create** | [**SuggestionCreate**](SuggestionCreate.md)|  | 

### Return type

[**SuggestionRead**](SuggestionRead.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**201** | Successful Response |  -  |
**422** | Validation Error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **delete_suggestion_api_v1_suggestions_id_delete**
> delete_suggestion_api_v1_suggestions_id_delete(id_)

Delete Suggestion

### Example

* Bearer Authentication (bearerAuth):

```python
import magneto_api_client
from magneto_api_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = magneto_api_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: bearerAuth
configuration = magneto_api_client.Configuration(
    access_token = os.environ["BEARER_TOKEN"]
)

# Enter a context with an instance of the API client
async with magneto_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = magneto_api_client.SuggestionsApi(api_client)
    id_ = 'id__example' # str | 

    try:
        # Delete Suggestion
        await api_instance.delete_suggestion_api_v1_suggestions_id_delete(id_)
    except Exception as e:
        print("Exception when calling SuggestionsApi->delete_suggestion_api_v1_suggestions_id_delete: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id_** | **str**|  | 

### Return type

void (empty response body)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**204** | Successful Response |  -  |
**422** | Validation Error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_suggestion_api_v1_suggestions_id_get**
> SuggestionRead get_suggestion_api_v1_suggestions_id_get(id_)

Get Suggestion

### Example

* Bearer Authentication (bearerAuth):

```python
import magneto_api_client
from magneto_api_client.models.suggestion_read import SuggestionRead
from magneto_api_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = magneto_api_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: bearerAuth
configuration = magneto_api_client.Configuration(
    access_token = os.environ["BEARER_TOKEN"]
)

# Enter a context with an instance of the API client
async with magneto_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = magneto_api_client.SuggestionsApi(api_client)
    id_ = 'id__example' # str | 

    try:
        # Get Suggestion
        api_response = await api_instance.get_suggestion_api_v1_suggestions_id_get(id_)
        print("The response of SuggestionsApi->get_suggestion_api_v1_suggestions_id_get:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling SuggestionsApi->get_suggestion_api_v1_suggestions_id_get: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id_** | **str**|  | 

### Return type

[**SuggestionRead**](SuggestionRead.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful Response |  -  |
**422** | Validation Error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_suggestions_api_v1_suggestions_get**
> CursorPageSuggestionRead get_suggestions_api_v1_suggestions_get(search_query=search_query, filters=filters, sorts=sorts, cursor=cursor, size=size, include_total=include_total)

Get Suggestions

### Example

* Bearer Authentication (bearerAuth):

```python
import magneto_api_client
from magneto_api_client.models.cursor_page_suggestion_read import CursorPageSuggestionRead
from magneto_api_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = magneto_api_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: bearerAuth
configuration = magneto_api_client.Configuration(
    access_token = os.environ["BEARER_TOKEN"]
)

# Enter a context with an instance of the API client
async with magneto_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = magneto_api_client.SuggestionsApi(api_client)
    search_query = 'search_query_example' # str |  (optional)
    filters = ['filters_example'] # List[str] |  (optional)
    sorts = ['sorts_example'] # List[str] |  (optional)
    cursor = 'cursor_example' # str |  (optional)
    size = 50 # int |  (optional) (default to 50)
    include_total = False # bool |  (optional) (default to False)

    try:
        # Get Suggestions
        api_response = await api_instance.get_suggestions_api_v1_suggestions_get(search_query=search_query, filters=filters, sorts=sorts, cursor=cursor, size=size, include_total=include_total)
        print("The response of SuggestionsApi->get_suggestions_api_v1_suggestions_get:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling SuggestionsApi->get_suggestions_api_v1_suggestions_get: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **search_query** | **str**|  | [optional] 
 **filters** | [**List[str]**](str.md)|  | [optional] 
 **sorts** | [**List[str]**](str.md)|  | [optional] 
 **cursor** | **str**|  | [optional] 
 **size** | **int**|  | [optional] [default to 50]
 **include_total** | **bool**|  | [optional] [default to False]

### Return type

[**CursorPageSuggestionRead**](CursorPageSuggestionRead.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful Response |  -  |
**422** | Validation Error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **upsert_suggestion_api_v1_suggestions_id_put**
> SuggestionRead upsert_suggestion_api_v1_suggestions_id_put(id_, suggestion_update)

Upsert Suggestion

### Example

* Bearer Authentication (bearerAuth):

```python
import magneto_api_client
from magneto_api_client.models.suggestion_read import SuggestionRead
from magneto_api_client.models.suggestion_update import SuggestionUpdate
from magneto_api_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = magneto_api_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: bearerAuth
configuration = magneto_api_client.Configuration(
    access_token = os.environ["BEARER_TOKEN"]
)

# Enter a context with an instance of the API client
async with magneto_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = magneto_api_client.SuggestionsApi(api_client)
    id_ = 'id__example' # str | 
    suggestion_update = magneto_api_client.SuggestionUpdate() # SuggestionUpdate | 

    try:
        # Upsert Suggestion
        api_response = await api_instance.upsert_suggestion_api_v1_suggestions_id_put(id_, suggestion_update)
        print("The response of SuggestionsApi->upsert_suggestion_api_v1_suggestions_id_put:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling SuggestionsApi->upsert_suggestion_api_v1_suggestions_id_put: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id_** | **str**|  | 
 **suggestion_update** | [**SuggestionUpdate**](SuggestionUpdate.md)|  | 

### Return type

[**SuggestionRead**](SuggestionRead.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful Response |  -  |
**422** | Validation Error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

