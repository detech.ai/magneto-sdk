# BlueprintUpdate


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** |  | 
**title** | **str** |  | 
**description** | **str** |  | [optional] 
**icon** | **str** |  | [optional] 
**schema_properties** | [**SchemaProperties**](SchemaProperties.md) |  | [optional] 
**reference_properties** | [**Dict[str, ReferencePropertyCreateOrUpdate]**](ReferencePropertyCreateOrUpdate.md) |  | [optional] 
**calculation_properties** | [**Dict[str, CalculationProperty]**](CalculationProperty.md) |  | [optional] 
**relations** | [**Dict[str, BlueprintRelation]**](BlueprintRelation.md) |  | [optional] 
**options** | [**BlueprintOptions**](BlueprintOptions.md) |  | [optional] 
**is_active** | **bool** |  | [optional] [default to True]
**is_hideable** | **bool** |  | [optional] [default to True]

## Example

```python
from magneto_api_client.models.blueprint_update import BlueprintUpdate

# TODO update the JSON string below
json = "{}"
# create an instance of BlueprintUpdate from a JSON string
blueprint_update_instance = BlueprintUpdate.from_json(json)
# print the JSON string representation of the object
print(BlueprintUpdate.to_json())

# convert the object into a dict
blueprint_update_dict = blueprint_update_instance.to_dict()
# create an instance of BlueprintUpdate from a dict
blueprint_update_from_dict = BlueprintUpdate.from_dict(blueprint_update_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


