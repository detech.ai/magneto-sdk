# ScorecardRule

Base class for models that exclude unset fields.  **Note**: This overrides any `exclude_unset` kwarg passed for the model and its nested models.

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** |  | 
**title** | **str** |  | [optional] 
**description** | **str** |  | [optional] 
**icon** | **str** |  | [optional] 
**conditions** | [**List[Condition]**](Condition.md) |  | 

## Example

```python
from magneto_api_client.models.scorecard_rule import ScorecardRule

# TODO update the JSON string below
json = "{}"
# create an instance of ScorecardRule from a JSON string
scorecard_rule_instance = ScorecardRule.from_json(json)
# print the JSON string representation of the object
print(ScorecardRule.to_json())

# convert the object into a dict
scorecard_rule_dict = scorecard_rule_instance.to_dict()
# create an instance of ScorecardRule from a dict
scorecard_rule_from_dict = ScorecardRule.from_dict(scorecard_rule_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


