# OrganizationViewConfigRead


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**config** | **object** |  | 

## Example

```python
from magneto_api_client.models.organization_view_config_read import OrganizationViewConfigRead

# TODO update the JSON string below
json = "{}"
# create an instance of OrganizationViewConfigRead from a JSON string
organization_view_config_read_instance = OrganizationViewConfigRead.from_json(json)
# print the JSON string representation of the object
print(OrganizationViewConfigRead.to_json())

# convert the object into a dict
organization_view_config_read_dict = organization_view_config_read_instance.to_dict()
# create an instance of OrganizationViewConfigRead from a dict
organization_view_config_read_from_dict = OrganizationViewConfigRead.from_dict(organization_view_config_read_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


