# OrganizationRead

Base class for models that have a bigint ID.

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**sid** | **int** |  | 
**name** | **str** |  | [optional] [default to '']
**is_active** | **bool** |  | [optional] 
**status** | [**OrganizationStatus**](OrganizationStatus.md) |  | [optional] 
**trial_enabled** | **bool** |  | [optional] 
**date_joined** | **datetime** |  | [optional] 
**connection** | **str** |  | [optional] 

## Example

```python
from magneto_api_client.models.organization_read import OrganizationRead

# TODO update the JSON string below
json = "{}"
# create an instance of OrganizationRead from a JSON string
organization_read_instance = OrganizationRead.from_json(json)
# print the JSON string representation of the object
print(OrganizationRead.to_json())

# convert the object into a dict
organization_read_dict = organization_read_instance.to_dict()
# create an instance of OrganizationRead from a dict
organization_read_from_dict = OrganizationRead.from_dict(organization_read_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


