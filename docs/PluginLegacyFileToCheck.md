# PluginLegacyFileToCheck


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**path** | **str** |  | 
**destination** | **str** |  | 
**regex** | **str** |  | [optional] 

## Example

```python
from magneto_api_client.models.plugin_legacy_file_to_check import PluginLegacyFileToCheck

# TODO update the JSON string below
json = "{}"
# create an instance of PluginLegacyFileToCheck from a JSON string
plugin_legacy_file_to_check_instance = PluginLegacyFileToCheck.from_json(json)
# print the JSON string representation of the object
print(PluginLegacyFileToCheck.to_json())

# convert the object into a dict
plugin_legacy_file_to_check_dict = plugin_legacy_file_to_check_instance.to_dict()
# create an instance of PluginLegacyFileToCheck from a dict
plugin_legacy_file_to_check_from_dict = PluginLegacyFileToCheck.from_dict(plugin_legacy_file_to_check_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


