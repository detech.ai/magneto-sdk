# UserEntityUpdate


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**team** | **str** |  | 

## Example

```python
from magneto_api_client.models.user_entity_update import UserEntityUpdate

# TODO update the JSON string below
json = "{}"
# create an instance of UserEntityUpdate from a JSON string
user_entity_update_instance = UserEntityUpdate.from_json(json)
# print the JSON string representation of the object
print(UserEntityUpdate.to_json())

# convert the object into a dict
user_entity_update_dict = user_entity_update_instance.to_dict()
# create an instance of UserEntityUpdate from a dict
user_entity_update_from_dict = UserEntityUpdate.from_dict(user_entity_update_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


