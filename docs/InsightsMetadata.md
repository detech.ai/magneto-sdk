# InsightsMetadata

Metadata about the insights including timing and scope.

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**total_entities** | **int** |  | 
**timestamp** | **datetime** |  | [optional] 

## Example

```python
from magneto_api_client.models.insights_metadata import InsightsMetadata

# TODO update the JSON string below
json = "{}"
# create an instance of InsightsMetadata from a JSON string
insights_metadata_instance = InsightsMetadata.from_json(json)
# print the JSON string representation of the object
print(InsightsMetadata.to_json())

# convert the object into a dict
insights_metadata_dict = insights_metadata_instance.to_dict()
# create an instance of InsightsMetadata from a dict
insights_metadata_from_dict = InsightsMetadata.from_dict(insights_metadata_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


