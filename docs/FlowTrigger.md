# FlowTrigger

Base class for models that exclude unset fields.  **Note**: This overrides any `exclude_unset` kwarg passed for the model and its nested models.

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**type** | [**FlowTriggerType**](FlowTriggerType.md) |  | 
**conditions** | [**List[Condition]**](Condition.md) |  | [optional] 
**event** | [**FlowTriggerEventData**](FlowTriggerEventData.md) |  | [optional] 
**input_schema** | **object** |  | [optional] 

## Example

```python
from magneto_api_client.models.flow_trigger import FlowTrigger

# TODO update the JSON string below
json = "{}"
# create an instance of FlowTrigger from a JSON string
flow_trigger_instance = FlowTrigger.from_json(json)
# print the JSON string representation of the object
print(FlowTrigger.to_json())

# convert the object into a dict
flow_trigger_dict = flow_trigger_instance.to_dict()
# create an instance of FlowTrigger from a dict
flow_trigger_from_dict = FlowTrigger.from_dict(flow_trigger_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


