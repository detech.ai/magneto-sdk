# OrganizationViewConfigCreate


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**path** | **str** |  | 
**config** | **object** |  | 

## Example

```python
from magneto_api_client.models.organization_view_config_create import OrganizationViewConfigCreate

# TODO update the JSON string below
json = "{}"
# create an instance of OrganizationViewConfigCreate from a JSON string
organization_view_config_create_instance = OrganizationViewConfigCreate.from_json(json)
# print the JSON string representation of the object
print(OrganizationViewConfigCreate.to_json())

# convert the object into a dict
organization_view_config_create_dict = organization_view_config_create_instance.to_dict()
# create an instance of OrganizationViewConfigCreate from a dict
organization_view_config_create_from_dict = OrganizationViewConfigCreate.from_dict(organization_view_config_create_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


