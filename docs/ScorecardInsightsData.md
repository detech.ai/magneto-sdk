# ScorecardInsightsData

Container for all rank-related insights.

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ranks** | [**List[RankInsight]**](RankInsight.md) |  | [optional] 

## Example

```python
from magneto_api_client.models.scorecard_insights_data import ScorecardInsightsData

# TODO update the JSON string below
json = "{}"
# create an instance of ScorecardInsightsData from a JSON string
scorecard_insights_data_instance = ScorecardInsightsData.from_json(json)
# print the JSON string representation of the object
print(ScorecardInsightsData.to_json())

# convert the object into a dict
scorecard_insights_data_dict = scorecard_insights_data_instance.to_dict()
# create an instance of ScorecardInsightsData from a dict
scorecard_insights_data_from_dict = ScorecardInsightsData.from_dict(scorecard_insights_data_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


