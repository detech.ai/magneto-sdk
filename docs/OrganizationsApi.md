# magneto_api_client.OrganizationsApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**create_or_update_view_config_api_v1_organizations_self_view_configs_put**](OrganizationsApi.md#create_or_update_view_config_api_v1_organizations_self_view_configs_put) | **PUT** /api/v1/organizations/self/view-configs | Create Or Update View Config
[**create_organization_api_v1_organizations_post**](OrganizationsApi.md#create_organization_api_v1_organizations_post) | **POST** /api/v1/organizations | Create Organization
[**delete_organization_me_api_v1_organizations_self_delete**](OrganizationsApi.md#delete_organization_me_api_v1_organizations_self_delete) | **DELETE** /api/v1/organizations/self | Delete Organization Me
[**disable_organization_me_api_v1_organizations_self_disable_post**](OrganizationsApi.md#disable_organization_me_api_v1_organizations_self_disable_post) | **POST** /api/v1/organizations/self/disable | Disable Organization Me
[**enable_organization_me_api_v1_organizations_self_enable_post**](OrganizationsApi.md#enable_organization_me_api_v1_organizations_self_enable_post) | **POST** /api/v1/organizations/self/enable | Enable Organization Me
[**get_organization_me_api_v1_organizations_self_get**](OrganizationsApi.md#get_organization_me_api_v1_organizations_self_get) | **GET** /api/v1/organizations/self | Get Organization Me
[**get_view_config_api_v1_organizations_self_view_configs_get**](OrganizationsApi.md#get_view_config_api_v1_organizations_self_view_configs_get) | **GET** /api/v1/organizations/self/view-configs | Get View Config


# **create_or_update_view_config_api_v1_organizations_self_view_configs_put**
> OrganizationViewConfigRead create_or_update_view_config_api_v1_organizations_self_view_configs_put(organization_view_config_create)

Create Or Update View Config

### Example

* Bearer Authentication (bearerAuth):

```python
import magneto_api_client
from magneto_api_client.models.organization_view_config_create import OrganizationViewConfigCreate
from magneto_api_client.models.organization_view_config_read import OrganizationViewConfigRead
from magneto_api_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = magneto_api_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: bearerAuth
configuration = magneto_api_client.Configuration(
    access_token = os.environ["BEARER_TOKEN"]
)

# Enter a context with an instance of the API client
async with magneto_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = magneto_api_client.OrganizationsApi(api_client)
    organization_view_config_create = magneto_api_client.OrganizationViewConfigCreate() # OrganizationViewConfigCreate | 

    try:
        # Create Or Update View Config
        api_response = await api_instance.create_or_update_view_config_api_v1_organizations_self_view_configs_put(organization_view_config_create)
        print("The response of OrganizationsApi->create_or_update_view_config_api_v1_organizations_self_view_configs_put:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling OrganizationsApi->create_or_update_view_config_api_v1_organizations_self_view_configs_put: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **organization_view_config_create** | [**OrganizationViewConfigCreate**](OrganizationViewConfigCreate.md)|  | 

### Return type

[**OrganizationViewConfigRead**](OrganizationViewConfigRead.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**201** | Successful Response |  -  |
**422** | Validation Error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **create_organization_api_v1_organizations_post**
> OrganizationRead create_organization_api_v1_organizations_post(organization_create)

Create Organization

### Example

* Bearer Authentication (bearerAuth):

```python
import magneto_api_client
from magneto_api_client.models.organization_create import OrganizationCreate
from magneto_api_client.models.organization_read import OrganizationRead
from magneto_api_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = magneto_api_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: bearerAuth
configuration = magneto_api_client.Configuration(
    access_token = os.environ["BEARER_TOKEN"]
)

# Enter a context with an instance of the API client
async with magneto_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = magneto_api_client.OrganizationsApi(api_client)
    organization_create = magneto_api_client.OrganizationCreate() # OrganizationCreate | 

    try:
        # Create Organization
        api_response = await api_instance.create_organization_api_v1_organizations_post(organization_create)
        print("The response of OrganizationsApi->create_organization_api_v1_organizations_post:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling OrganizationsApi->create_organization_api_v1_organizations_post: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **organization_create** | [**OrganizationCreate**](OrganizationCreate.md)|  | 

### Return type

[**OrganizationRead**](OrganizationRead.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**201** | Successful Response |  -  |
**422** | Validation Error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **delete_organization_me_api_v1_organizations_self_delete**
> OrganizationRead delete_organization_me_api_v1_organizations_self_delete()

Delete Organization Me

### Example

* Bearer Authentication (bearerAuth):

```python
import magneto_api_client
from magneto_api_client.models.organization_read import OrganizationRead
from magneto_api_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = magneto_api_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: bearerAuth
configuration = magneto_api_client.Configuration(
    access_token = os.environ["BEARER_TOKEN"]
)

# Enter a context with an instance of the API client
async with magneto_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = magneto_api_client.OrganizationsApi(api_client)

    try:
        # Delete Organization Me
        api_response = await api_instance.delete_organization_me_api_v1_organizations_self_delete()
        print("The response of OrganizationsApi->delete_organization_me_api_v1_organizations_self_delete:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling OrganizationsApi->delete_organization_me_api_v1_organizations_self_delete: %s\n" % e)
```



### Parameters

This endpoint does not need any parameter.

### Return type

[**OrganizationRead**](OrganizationRead.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful Response |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **disable_organization_me_api_v1_organizations_self_disable_post**
> OrganizationRead disable_organization_me_api_v1_organizations_self_disable_post()

Disable Organization Me

### Example

* Bearer Authentication (bearerAuth):

```python
import magneto_api_client
from magneto_api_client.models.organization_read import OrganizationRead
from magneto_api_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = magneto_api_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: bearerAuth
configuration = magneto_api_client.Configuration(
    access_token = os.environ["BEARER_TOKEN"]
)

# Enter a context with an instance of the API client
async with magneto_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = magneto_api_client.OrganizationsApi(api_client)

    try:
        # Disable Organization Me
        api_response = await api_instance.disable_organization_me_api_v1_organizations_self_disable_post()
        print("The response of OrganizationsApi->disable_organization_me_api_v1_organizations_self_disable_post:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling OrganizationsApi->disable_organization_me_api_v1_organizations_self_disable_post: %s\n" % e)
```



### Parameters

This endpoint does not need any parameter.

### Return type

[**OrganizationRead**](OrganizationRead.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful Response |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **enable_organization_me_api_v1_organizations_self_enable_post**
> OrganizationRead enable_organization_me_api_v1_organizations_self_enable_post(remove_from_trial=remove_from_trial)

Enable Organization Me

### Example

* Bearer Authentication (bearerAuth):

```python
import magneto_api_client
from magneto_api_client.models.organization_read import OrganizationRead
from magneto_api_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = magneto_api_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: bearerAuth
configuration = magneto_api_client.Configuration(
    access_token = os.environ["BEARER_TOKEN"]
)

# Enter a context with an instance of the API client
async with magneto_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = magneto_api_client.OrganizationsApi(api_client)
    remove_from_trial = False # bool |  (optional) (default to False)

    try:
        # Enable Organization Me
        api_response = await api_instance.enable_organization_me_api_v1_organizations_self_enable_post(remove_from_trial=remove_from_trial)
        print("The response of OrganizationsApi->enable_organization_me_api_v1_organizations_self_enable_post:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling OrganizationsApi->enable_organization_me_api_v1_organizations_self_enable_post: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **remove_from_trial** | **bool**|  | [optional] [default to False]

### Return type

[**OrganizationRead**](OrganizationRead.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful Response |  -  |
**422** | Validation Error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_organization_me_api_v1_organizations_self_get**
> OrganizationRead get_organization_me_api_v1_organizations_self_get()

Get Organization Me

### Example

* Bearer Authentication (bearerAuth):

```python
import magneto_api_client
from magneto_api_client.models.organization_read import OrganizationRead
from magneto_api_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = magneto_api_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: bearerAuth
configuration = magneto_api_client.Configuration(
    access_token = os.environ["BEARER_TOKEN"]
)

# Enter a context with an instance of the API client
async with magneto_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = magneto_api_client.OrganizationsApi(api_client)

    try:
        # Get Organization Me
        api_response = await api_instance.get_organization_me_api_v1_organizations_self_get()
        print("The response of OrganizationsApi->get_organization_me_api_v1_organizations_self_get:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling OrganizationsApi->get_organization_me_api_v1_organizations_self_get: %s\n" % e)
```



### Parameters

This endpoint does not need any parameter.

### Return type

[**OrganizationRead**](OrganizationRead.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful Response |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_view_config_api_v1_organizations_self_view_configs_get**
> OrganizationViewConfigRead get_view_config_api_v1_organizations_self_view_configs_get(path=path)

Get View Config

### Example

* Bearer Authentication (bearerAuth):

```python
import magneto_api_client
from magneto_api_client.models.organization_view_config_read import OrganizationViewConfigRead
from magneto_api_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = magneto_api_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: bearerAuth
configuration = magneto_api_client.Configuration(
    access_token = os.environ["BEARER_TOKEN"]
)

# Enter a context with an instance of the API client
async with magneto_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = magneto_api_client.OrganizationsApi(api_client)
    path = 'path_example' # str |  (optional)

    try:
        # Get View Config
        api_response = await api_instance.get_view_config_api_v1_organizations_self_view_configs_get(path=path)
        print("The response of OrganizationsApi->get_view_config_api_v1_organizations_self_view_configs_get:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling OrganizationsApi->get_view_config_api_v1_organizations_self_view_configs_get: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **path** | **str**|  | [optional] 

### Return type

[**OrganizationViewConfigRead**](OrganizationViewConfigRead.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful Response |  -  |
**422** | Validation Error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

