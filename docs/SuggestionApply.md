# SuggestionApply

Suggestion Apply Request Model.

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**target_id_override** | **str** |  | [optional] 

## Example

```python
from magneto_api_client.models.suggestion_apply import SuggestionApply

# TODO update the JSON string below
json = "{}"
# create an instance of SuggestionApply from a JSON string
suggestion_apply_instance = SuggestionApply.from_json(json)
# print the JSON string representation of the object
print(SuggestionApply.to_json())

# convert the object into a dict
suggestion_apply_dict = suggestion_apply_instance.to_dict()
# create an instance of SuggestionApply from a dict
suggestion_apply_from_dict = SuggestionApply.from_dict(suggestion_apply_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


