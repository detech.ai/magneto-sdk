# magneto_api_client.LegacyPluginsApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**create_plugin_api_v1_legacy_plugins_post**](LegacyPluginsApi.md#create_plugin_api_v1_legacy_plugins_post) | **POST** /api/v1/legacy/plugins | Create Plugin
[**delete_plugin_api_v1_legacy_plugins_id_delete**](LegacyPluginsApi.md#delete_plugin_api_v1_legacy_plugins_id_delete) | **DELETE** /api/v1/legacy/plugins/{id_} | Delete Plugin
[**get_plugin_api_v1_legacy_plugins_id_get**](LegacyPluginsApi.md#get_plugin_api_v1_legacy_plugins_id_get) | **GET** /api/v1/legacy/plugins/{id_} | Get Plugin
[**get_plugin_blueprints_api_v1_legacy_plugins_data_source_blueprints_get**](LegacyPluginsApi.md#get_plugin_blueprints_api_v1_legacy_plugins_data_source_blueprints_get) | **GET** /api/v1/legacy/plugins/{data_source}/blueprints | Get Plugin Blueprints
[**get_plugin_token_api_v1_legacy_plugins_id_token_get**](LegacyPluginsApi.md#get_plugin_token_api_v1_legacy_plugins_id_token_get) | **GET** /api/v1/legacy/plugins/{id_}/token | Get Plugin Token
[**get_plugins_api_v1_legacy_plugins_get**](LegacyPluginsApi.md#get_plugins_api_v1_legacy_plugins_get) | **GET** /api/v1/legacy/plugins | Get Plugins
[**update_plugin_api_v1_legacy_plugins_id_put**](LegacyPluginsApi.md#update_plugin_api_v1_legacy_plugins_id_put) | **PUT** /api/v1/legacy/plugins/{id_} | Update Plugin
[**update_plugin_files_to_check_api_v1_legacy_plugins_id_files_to_check_put**](LegacyPluginsApi.md#update_plugin_files_to_check_api_v1_legacy_plugins_id_files_to_check_put) | **PUT** /api/v1/legacy/plugins/{id_}/files-to-check | Update Plugin Files To Check
[**validate_query_plugin_api_v1_legacy_plugins_id_validate_query_post**](LegacyPluginsApi.md#validate_query_plugin_api_v1_legacy_plugins_id_validate_query_post) | **POST** /api/v1/legacy/plugins/{id_}/validate-query | Validate Query Plugin


# **create_plugin_api_v1_legacy_plugins_post**
> PluginLegacyRead create_plugin_api_v1_legacy_plugins_post(plugin_legacy_create)

Create Plugin

### Example

* Bearer Authentication (bearerAuth):

```python
import magneto_api_client
from magneto_api_client.models.plugin_legacy_create import PluginLegacyCreate
from magneto_api_client.models.plugin_legacy_read import PluginLegacyRead
from magneto_api_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = magneto_api_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: bearerAuth
configuration = magneto_api_client.Configuration(
    access_token = os.environ["BEARER_TOKEN"]
)

# Enter a context with an instance of the API client
async with magneto_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = magneto_api_client.LegacyPluginsApi(api_client)
    plugin_legacy_create = magneto_api_client.PluginLegacyCreate() # PluginLegacyCreate | 

    try:
        # Create Plugin
        api_response = await api_instance.create_plugin_api_v1_legacy_plugins_post(plugin_legacy_create)
        print("The response of LegacyPluginsApi->create_plugin_api_v1_legacy_plugins_post:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling LegacyPluginsApi->create_plugin_api_v1_legacy_plugins_post: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **plugin_legacy_create** | [**PluginLegacyCreate**](PluginLegacyCreate.md)|  | 

### Return type

[**PluginLegacyRead**](PluginLegacyRead.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**201** | Successful Response |  -  |
**422** | Validation Error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **delete_plugin_api_v1_legacy_plugins_id_delete**
> delete_plugin_api_v1_legacy_plugins_id_delete(id_)

Delete Plugin

### Example

* Bearer Authentication (bearerAuth):

```python
import magneto_api_client
from magneto_api_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = magneto_api_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: bearerAuth
configuration = magneto_api_client.Configuration(
    access_token = os.environ["BEARER_TOKEN"]
)

# Enter a context with an instance of the API client
async with magneto_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = magneto_api_client.LegacyPluginsApi(api_client)
    id_ = 'id__example' # str | 

    try:
        # Delete Plugin
        await api_instance.delete_plugin_api_v1_legacy_plugins_id_delete(id_)
    except Exception as e:
        print("Exception when calling LegacyPluginsApi->delete_plugin_api_v1_legacy_plugins_id_delete: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id_** | **str**|  | 

### Return type

void (empty response body)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**204** | Successful Response |  -  |
**422** | Validation Error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_plugin_api_v1_legacy_plugins_id_get**
> PluginLegacyRead get_plugin_api_v1_legacy_plugins_id_get(id_)

Get Plugin

### Example

* Bearer Authentication (bearerAuth):

```python
import magneto_api_client
from magneto_api_client.models.plugin_legacy_read import PluginLegacyRead
from magneto_api_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = magneto_api_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: bearerAuth
configuration = magneto_api_client.Configuration(
    access_token = os.environ["BEARER_TOKEN"]
)

# Enter a context with an instance of the API client
async with magneto_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = magneto_api_client.LegacyPluginsApi(api_client)
    id_ = 'id__example' # str | 

    try:
        # Get Plugin
        api_response = await api_instance.get_plugin_api_v1_legacy_plugins_id_get(id_)
        print("The response of LegacyPluginsApi->get_plugin_api_v1_legacy_plugins_id_get:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling LegacyPluginsApi->get_plugin_api_v1_legacy_plugins_id_get: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id_** | **str**|  | 

### Return type

[**PluginLegacyRead**](PluginLegacyRead.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful Response |  -  |
**422** | Validation Error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_plugin_blueprints_api_v1_legacy_plugins_data_source_blueprints_get**
> List[BlueprintRead] get_plugin_blueprints_api_v1_legacy_plugins_data_source_blueprints_get(data_source)

Get Plugin Blueprints

### Example

* Bearer Authentication (bearerAuth):

```python
import magneto_api_client
from magneto_api_client.models.blueprint_read import BlueprintRead
from magneto_api_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = magneto_api_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: bearerAuth
configuration = magneto_api_client.Configuration(
    access_token = os.environ["BEARER_TOKEN"]
)

# Enter a context with an instance of the API client
async with magneto_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = magneto_api_client.LegacyPluginsApi(api_client)
    data_source = 'data_source_example' # str | 

    try:
        # Get Plugin Blueprints
        api_response = await api_instance.get_plugin_blueprints_api_v1_legacy_plugins_data_source_blueprints_get(data_source)
        print("The response of LegacyPluginsApi->get_plugin_blueprints_api_v1_legacy_plugins_data_source_blueprints_get:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling LegacyPluginsApi->get_plugin_blueprints_api_v1_legacy_plugins_data_source_blueprints_get: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data_source** | **str**|  | 

### Return type

[**List[BlueprintRead]**](BlueprintRead.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful Response |  -  |
**422** | Validation Error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_plugin_token_api_v1_legacy_plugins_id_token_get**
> PluginTokenRead get_plugin_token_api_v1_legacy_plugins_id_token_get(id_)

Get Plugin Token

### Example

* Bearer Authentication (bearerAuth):

```python
import magneto_api_client
from magneto_api_client.models.plugin_token_read import PluginTokenRead
from magneto_api_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = magneto_api_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: bearerAuth
configuration = magneto_api_client.Configuration(
    access_token = os.environ["BEARER_TOKEN"]
)

# Enter a context with an instance of the API client
async with magneto_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = magneto_api_client.LegacyPluginsApi(api_client)
    id_ = 'id__example' # str | 

    try:
        # Get Plugin Token
        api_response = await api_instance.get_plugin_token_api_v1_legacy_plugins_id_token_get(id_)
        print("The response of LegacyPluginsApi->get_plugin_token_api_v1_legacy_plugins_id_token_get:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling LegacyPluginsApi->get_plugin_token_api_v1_legacy_plugins_id_token_get: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id_** | **str**|  | 

### Return type

[**PluginTokenRead**](PluginTokenRead.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful Response |  -  |
**422** | Validation Error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_plugins_api_v1_legacy_plugins_get**
> List[PluginLegacyRead] get_plugins_api_v1_legacy_plugins_get(filter_capabilities=filter_capabilities)

Get Plugins

### Example

* Bearer Authentication (bearerAuth):

```python
import magneto_api_client
from magneto_api_client.models.plugin_capability import PluginCapability
from magneto_api_client.models.plugin_legacy_read import PluginLegacyRead
from magneto_api_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = magneto_api_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: bearerAuth
configuration = magneto_api_client.Configuration(
    access_token = os.environ["BEARER_TOKEN"]
)

# Enter a context with an instance of the API client
async with magneto_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = magneto_api_client.LegacyPluginsApi(api_client)
    filter_capabilities = [magneto_api_client.PluginCapability()] # List[PluginCapability] | Filter plugins by capabilities (optional)

    try:
        # Get Plugins
        api_response = await api_instance.get_plugins_api_v1_legacy_plugins_get(filter_capabilities=filter_capabilities)
        print("The response of LegacyPluginsApi->get_plugins_api_v1_legacy_plugins_get:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling LegacyPluginsApi->get_plugins_api_v1_legacy_plugins_get: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter_capabilities** | [**List[PluginCapability]**](PluginCapability.md)| Filter plugins by capabilities | [optional] 

### Return type

[**List[PluginLegacyRead]**](PluginLegacyRead.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful Response |  -  |
**422** | Validation Error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **update_plugin_api_v1_legacy_plugins_id_put**
> PluginLegacyRead update_plugin_api_v1_legacy_plugins_id_put(id_, plugin_legacy_update)

Update Plugin

### Example

* Bearer Authentication (bearerAuth):

```python
import magneto_api_client
from magneto_api_client.models.plugin_legacy_read import PluginLegacyRead
from magneto_api_client.models.plugin_legacy_update import PluginLegacyUpdate
from magneto_api_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = magneto_api_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: bearerAuth
configuration = magneto_api_client.Configuration(
    access_token = os.environ["BEARER_TOKEN"]
)

# Enter a context with an instance of the API client
async with magneto_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = magneto_api_client.LegacyPluginsApi(api_client)
    id_ = 'id__example' # str | 
    plugin_legacy_update = magneto_api_client.PluginLegacyUpdate() # PluginLegacyUpdate | 

    try:
        # Update Plugin
        api_response = await api_instance.update_plugin_api_v1_legacy_plugins_id_put(id_, plugin_legacy_update)
        print("The response of LegacyPluginsApi->update_plugin_api_v1_legacy_plugins_id_put:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling LegacyPluginsApi->update_plugin_api_v1_legacy_plugins_id_put: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id_** | **str**|  | 
 **plugin_legacy_update** | [**PluginLegacyUpdate**](PluginLegacyUpdate.md)|  | 

### Return type

[**PluginLegacyRead**](PluginLegacyRead.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful Response |  -  |
**422** | Validation Error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **update_plugin_files_to_check_api_v1_legacy_plugins_id_files_to_check_put**
> PluginLegacyRead update_plugin_files_to_check_api_v1_legacy_plugins_id_files_to_check_put(id_, plugin_legacy_files_to_check_update)

Update Plugin Files To Check

### Example

* Bearer Authentication (bearerAuth):

```python
import magneto_api_client
from magneto_api_client.models.plugin_legacy_files_to_check_update import PluginLegacyFilesToCheckUpdate
from magneto_api_client.models.plugin_legacy_read import PluginLegacyRead
from magneto_api_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = magneto_api_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: bearerAuth
configuration = magneto_api_client.Configuration(
    access_token = os.environ["BEARER_TOKEN"]
)

# Enter a context with an instance of the API client
async with magneto_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = magneto_api_client.LegacyPluginsApi(api_client)
    id_ = 'id__example' # str | 
    plugin_legacy_files_to_check_update = magneto_api_client.PluginLegacyFilesToCheckUpdate() # PluginLegacyFilesToCheckUpdate | 

    try:
        # Update Plugin Files To Check
        api_response = await api_instance.update_plugin_files_to_check_api_v1_legacy_plugins_id_files_to_check_put(id_, plugin_legacy_files_to_check_update)
        print("The response of LegacyPluginsApi->update_plugin_files_to_check_api_v1_legacy_plugins_id_files_to_check_put:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling LegacyPluginsApi->update_plugin_files_to_check_api_v1_legacy_plugins_id_files_to_check_put: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id_** | **str**|  | 
 **plugin_legacy_files_to_check_update** | [**PluginLegacyFilesToCheckUpdate**](PluginLegacyFilesToCheckUpdate.md)|  | 

### Return type

[**PluginLegacyRead**](PluginLegacyRead.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful Response |  -  |
**422** | Validation Error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **validate_query_plugin_api_v1_legacy_plugins_id_validate_query_post**
> PluginLegacyValidateQueryOutput validate_query_plugin_api_v1_legacy_plugins_id_validate_query_post(id_, plugin_legacy_validate_query_input)

Validate Query Plugin

### Example

* Bearer Authentication (bearerAuth):

```python
import magneto_api_client
from magneto_api_client.models.plugin_legacy_validate_query_input import PluginLegacyValidateQueryInput
from magneto_api_client.models.plugin_legacy_validate_query_output import PluginLegacyValidateQueryOutput
from magneto_api_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = magneto_api_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: bearerAuth
configuration = magneto_api_client.Configuration(
    access_token = os.environ["BEARER_TOKEN"]
)

# Enter a context with an instance of the API client
async with magneto_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = magneto_api_client.LegacyPluginsApi(api_client)
    id_ = 'id__example' # str | 
    plugin_legacy_validate_query_input = magneto_api_client.PluginLegacyValidateQueryInput() # PluginLegacyValidateQueryInput | 

    try:
        # Validate Query Plugin
        api_response = await api_instance.validate_query_plugin_api_v1_legacy_plugins_id_validate_query_post(id_, plugin_legacy_validate_query_input)
        print("The response of LegacyPluginsApi->validate_query_plugin_api_v1_legacy_plugins_id_validate_query_post:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling LegacyPluginsApi->validate_query_plugin_api_v1_legacy_plugins_id_validate_query_post: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id_** | **str**|  | 
 **plugin_legacy_validate_query_input** | [**PluginLegacyValidateQueryInput**](PluginLegacyValidateQueryInput.md)|  | 

### Return type

[**PluginLegacyValidateQueryOutput**](PluginLegacyValidateQueryOutput.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful Response |  -  |
**422** | Validation Error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

