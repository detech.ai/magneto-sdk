# AgentStatus

An enumeration.

## Enum

* `ACTIVE` (value: `'active'`)

* `STOPPED` (value: `'stopped'`)

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


