# EntityRelation


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**value** | [**Value**](Value.md) |  | [optional] 

## Example

```python
from magneto_api_client.models.entity_relation import EntityRelation

# TODO update the JSON string below
json = "{}"
# create an instance of EntityRelation from a JSON string
entity_relation_instance = EntityRelation.from_json(json)
# print the JSON string representation of the object
print(EntityRelation.to_json())

# convert the object into a dict
entity_relation_dict = entity_relation_instance.to_dict()
# create an instance of EntityRelation from a dict
entity_relation_from_dict = EntityRelation.from_dict(entity_relation_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


