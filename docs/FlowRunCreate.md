# FlowRunCreate

FlowRun Create Model.

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** |  | [optional] 
**flow_id** | **str** |  | 
**flow_type** | **str** |  | [optional] [default to 'selfServiceAction']
**args** | **object** |  | [optional] 
**tags** | [**List[Tag]**](Tag.md) |  | [optional] 

## Example

```python
from magneto_api_client.models.flow_run_create import FlowRunCreate

# TODO update the JSON string below
json = "{}"
# create an instance of FlowRunCreate from a JSON string
flow_run_create_instance = FlowRunCreate.from_json(json)
# print the JSON string representation of the object
print(FlowRunCreate.to_json())

# convert the object into a dict
flow_run_create_dict = flow_run_create_instance.to_dict()
# create an instance of FlowRunCreate from a dict
flow_run_create_from_dict = FlowRunCreate.from_dict(flow_run_create_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


