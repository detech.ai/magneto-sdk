# FlowActionType

An enumeration.

## Enum

* `UPSERTRESOURCE` (value: `'upsertResource'`)

* `REFERENCEACTION` (value: `'referenceAction'`)

* `FETCHRESOURCE` (value: `'fetchResource'`)

* `HTTPREQUEST` (value: `'httpRequest'`)

* `EVALUATESCORECARDRULES` (value: `'evaluateScorecardRules'`)

* `SHELLCOMMAND` (value: `'shellCommand'`)

* `TRIGGERAUTOMATIONS` (value: `'triggerAutomations'`)

* `QUERYMODEL` (value: `'queryModel'`)

* `FETCHFIELDSFROMRESOURCES` (value: `'fetchFieldsFromResources'`)

* `FETCHENTITIESBYSOURCE` (value: `'fetchEntitiesBySource'`)

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


