# BlueprintRelation

Base class for models that exclude unset fields.  **Note**: This overrides any `exclude_unset` kwarg passed for the model and its nested models.

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**value** | **str** |  | 
**array** | **bool** |  | [optional] [default to False]
**title** | **str** |  | 
**description** | **str** |  | [optional] 

## Example

```python
from magneto_api_client.models.blueprint_relation import BlueprintRelation

# TODO update the JSON string below
json = "{}"
# create an instance of BlueprintRelation from a JSON string
blueprint_relation_instance = BlueprintRelation.from_json(json)
# print the JSON string representation of the object
print(BlueprintRelation.to_json())

# convert the object into a dict
blueprint_relation_dict = blueprint_relation_instance.to_dict()
# create an instance of BlueprintRelation from a dict
blueprint_relation_from_dict = BlueprintRelation.from_dict(blueprint_relation_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


