# CalculationPropertyMethod

An enumeration.

## Enum

* `AGGREGATION` (value: `'aggregation'`)

* `SQL` (value: `'sql'`)

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


