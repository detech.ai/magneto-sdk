# ScorecardCreate

Scorecard Create Model.

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** |  | [optional] 
**title** | **str** |  | 
**description** | **str** |  | [optional] 
**is_active** | **bool** |  | [optional] [default to True]
**blueprint_id** | **str** |  | 
**ranks** | [**List[ScorecardRank]**](ScorecardRank.md) |  | [optional] 
**median_rank** | **str** |  | [optional] 

## Example

```python
from magneto_api_client.models.scorecard_create import ScorecardCreate

# TODO update the JSON string below
json = "{}"
# create an instance of ScorecardCreate from a JSON string
scorecard_create_instance = ScorecardCreate.from_json(json)
# print the JSON string representation of the object
print(ScorecardCreate.to_json())

# convert the object into a dict
scorecard_create_dict = scorecard_create_instance.to_dict()
# create an instance of ScorecardCreate from a dict
scorecard_create_from_dict = ScorecardCreate.from_dict(scorecard_create_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


