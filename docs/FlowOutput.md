# FlowOutput

Base class for models that exclude unset fields.  **Note**: This overrides any `exclude_unset` kwarg passed for the model and its nested models.

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**value** | **str** |  | 
**icon** | **str** |  | [optional] 
**title** | **str** |  | [optional] 
**description** | **str** |  | [optional] 
**type** | **str** |  | [optional] 
**format** | **str** |  | [optional] 

## Example

```python
from magneto_api_client.models.flow_output import FlowOutput

# TODO update the JSON string below
json = "{}"
# create an instance of FlowOutput from a JSON string
flow_output_instance = FlowOutput.from_json(json)
# print the JSON string representation of the object
print(FlowOutput.to_json())

# convert the object into a dict
flow_output_dict = flow_output_instance.to_dict()
# create an instance of FlowOutput from a dict
flow_output_from_dict = FlowOutput.from_dict(flow_output_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


