# ActionDetails


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** |  | [optional] 
**phase** | [**ActionPhase**](ActionPhase.md) |  | 
**start_time** | **datetime** |  | 
**end_time** | **datetime** |  | [optional] 
**outputs** | **object** |  | [optional] 

## Example

```python
from magneto_api_client.models.action_details import ActionDetails

# TODO update the JSON string below
json = "{}"
# create an instance of ActionDetails from a JSON string
action_details_instance = ActionDetails.from_json(json)
# print the JSON string representation of the object
print(ActionDetails.to_json())

# convert the object into a dict
action_details_dict = action_details_instance.to_dict()
# create an instance of ActionDetails from a dict
action_details_from_dict = ActionDetails.from_dict(action_details_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


