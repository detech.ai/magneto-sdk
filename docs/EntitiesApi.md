# magneto_api_client.EntitiesApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**create_entity_api_v1_entities_post**](EntitiesApi.md#create_entity_api_v1_entities_post) | **POST** /api/v1/entities | Create Entity
[**create_entity_properties_api_v1_entities_id_properties_post**](EntitiesApi.md#create_entity_properties_api_v1_entities_id_properties_post) | **POST** /api/v1/entities/{id_}/properties | Create Entity Properties
[**create_or_update_entities_bulk_api_v1_entities_bulk_put**](EntitiesApi.md#create_or_update_entities_bulk_api_v1_entities_bulk_put) | **PUT** /api/v1/entities/bulk | Create Or Update Entities Bulk
[**delete_entity_api_v1_entities_id_delete**](EntitiesApi.md#delete_entity_api_v1_entities_id_delete) | **DELETE** /api/v1/entities/{id_} | Delete Entity
[**delete_entity_properties_api_v1_entities_id_properties_property_key_delete**](EntitiesApi.md#delete_entity_properties_api_v1_entities_id_properties_property_key_delete) | **DELETE** /api/v1/entities/{id_}/properties/{property_key} | Delete Entity Properties
[**evaluate_temporary_calculation_properties_api_v1_entities_id_calculation_properties_test_post**](EntitiesApi.md#evaluate_temporary_calculation_properties_api_v1_entities_id_calculation_properties_test_post) | **POST** /api/v1/entities/{id_}/calculationProperties/test | Evaluate Temporary Calculation Properties
[**get_aggregated_calculation_property_by_key_api_v1_entities_calculation_properties_get**](EntitiesApi.md#get_aggregated_calculation_property_by_key_api_v1_entities_calculation_properties_get) | **GET** /api/v1/entities/calculationProperties | Get Aggregated Calculation Property By Key
[**get_entities_api_v1_entities_get**](EntitiesApi.md#get_entities_api_v1_entities_get) | **GET** /api/v1/entities | Get Entities
[**get_entity_api_v1_entities_id_get**](EntitiesApi.md#get_entity_api_v1_entities_id_get) | **GET** /api/v1/entities/{id_} | Get Entity
[**get_entity_calculation_properties_api_v1_entities_id_calculation_properties_get**](EntitiesApi.md#get_entity_calculation_properties_api_v1_entities_id_calculation_properties_get) | **GET** /api/v1/entities/{id_}/calculationProperties | Get Entity Calculation Properties
[**get_entity_calculation_properties_by_key_api_v1_entities_id_calculation_properties_calculation_property_key_get**](EntitiesApi.md#get_entity_calculation_properties_by_key_api_v1_entities_id_calculation_properties_calculation_property_key_get) | **GET** /api/v1/entities/{id_}/calculationProperties/{calculation_property_key} | Get Entity Calculation Properties By Key
[**get_entity_properties_api_v1_entities_id_properties_get**](EntitiesApi.md#get_entity_properties_api_v1_entities_id_properties_get) | **GET** /api/v1/entities/{id_}/properties | Get Entity Properties
[**get_entity_property_api_v1_entities_id_properties_property_key_get**](EntitiesApi.md#get_entity_property_api_v1_entities_id_properties_property_key_get) | **GET** /api/v1/entities/{id_}/properties/{property_key} | Get Entity Property
[**get_relations_api_v1_entities_id_relations_get**](EntitiesApi.md#get_relations_api_v1_entities_id_relations_get) | **GET** /api/v1/entities/{id_}/relations | Get Relations
[**search_entities_api_v1_entities_search_post**](EntitiesApi.md#search_entities_api_v1_entities_search_post) | **POST** /api/v1/entities/search | Search Entities
[**update_entity_properties_api_v1_entities_id_properties_put**](EntitiesApi.md#update_entity_properties_api_v1_entities_id_properties_put) | **PUT** /api/v1/entities/{id_}/properties | Update Entity Properties
[**upsert_entity_api_v1_entities_id_put**](EntitiesApi.md#upsert_entity_api_v1_entities_id_put) | **PUT** /api/v1/entities/{id_} | Upsert Entity
[**validate_entity_api_v1_entities_validate_post**](EntitiesApi.md#validate_entity_api_v1_entities_validate_post) | **POST** /api/v1/entities/validate | Validate Entity


# **create_entity_api_v1_entities_post**
> EntityRead create_entity_api_v1_entities_post(entity_create)

Create Entity

### Example

* Bearer Authentication (bearerAuth):

```python
import magneto_api_client
from magneto_api_client.models.entity_create import EntityCreate
from magneto_api_client.models.entity_read import EntityRead
from magneto_api_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = magneto_api_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: bearerAuth
configuration = magneto_api_client.Configuration(
    access_token = os.environ["BEARER_TOKEN"]
)

# Enter a context with an instance of the API client
async with magneto_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = magneto_api_client.EntitiesApi(api_client)
    entity_create = magneto_api_client.EntityCreate() # EntityCreate | 

    try:
        # Create Entity
        api_response = await api_instance.create_entity_api_v1_entities_post(entity_create)
        print("The response of EntitiesApi->create_entity_api_v1_entities_post:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling EntitiesApi->create_entity_api_v1_entities_post: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **entity_create** | [**EntityCreate**](EntityCreate.md)|  | 

### Return type

[**EntityRead**](EntityRead.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful Response |  -  |
**422** | Validation Error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **create_entity_properties_api_v1_entities_id_properties_post**
> object create_entity_properties_api_v1_entities_id_properties_post(id_, body)

Create Entity Properties

### Example

* Bearer Authentication (bearerAuth):

```python
import magneto_api_client
from magneto_api_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = magneto_api_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: bearerAuth
configuration = magneto_api_client.Configuration(
    access_token = os.environ["BEARER_TOKEN"]
)

# Enter a context with an instance of the API client
async with magneto_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = magneto_api_client.EntitiesApi(api_client)
    id_ = 'id__example' # str | 
    body = None # object | 

    try:
        # Create Entity Properties
        api_response = await api_instance.create_entity_properties_api_v1_entities_id_properties_post(id_, body)
        print("The response of EntitiesApi->create_entity_properties_api_v1_entities_id_properties_post:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling EntitiesApi->create_entity_properties_api_v1_entities_id_properties_post: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id_** | **str**|  | 
 **body** | **object**|  | 

### Return type

**object**

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful Response |  -  |
**422** | Validation Error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **create_or_update_entities_bulk_api_v1_entities_bulk_put**
> List[EntityRead] create_or_update_entities_bulk_api_v1_entities_bulk_put(entity_update, patch=patch, unmapped_properties=unmapped_properties, update_properties_and_relations_only=update_properties_and_relations_only, create_relations=create_relations, override_sources=override_sources, remove_unused_sources=remove_unused_sources)

Create Or Update Entities Bulk

### Example

* Bearer Authentication (bearerAuth):

```python
import magneto_api_client
from magneto_api_client.models.entity_read import EntityRead
from magneto_api_client.models.entity_update import EntityUpdate
from magneto_api_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = magneto_api_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: bearerAuth
configuration = magneto_api_client.Configuration(
    access_token = os.environ["BEARER_TOKEN"]
)

# Enter a context with an instance of the API client
async with magneto_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = magneto_api_client.EntitiesApi(api_client)
    entity_update = [magneto_api_client.EntityUpdate()] # List[EntityUpdate] | 
    patch = False # bool | Patch entity instead of full update (optional) (default to False)
    unmapped_properties = magneto_api_client.UnmappedPropertyValue() # UnmappedPropertyValue |  (optional)
    update_properties_and_relations_only = False # bool | Update only properties and relations (optional) (default to False)
    create_relations = False # bool | Create entity relations if they dont exist (optional) (default to False)
    override_sources = False # bool | Allow updating entity data that have an active source (optional) (default to False)
    remove_unused_sources = False # bool | Remove all sources that are not in the request (optional) (default to False)

    try:
        # Create Or Update Entities Bulk
        api_response = await api_instance.create_or_update_entities_bulk_api_v1_entities_bulk_put(entity_update, patch=patch, unmapped_properties=unmapped_properties, update_properties_and_relations_only=update_properties_and_relations_only, create_relations=create_relations, override_sources=override_sources, remove_unused_sources=remove_unused_sources)
        print("The response of EntitiesApi->create_or_update_entities_bulk_api_v1_entities_bulk_put:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling EntitiesApi->create_or_update_entities_bulk_api_v1_entities_bulk_put: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **entity_update** | [**List[EntityUpdate]**](EntityUpdate.md)|  | 
 **patch** | **bool**| Patch entity instead of full update | [optional] [default to False]
 **unmapped_properties** | [**UnmappedPropertyValue**](.md)|  | [optional] 
 **update_properties_and_relations_only** | **bool**| Update only properties and relations | [optional] [default to False]
 **create_relations** | **bool**| Create entity relations if they dont exist | [optional] [default to False]
 **override_sources** | **bool**| Allow updating entity data that have an active source | [optional] [default to False]
 **remove_unused_sources** | **bool**| Remove all sources that are not in the request | [optional] [default to False]

### Return type

[**List[EntityRead]**](EntityRead.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**201** | Successful Response |  -  |
**422** | Validation Error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **delete_entity_api_v1_entities_id_delete**
> delete_entity_api_v1_entities_id_delete(id_)

Delete Entity

### Example

* Bearer Authentication (bearerAuth):

```python
import magneto_api_client
from magneto_api_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = magneto_api_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: bearerAuth
configuration = magneto_api_client.Configuration(
    access_token = os.environ["BEARER_TOKEN"]
)

# Enter a context with an instance of the API client
async with magneto_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = magneto_api_client.EntitiesApi(api_client)
    id_ = 'id__example' # str | 

    try:
        # Delete Entity
        await api_instance.delete_entity_api_v1_entities_id_delete(id_)
    except Exception as e:
        print("Exception when calling EntitiesApi->delete_entity_api_v1_entities_id_delete: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id_** | **str**|  | 

### Return type

void (empty response body)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**204** | Successful Response |  -  |
**422** | Validation Error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **delete_entity_properties_api_v1_entities_id_properties_property_key_delete**
> delete_entity_properties_api_v1_entities_id_properties_property_key_delete(id_, property_key)

Delete Entity Properties

### Example

* Bearer Authentication (bearerAuth):

```python
import magneto_api_client
from magneto_api_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = magneto_api_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: bearerAuth
configuration = magneto_api_client.Configuration(
    access_token = os.environ["BEARER_TOKEN"]
)

# Enter a context with an instance of the API client
async with magneto_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = magneto_api_client.EntitiesApi(api_client)
    id_ = 'id__example' # str | 
    property_key = 'property_key_example' # str | 

    try:
        # Delete Entity Properties
        await api_instance.delete_entity_properties_api_v1_entities_id_properties_property_key_delete(id_, property_key)
    except Exception as e:
        print("Exception when calling EntitiesApi->delete_entity_properties_api_v1_entities_id_properties_property_key_delete: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id_** | **str**|  | 
 **property_key** | **str**|  | 

### Return type

void (empty response body)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**204** | Successful Response |  -  |
**422** | Validation Error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **evaluate_temporary_calculation_properties_api_v1_entities_id_calculation_properties_test_post**
> Dict[str, EntityCalculationPropertyRead] evaluate_temporary_calculation_properties_api_v1_entities_id_calculation_properties_test_post(id_, request_body, time_interval=time_interval, time_range_start=time_range_start, time_range_end=time_range_end)

Evaluate Temporary Calculation Properties

### Example

* Bearer Authentication (bearerAuth):

```python
import magneto_api_client
from magneto_api_client.models.calculation_property import CalculationProperty
from magneto_api_client.models.entity_calculation_property_read import EntityCalculationPropertyRead
from magneto_api_client.models.time_interval import TimeInterval
from magneto_api_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = magneto_api_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: bearerAuth
configuration = magneto_api_client.Configuration(
    access_token = os.environ["BEARER_TOKEN"]
)

# Enter a context with an instance of the API client
async with magneto_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = magneto_api_client.EntitiesApi(api_client)
    id_ = 'id__example' # str | 
    request_body = {'key': magneto_api_client.CalculationProperty()} # Dict[str, CalculationProperty] | 
    time_interval = magneto_api_client.TimeInterval() # TimeInterval |  (optional)
    time_range_start = '2013-10-20T19:20:30+01:00' # datetime |  (optional)
    time_range_end = '2013-10-20T19:20:30+01:00' # datetime |  (optional)

    try:
        # Evaluate Temporary Calculation Properties
        api_response = await api_instance.evaluate_temporary_calculation_properties_api_v1_entities_id_calculation_properties_test_post(id_, request_body, time_interval=time_interval, time_range_start=time_range_start, time_range_end=time_range_end)
        print("The response of EntitiesApi->evaluate_temporary_calculation_properties_api_v1_entities_id_calculation_properties_test_post:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling EntitiesApi->evaluate_temporary_calculation_properties_api_v1_entities_id_calculation_properties_test_post: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id_** | **str**|  | 
 **request_body** | [**Dict[str, CalculationProperty]**](CalculationProperty.md)|  | 
 **time_interval** | [**TimeInterval**](.md)|  | [optional] 
 **time_range_start** | **datetime**|  | [optional] 
 **time_range_end** | **datetime**|  | [optional] 

### Return type

[**Dict[str, EntityCalculationPropertyRead]**](EntityCalculationPropertyRead.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful Response |  -  |
**422** | Validation Error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_aggregated_calculation_property_by_key_api_v1_entities_calculation_properties_get**
> Dict[str, EntityCalculationPropertyRead] get_aggregated_calculation_property_by_key_api_v1_entities_calculation_properties_get(blueprint_id, selected_properties=selected_properties, aggregation_function=aggregation_function, time_interval=time_interval, time_range_start=time_range_start, time_range_end=time_range_end, filters=filters, sorts=sorts, q=q)

Get Aggregated Calculation Property By Key

### Example

* Bearer Authentication (bearerAuth):

```python
import magneto_api_client
from magneto_api_client.models.entity_calculation_property_read import EntityCalculationPropertyRead
from magneto_api_client.models.time_interval import TimeInterval
from magneto_api_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = magneto_api_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: bearerAuth
configuration = magneto_api_client.Configuration(
    access_token = os.environ["BEARER_TOKEN"]
)

# Enter a context with an instance of the API client
async with magneto_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = magneto_api_client.EntitiesApi(api_client)
    blueprint_id = 'blueprint_id_example' # str | 
    selected_properties = ['selected_properties_example'] # List[str] |  (optional)
    aggregation_function = magneto_api_client.AggregationFunction() # AggregationFunction |  (optional)
    time_interval = magneto_api_client.TimeInterval() # TimeInterval |  (optional)
    time_range_start = '2013-10-20T19:20:30+01:00' # datetime |  (optional)
    time_range_end = '2013-10-20T19:20:30+01:00' # datetime |  (optional)
    filters = ['filters_example'] # List[str] |  (optional)
    sorts = ['sorts_example'] # List[str] |  (optional)
    q = 'q_example' # str |  (optional)

    try:
        # Get Aggregated Calculation Property By Key
        api_response = await api_instance.get_aggregated_calculation_property_by_key_api_v1_entities_calculation_properties_get(blueprint_id, selected_properties=selected_properties, aggregation_function=aggregation_function, time_interval=time_interval, time_range_start=time_range_start, time_range_end=time_range_end, filters=filters, sorts=sorts, q=q)
        print("The response of EntitiesApi->get_aggregated_calculation_property_by_key_api_v1_entities_calculation_properties_get:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling EntitiesApi->get_aggregated_calculation_property_by_key_api_v1_entities_calculation_properties_get: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **blueprint_id** | **str**|  | 
 **selected_properties** | [**List[str]**](str.md)|  | [optional] 
 **aggregation_function** | [**AggregationFunction**](.md)|  | [optional] 
 **time_interval** | [**TimeInterval**](.md)|  | [optional] 
 **time_range_start** | **datetime**|  | [optional] 
 **time_range_end** | **datetime**|  | [optional] 
 **filters** | [**List[str]**](str.md)|  | [optional] 
 **sorts** | [**List[str]**](str.md)|  | [optional] 
 **q** | **str**|  | [optional] 

### Return type

[**Dict[str, EntityCalculationPropertyRead]**](EntityCalculationPropertyRead.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful Response |  -  |
**422** | Validation Error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_entities_api_v1_entities_get**
> CursorPageEntityReadExtended get_entities_api_v1_entities_get(blueprint_ids=blueprint_ids, exclude_reference_properties=exclude_reference_properties, include_calculation_properties=include_calculation_properties, search_query=search_query, show_hidden=show_hidden, filters=filters, sorts=sorts, q=q, cursor=cursor, size=size, include_total=include_total)

Get Entities

### Example

* Bearer Authentication (bearerAuth):

```python
import magneto_api_client
from magneto_api_client.models.cursor_page_entity_read_extended import CursorPageEntityReadExtended
from magneto_api_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = magneto_api_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: bearerAuth
configuration = magneto_api_client.Configuration(
    access_token = os.environ["BEARER_TOKEN"]
)

# Enter a context with an instance of the API client
async with magneto_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = magneto_api_client.EntitiesApi(api_client)
    blueprint_ids = ['blueprint_ids_example'] # List[str] |  (optional)
    exclude_reference_properties = False # bool |  (optional) (default to False)
    include_calculation_properties = False # bool |  (optional) (default to False)
    search_query = 'search_query_example' # str |  (optional)
    show_hidden = False # bool |  (optional) (default to False)
    filters = ['filters_example'] # List[str] |  (optional)
    sorts = ['sorts_example'] # List[str] |  (optional)
    q = 'q_example' # str |  (optional)
    cursor = 'cursor_example' # str |  (optional)
    size = 50 # int |  (optional) (default to 50)
    include_total = False # bool |  (optional) (default to False)

    try:
        # Get Entities
        api_response = await api_instance.get_entities_api_v1_entities_get(blueprint_ids=blueprint_ids, exclude_reference_properties=exclude_reference_properties, include_calculation_properties=include_calculation_properties, search_query=search_query, show_hidden=show_hidden, filters=filters, sorts=sorts, q=q, cursor=cursor, size=size, include_total=include_total)
        print("The response of EntitiesApi->get_entities_api_v1_entities_get:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling EntitiesApi->get_entities_api_v1_entities_get: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **blueprint_ids** | [**List[str]**](str.md)|  | [optional] 
 **exclude_reference_properties** | **bool**|  | [optional] [default to False]
 **include_calculation_properties** | **bool**|  | [optional] [default to False]
 **search_query** | **str**|  | [optional] 
 **show_hidden** | **bool**|  | [optional] [default to False]
 **filters** | [**List[str]**](str.md)|  | [optional] 
 **sorts** | [**List[str]**](str.md)|  | [optional] 
 **q** | **str**|  | [optional] 
 **cursor** | **str**|  | [optional] 
 **size** | **int**|  | [optional] [default to 50]
 **include_total** | **bool**|  | [optional] [default to False]

### Return type

[**CursorPageEntityReadExtended**](CursorPageEntityReadExtended.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful Response |  -  |
**422** | Validation Error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_entity_api_v1_entities_id_get**
> EntityReadExtended get_entity_api_v1_entities_id_get(id_, exclude_reference_properties=exclude_reference_properties, include_calculation_properties=include_calculation_properties, fetch_live_properties=fetch_live_properties)

Get Entity

### Example

* Bearer Authentication (bearerAuth):

```python
import magneto_api_client
from magneto_api_client.models.entity_read_extended import EntityReadExtended
from magneto_api_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = magneto_api_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: bearerAuth
configuration = magneto_api_client.Configuration(
    access_token = os.environ["BEARER_TOKEN"]
)

# Enter a context with an instance of the API client
async with magneto_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = magneto_api_client.EntitiesApi(api_client)
    id_ = 'id__example' # str | 
    exclude_reference_properties = False # bool |  (optional) (default to False)
    include_calculation_properties = False # bool |  (optional) (default to False)
    fetch_live_properties = False # bool | Fetch live properties data synchronously. (optional) (default to False)

    try:
        # Get Entity
        api_response = await api_instance.get_entity_api_v1_entities_id_get(id_, exclude_reference_properties=exclude_reference_properties, include_calculation_properties=include_calculation_properties, fetch_live_properties=fetch_live_properties)
        print("The response of EntitiesApi->get_entity_api_v1_entities_id_get:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling EntitiesApi->get_entity_api_v1_entities_id_get: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id_** | **str**|  | 
 **exclude_reference_properties** | **bool**|  | [optional] [default to False]
 **include_calculation_properties** | **bool**|  | [optional] [default to False]
 **fetch_live_properties** | **bool**| Fetch live properties data synchronously. | [optional] [default to False]

### Return type

[**EntityReadExtended**](EntityReadExtended.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful Response |  -  |
**422** | Validation Error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_entity_calculation_properties_api_v1_entities_id_calculation_properties_get**
> Dict[str, EntityCalculationPropertyRead] get_entity_calculation_properties_api_v1_entities_id_calculation_properties_get(id_, selected_properties=selected_properties, time_interval=time_interval, time_range_start=time_range_start, time_range_end=time_range_end)

Get Entity Calculation Properties

### Example

* Bearer Authentication (bearerAuth):

```python
import magneto_api_client
from magneto_api_client.models.entity_calculation_property_read import EntityCalculationPropertyRead
from magneto_api_client.models.time_interval import TimeInterval
from magneto_api_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = magneto_api_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: bearerAuth
configuration = magneto_api_client.Configuration(
    access_token = os.environ["BEARER_TOKEN"]
)

# Enter a context with an instance of the API client
async with magneto_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = magneto_api_client.EntitiesApi(api_client)
    id_ = 'id__example' # str | 
    selected_properties = ['selected_properties_example'] # List[str] |  (optional)
    time_interval = magneto_api_client.TimeInterval() # TimeInterval |  (optional)
    time_range_start = '2013-10-20T19:20:30+01:00' # datetime |  (optional)
    time_range_end = '2013-10-20T19:20:30+01:00' # datetime |  (optional)

    try:
        # Get Entity Calculation Properties
        api_response = await api_instance.get_entity_calculation_properties_api_v1_entities_id_calculation_properties_get(id_, selected_properties=selected_properties, time_interval=time_interval, time_range_start=time_range_start, time_range_end=time_range_end)
        print("The response of EntitiesApi->get_entity_calculation_properties_api_v1_entities_id_calculation_properties_get:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling EntitiesApi->get_entity_calculation_properties_api_v1_entities_id_calculation_properties_get: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id_** | **str**|  | 
 **selected_properties** | [**List[str]**](str.md)|  | [optional] 
 **time_interval** | [**TimeInterval**](.md)|  | [optional] 
 **time_range_start** | **datetime**|  | [optional] 
 **time_range_end** | **datetime**|  | [optional] 

### Return type

[**Dict[str, EntityCalculationPropertyRead]**](EntityCalculationPropertyRead.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful Response |  -  |
**422** | Validation Error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_entity_calculation_properties_by_key_api_v1_entities_id_calculation_properties_calculation_property_key_get**
> Dict[str, EntityCalculationPropertyRead] get_entity_calculation_properties_by_key_api_v1_entities_id_calculation_properties_calculation_property_key_get(id_, calculation_property_key, time_interval=time_interval, time_range_start=time_range_start, time_range_end=time_range_end)

Get Entity Calculation Properties By Key

### Example

* Bearer Authentication (bearerAuth):

```python
import magneto_api_client
from magneto_api_client.models.entity_calculation_property_read import EntityCalculationPropertyRead
from magneto_api_client.models.time_interval import TimeInterval
from magneto_api_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = magneto_api_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: bearerAuth
configuration = magneto_api_client.Configuration(
    access_token = os.environ["BEARER_TOKEN"]
)

# Enter a context with an instance of the API client
async with magneto_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = magneto_api_client.EntitiesApi(api_client)
    id_ = 'id__example' # str | 
    calculation_property_key = 'calculation_property_key_example' # str | 
    time_interval = magneto_api_client.TimeInterval() # TimeInterval |  (optional)
    time_range_start = '2013-10-20T19:20:30+01:00' # datetime |  (optional)
    time_range_end = '2013-10-20T19:20:30+01:00' # datetime |  (optional)

    try:
        # Get Entity Calculation Properties By Key
        api_response = await api_instance.get_entity_calculation_properties_by_key_api_v1_entities_id_calculation_properties_calculation_property_key_get(id_, calculation_property_key, time_interval=time_interval, time_range_start=time_range_start, time_range_end=time_range_end)
        print("The response of EntitiesApi->get_entity_calculation_properties_by_key_api_v1_entities_id_calculation_properties_calculation_property_key_get:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling EntitiesApi->get_entity_calculation_properties_by_key_api_v1_entities_id_calculation_properties_calculation_property_key_get: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id_** | **str**|  | 
 **calculation_property_key** | **str**|  | 
 **time_interval** | [**TimeInterval**](.md)|  | [optional] 
 **time_range_start** | **datetime**|  | [optional] 
 **time_range_end** | **datetime**|  | [optional] 

### Return type

[**Dict[str, EntityCalculationPropertyRead]**](EntityCalculationPropertyRead.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful Response |  -  |
**422** | Validation Error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_entity_properties_api_v1_entities_id_properties_get**
> object get_entity_properties_api_v1_entities_id_properties_get(id_, fetch_live_properties=fetch_live_properties, live_properties_only=live_properties_only)

Get Entity Properties

### Example

* Bearer Authentication (bearerAuth):

```python
import magneto_api_client
from magneto_api_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = magneto_api_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: bearerAuth
configuration = magneto_api_client.Configuration(
    access_token = os.environ["BEARER_TOKEN"]
)

# Enter a context with an instance of the API client
async with magneto_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = magneto_api_client.EntitiesApi(api_client)
    id_ = 'id__example' # str | 
    fetch_live_properties = False # bool | Fetch live properties data synchronously. (optional) (default to False)
    live_properties_only = False # bool | Get only live properties. (optional) (default to False)

    try:
        # Get Entity Properties
        api_response = await api_instance.get_entity_properties_api_v1_entities_id_properties_get(id_, fetch_live_properties=fetch_live_properties, live_properties_only=live_properties_only)
        print("The response of EntitiesApi->get_entity_properties_api_v1_entities_id_properties_get:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling EntitiesApi->get_entity_properties_api_v1_entities_id_properties_get: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id_** | **str**|  | 
 **fetch_live_properties** | **bool**| Fetch live properties data synchronously. | [optional] [default to False]
 **live_properties_only** | **bool**| Get only live properties. | [optional] [default to False]

### Return type

**object**

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful Response |  -  |
**422** | Validation Error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_entity_property_api_v1_entities_id_properties_property_key_get**
> object get_entity_property_api_v1_entities_id_properties_property_key_get(id_, property_key, fetch_live_properties=fetch_live_properties)

Get Entity Property

### Example

* Bearer Authentication (bearerAuth):

```python
import magneto_api_client
from magneto_api_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = magneto_api_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: bearerAuth
configuration = magneto_api_client.Configuration(
    access_token = os.environ["BEARER_TOKEN"]
)

# Enter a context with an instance of the API client
async with magneto_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = magneto_api_client.EntitiesApi(api_client)
    id_ = 'id__example' # str | 
    property_key = 'property_key_example' # str | 
    fetch_live_properties = False # bool | Fetch live properties data synchronously. (optional) (default to False)

    try:
        # Get Entity Property
        api_response = await api_instance.get_entity_property_api_v1_entities_id_properties_property_key_get(id_, property_key, fetch_live_properties=fetch_live_properties)
        print("The response of EntitiesApi->get_entity_property_api_v1_entities_id_properties_property_key_get:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling EntitiesApi->get_entity_property_api_v1_entities_id_properties_property_key_get: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id_** | **str**|  | 
 **property_key** | **str**|  | 
 **fetch_live_properties** | **bool**| Fetch live properties data synchronously. | [optional] [default to False]

### Return type

**object**

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful Response |  -  |
**422** | Validation Error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_relations_api_v1_entities_id_relations_get**
> CursorPageAny get_relations_api_v1_entities_id_relations_get(id_, sort_by=sort_by, cursor=cursor, size=size, include_total=include_total)

Get Relations

### Example

* Bearer Authentication (bearerAuth):

```python
import magneto_api_client
from magneto_api_client.models.cursor_page_any import CursorPageAny
from magneto_api_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = magneto_api_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: bearerAuth
configuration = magneto_api_client.Configuration(
    access_token = os.environ["BEARER_TOKEN"]
)

# Enter a context with an instance of the API client
async with magneto_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = magneto_api_client.EntitiesApi(api_client)
    id_ = 'id__example' # str | 
    sort_by = 'sort_by_example' # str |  (optional)
    cursor = 'cursor_example' # str |  (optional)
    size = 50 # int |  (optional) (default to 50)
    include_total = False # bool |  (optional) (default to False)

    try:
        # Get Relations
        api_response = await api_instance.get_relations_api_v1_entities_id_relations_get(id_, sort_by=sort_by, cursor=cursor, size=size, include_total=include_total)
        print("The response of EntitiesApi->get_relations_api_v1_entities_id_relations_get:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling EntitiesApi->get_relations_api_v1_entities_id_relations_get: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id_** | **str**|  | 
 **sort_by** | **str**|  | [optional] 
 **cursor** | **str**|  | [optional] 
 **size** | **int**|  | [optional] [default to 50]
 **include_total** | **bool**|  | [optional] [default to False]

### Return type

[**CursorPageAny**](CursorPageAny.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful Response |  -  |
**422** | Validation Error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **search_entities_api_v1_entities_search_post**
> CursorPageEntityReadExtended search_entities_api_v1_entities_search_post(entity_search_query, exclude_reference_properties=exclude_reference_properties, include_calculation_properties=include_calculation_properties, show_hidden=show_hidden, cursor=cursor, size=size, include_total=include_total)

Search Entities

### Example

* Bearer Authentication (bearerAuth):

```python
import magneto_api_client
from magneto_api_client.models.cursor_page_entity_read_extended import CursorPageEntityReadExtended
from magneto_api_client.models.entity_search_query import EntitySearchQuery
from magneto_api_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = magneto_api_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: bearerAuth
configuration = magneto_api_client.Configuration(
    access_token = os.environ["BEARER_TOKEN"]
)

# Enter a context with an instance of the API client
async with magneto_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = magneto_api_client.EntitiesApi(api_client)
    entity_search_query = magneto_api_client.EntitySearchQuery() # EntitySearchQuery | 
    exclude_reference_properties = False # bool |  (optional) (default to False)
    include_calculation_properties = False # bool |  (optional) (default to False)
    show_hidden = False # bool |  (optional) (default to False)
    cursor = 'cursor_example' # str |  (optional)
    size = 50 # int |  (optional) (default to 50)
    include_total = False # bool |  (optional) (default to False)

    try:
        # Search Entities
        api_response = await api_instance.search_entities_api_v1_entities_search_post(entity_search_query, exclude_reference_properties=exclude_reference_properties, include_calculation_properties=include_calculation_properties, show_hidden=show_hidden, cursor=cursor, size=size, include_total=include_total)
        print("The response of EntitiesApi->search_entities_api_v1_entities_search_post:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling EntitiesApi->search_entities_api_v1_entities_search_post: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **entity_search_query** | [**EntitySearchQuery**](EntitySearchQuery.md)|  | 
 **exclude_reference_properties** | **bool**|  | [optional] [default to False]
 **include_calculation_properties** | **bool**|  | [optional] [default to False]
 **show_hidden** | **bool**|  | [optional] [default to False]
 **cursor** | **str**|  | [optional] 
 **size** | **int**|  | [optional] [default to 50]
 **include_total** | **bool**|  | [optional] [default to False]

### Return type

[**CursorPageEntityReadExtended**](CursorPageEntityReadExtended.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful Response |  -  |
**422** | Validation Error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **update_entity_properties_api_v1_entities_id_properties_put**
> object update_entity_properties_api_v1_entities_id_properties_put(id_, body)

Update Entity Properties

### Example

* Bearer Authentication (bearerAuth):

```python
import magneto_api_client
from magneto_api_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = magneto_api_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: bearerAuth
configuration = magneto_api_client.Configuration(
    access_token = os.environ["BEARER_TOKEN"]
)

# Enter a context with an instance of the API client
async with magneto_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = magneto_api_client.EntitiesApi(api_client)
    id_ = 'id__example' # str | 
    body = None # object | 

    try:
        # Update Entity Properties
        api_response = await api_instance.update_entity_properties_api_v1_entities_id_properties_put(id_, body)
        print("The response of EntitiesApi->update_entity_properties_api_v1_entities_id_properties_put:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling EntitiesApi->update_entity_properties_api_v1_entities_id_properties_put: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id_** | **str**|  | 
 **body** | **object**|  | 

### Return type

**object**

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful Response |  -  |
**422** | Validation Error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **upsert_entity_api_v1_entities_id_put**
> EntityRead upsert_entity_api_v1_entities_id_put(id_, entity_update, patch=patch, unmapped_properties=unmapped_properties, update_properties_and_relations_only=update_properties_and_relations_only, override_sources=override_sources)

Upsert Entity

### Example

* Bearer Authentication (bearerAuth):

```python
import magneto_api_client
from magneto_api_client.models.entity_read import EntityRead
from magneto_api_client.models.entity_update import EntityUpdate
from magneto_api_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = magneto_api_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: bearerAuth
configuration = magneto_api_client.Configuration(
    access_token = os.environ["BEARER_TOKEN"]
)

# Enter a context with an instance of the API client
async with magneto_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = magneto_api_client.EntitiesApi(api_client)
    id_ = 'id__example' # str | 
    entity_update = magneto_api_client.EntityUpdate() # EntityUpdate | 
    patch = False # bool | Patch entity instead of full update (optional) (default to False)
    unmapped_properties = magneto_api_client.UnmappedPropertyValue() # UnmappedPropertyValue |  (optional)
    update_properties_and_relations_only = False # bool | Update only properties and relations (optional) (default to False)
    override_sources = False # bool | Allow updating entity data that have an active source (optional) (default to False)

    try:
        # Upsert Entity
        api_response = await api_instance.upsert_entity_api_v1_entities_id_put(id_, entity_update, patch=patch, unmapped_properties=unmapped_properties, update_properties_and_relations_only=update_properties_and_relations_only, override_sources=override_sources)
        print("The response of EntitiesApi->upsert_entity_api_v1_entities_id_put:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling EntitiesApi->upsert_entity_api_v1_entities_id_put: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id_** | **str**|  | 
 **entity_update** | [**EntityUpdate**](EntityUpdate.md)|  | 
 **patch** | **bool**| Patch entity instead of full update | [optional] [default to False]
 **unmapped_properties** | [**UnmappedPropertyValue**](.md)|  | [optional] 
 **update_properties_and_relations_only** | **bool**| Update only properties and relations | [optional] [default to False]
 **override_sources** | **bool**| Allow updating entity data that have an active source | [optional] [default to False]

### Return type

[**EntityRead**](EntityRead.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful Response |  -  |
**422** | Validation Error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **validate_entity_api_v1_entities_validate_post**
> EntityValidationResponse validate_entity_api_v1_entities_validate_post(entity_create)

Validate Entity

### Example

* Bearer Authentication (bearerAuth):

```python
import magneto_api_client
from magneto_api_client.models.entity_create import EntityCreate
from magneto_api_client.models.entity_validation_response import EntityValidationResponse
from magneto_api_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = magneto_api_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: bearerAuth
configuration = magneto_api_client.Configuration(
    access_token = os.environ["BEARER_TOKEN"]
)

# Enter a context with an instance of the API client
async with magneto_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = magneto_api_client.EntitiesApi(api_client)
    entity_create = magneto_api_client.EntityCreate() # EntityCreate | 

    try:
        # Validate Entity
        api_response = await api_instance.validate_entity_api_v1_entities_validate_post(entity_create)
        print("The response of EntitiesApi->validate_entity_api_v1_entities_validate_post:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling EntitiesApi->validate_entity_api_v1_entities_validate_post: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **entity_create** | [**EntityCreate**](EntityCreate.md)|  | 

### Return type

[**EntityValidationResponse**](EntityValidationResponse.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful Response |  -  |
**422** | Validation Error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

