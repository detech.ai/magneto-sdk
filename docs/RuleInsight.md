# RuleInsight

Insight information about a specific rule.

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** |  | 
**results** | [**RuleResults**](RuleResults.md) |  | [optional] 

## Example

```python
from magneto_api_client.models.rule_insight import RuleInsight

# TODO update the JSON string below
json = "{}"
# create an instance of RuleInsight from a JSON string
rule_insight_instance = RuleInsight.from_json(json)
# print the JSON string representation of the object
print(RuleInsight.to_json())

# convert the object into a dict
rule_insight_dict = rule_insight_instance.to_dict()
# create an instance of RuleInsight from a dict
rule_insight_from_dict = RuleInsight.from_dict(rule_insight_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


