# ScorecardRead

Scorecard Read Model.

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** |  | [optional] 
**title** | **str** |  | 
**description** | **str** |  | [optional] 
**is_active** | **bool** |  | [optional] [default to True]
**blueprint_id** | **str** |  | 
**ranks** | [**List[ScorecardRank]**](ScorecardRank.md) |  | [optional] 
**median_rank** | **str** |  | [optional] 
**created_at** | **datetime** |  | 
**created_by** | **int** |  | [optional] 
**updated_at** | **datetime** |  | 
**updated_by** | **int** |  | [optional] 

## Example

```python
from magneto_api_client.models.scorecard_read import ScorecardRead

# TODO update the JSON string below
json = "{}"
# create an instance of ScorecardRead from a JSON string
scorecard_read_instance = ScorecardRead.from_json(json)
# print the JSON string representation of the object
print(ScorecardRead.to_json())

# convert the object into a dict
scorecard_read_dict = scorecard_read_instance.to_dict()
# create an instance of ScorecardRead from a dict
scorecard_read_from_dict = ScorecardRead.from_dict(scorecard_read_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


