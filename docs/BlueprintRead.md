# BlueprintRead

Base class for models that have audit columns.

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** |  | 
**title** | **str** |  | 
**description** | **str** |  | [optional] 
**icon** | **str** |  | [optional] 
**schema_properties** | [**SchemaProperties**](SchemaProperties.md) |  | [optional] 
**reference_properties** | [**Dict[str, ReferenceProperty]**](ReferenceProperty.md) |  | [optional] 
**calculation_properties** | [**Dict[str, CalculationProperty]**](CalculationProperty.md) |  | [optional] 
**relations** | [**Dict[str, BlueprintRelation]**](BlueprintRelation.md) |  | [optional] 
**options** | [**BlueprintOptions**](BlueprintOptions.md) |  | [optional] 
**is_active** | **bool** |  | [optional] [default to True]
**is_hideable** | **bool** |  | [optional] [default to True]
**created_at** | **datetime** |  | [optional] 
**created_by** | **int** |  | [optional] 
**updated_at** | **datetime** |  | [optional] 
**updated_by** | **int** |  | [optional] 
**user_editable** | **bool** |  | 
**user_deletable** | **bool** |  | 

## Example

```python
from magneto_api_client.models.blueprint_read import BlueprintRead

# TODO update the JSON string below
json = "{}"
# create an instance of BlueprintRead from a JSON string
blueprint_read_instance = BlueprintRead.from_json(json)
# print the JSON string representation of the object
print(BlueprintRead.to_json())

# convert the object into a dict
blueprint_read_dict = blueprint_read_instance.to_dict()
# create an instance of BlueprintRead from a dict
blueprint_read_from_dict = BlueprintRead.from_dict(blueprint_read_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


