# FetchStatus


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | 
**label** | **str** |  | [optional] 
**run_info** | **str** |  | [optional] 
**last_fetched_at** | **datetime** |  | [optional] 
**last_fetched_success_at** | **datetime** |  | [optional] 

## Example

```python
from magneto_api_client.models.fetch_status import FetchStatus

# TODO update the JSON string below
json = "{}"
# create an instance of FetchStatus from a JSON string
fetch_status_instance = FetchStatus.from_json(json)
# print the JSON string representation of the object
print(FetchStatus.to_json())

# convert the object into a dict
fetch_status_dict = fetch_status_instance.to_dict()
# create an instance of FetchStatus from a dict
fetch_status_from_dict = FetchStatus.from_dict(fetch_status_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


