# PluginLegacyUpdate


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **str** |  | 
**source_key** | **str** |  | [optional] 
**status** | **int** |  | [optional] [default to 1]
**customer_managed** | **bool** |  | [optional] [default to False]
**assets_for_discovery** | **List[str]** |  | [optional] 

## Example

```python
from magneto_api_client.models.plugin_legacy_update import PluginLegacyUpdate

# TODO update the JSON string below
json = "{}"
# create an instance of PluginLegacyUpdate from a JSON string
plugin_legacy_update_instance = PluginLegacyUpdate.from_json(json)
# print the JSON string representation of the object
print(PluginLegacyUpdate.to_json())

# convert the object into a dict
plugin_legacy_update_dict = plugin_legacy_update_instance.to_dict()
# create an instance of PluginLegacyUpdate from a dict
plugin_legacy_update_from_dict = PluginLegacyUpdate.from_dict(plugin_legacy_update_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


