# magneto_api_client.HealthCheckApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**healthcheck_api_v1_health_get**](HealthCheckApi.md#healthcheck_api_v1_health_get) | **GET** /api/v1/health | Perform a Health Check
[**healthcheck_velini_api_v1_health_velini_get**](HealthCheckApi.md#healthcheck_velini_api_v1_health_velini_get) | **GET** /api/v1/health/velini | Perform a Health Check by Velini


# **healthcheck_api_v1_health_get**
> object healthcheck_api_v1_health_get(status_code=status_code, detail=detail)

Perform a Health Check

### Example

* Bearer Authentication (bearerAuth):

```python
import magneto_api_client
from magneto_api_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = magneto_api_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: bearerAuth
configuration = magneto_api_client.Configuration(
    access_token = os.environ["BEARER_TOKEN"]
)

# Enter a context with an instance of the API client
async with magneto_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = magneto_api_client.HealthCheckApi(api_client)
    status_code = 503 # int |  (optional) (default to 503)
    detail = 'Service Unavailable' # str |  (optional) (default to 'Service Unavailable')

    try:
        # Perform a Health Check
        api_response = await api_instance.healthcheck_api_v1_health_get(status_code=status_code, detail=detail)
        print("The response of HealthCheckApi->healthcheck_api_v1_health_get:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling HealthCheckApi->healthcheck_api_v1_health_get: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **status_code** | **int**|  | [optional] [default to 503]
 **detail** | **str**|  | [optional] [default to &#39;Service Unavailable&#39;]

### Return type

**object**

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Return HTTP Status Code 200 (OK) if the service is up and running |  -  |
**422** | Validation Error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **healthcheck_velini_api_v1_health_velini_get**
> object healthcheck_velini_api_v1_health_velini_get(status_code=status_code, detail=detail)

Perform a Health Check by Velini

### Example

* Bearer Authentication (bearerAuth):

```python
import magneto_api_client
from magneto_api_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = magneto_api_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: bearerAuth
configuration = magneto_api_client.Configuration(
    access_token = os.environ["BEARER_TOKEN"]
)

# Enter a context with an instance of the API client
async with magneto_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = magneto_api_client.HealthCheckApi(api_client)
    status_code = 503 # int |  (optional) (default to 503)
    detail = 'Service Unavailable' # str |  (optional) (default to 'Service Unavailable')

    try:
        # Perform a Health Check by Velini
        api_response = await api_instance.healthcheck_velini_api_v1_health_velini_get(status_code=status_code, detail=detail)
        print("The response of HealthCheckApi->healthcheck_velini_api_v1_health_velini_get:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling HealthCheckApi->healthcheck_velini_api_v1_health_velini_get: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **status_code** | **int**|  | [optional] [default to 503]
 **detail** | **str**|  | [optional] [default to &#39;Service Unavailable&#39;]

### Return type

**object**

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Return HTTP Status Code 500 (NOT OK) if the service is up and running |  -  |
**422** | Validation Error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

