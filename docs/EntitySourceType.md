# EntitySourceType

An enumeration.

## Enum

* `AUTOMATION` (value: `'automation'`)

* `REFERENCE` (value: `'reference'`)

* `API` (value: `'api'`)

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


