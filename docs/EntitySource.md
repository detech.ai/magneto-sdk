# EntitySource


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**type** | [**EntitySourceType**](EntitySourceType.md) |  | 
**config** | **object** |  | [optional] 
**active_mappings** | **Dict[str, str]** |  | [optional] 

## Example

```python
from magneto_api_client.models.entity_source import EntitySource

# TODO update the JSON string below
json = "{}"
# create an instance of EntitySource from a JSON string
entity_source_instance = EntitySource.from_json(json)
# print the JSON string representation of the object
print(EntitySource.to_json())

# convert the object into a dict
entity_source_dict = entity_source_instance.to_dict()
# create an instance of EntitySource from a dict
entity_source_from_dict = EntitySource.from_dict(entity_source_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


