# EntityScorecardsInsights

Insights about scorecards for a specific entity.

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**scorecard_count** | **int** |  | 
**scorecard_ids** | **List[str]** |  | 
**median_rank** | **str** |  | 
**gold_count** | **int** |  | 
**silver_count** | **int** |  | 
**bronze_count** | **int** |  | 
**no_rank_count** | **int** |  | 
**no_data_count** | **int** |  | 

## Example

```python
from magneto_api_client.models.entity_scorecards_insights import EntityScorecardsInsights

# TODO update the JSON string below
json = "{}"
# create an instance of EntityScorecardsInsights from a JSON string
entity_scorecards_insights_instance = EntityScorecardsInsights.from_json(json)
# print the JSON string representation of the object
print(EntityScorecardsInsights.to_json())

# convert the object into a dict
entity_scorecards_insights_dict = entity_scorecards_insights_instance.to_dict()
# create an instance of EntityScorecardsInsights from a dict
entity_scorecards_insights_from_dict = EntityScorecardsInsights.from_dict(entity_scorecards_insights_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


