# ScorecardResultRead

Scorecard Result Read Model.

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**scorecard_id** | **str** |  | 
**entity_id** | **str** |  | 
**current_rank** | **str** |  | 
**rules** | [**Dict[str, ScorecardResultRule]**](ScorecardResultRule.md) |  | 
**updated_at** | **datetime** |  | 

## Example

```python
from magneto_api_client.models.scorecard_result_read import ScorecardResultRead

# TODO update the JSON string below
json = "{}"
# create an instance of ScorecardResultRead from a JSON string
scorecard_result_read_instance = ScorecardResultRead.from_json(json)
# print the JSON string representation of the object
print(ScorecardResultRead.to_json())

# convert the object into a dict
scorecard_result_read_dict = scorecard_result_read_instance.to_dict()
# create an instance of ScorecardResultRead from a dict
scorecard_result_read_from_dict = ScorecardResultRead.from_dict(scorecard_result_read_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


