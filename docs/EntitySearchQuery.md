# EntitySearchQuery


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**filters** | **List[str]** |  | [optional] 
**sorts** | **List[str]** |  | [optional] 
**search_query** | **str** |  | [optional] 

## Example

```python
from magneto_api_client.models.entity_search_query import EntitySearchQuery

# TODO update the JSON string below
json = "{}"
# create an instance of EntitySearchQuery from a JSON string
entity_search_query_instance = EntitySearchQuery.from_json(json)
# print the JSON string representation of the object
print(EntitySearchQuery.to_json())

# convert the object into a dict
entity_search_query_dict = entity_search_query_instance.to_dict()
# create an instance of EntitySearchQuery from a dict
entity_search_query_from_dict = EntitySearchQuery.from_dict(entity_search_query_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


