# TaskErrorInfo


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**type** | **str** |  | 
**message** | **str** |  | 
**details** | **object** |  | [optional] 

## Example

```python
from magneto_api_client.models.task_error_info import TaskErrorInfo

# TODO update the JSON string below
json = "{}"
# create an instance of TaskErrorInfo from a JSON string
task_error_info_instance = TaskErrorInfo.from_json(json)
# print the JSON string representation of the object
print(TaskErrorInfo.to_json())

# convert the object into a dict
task_error_info_dict = task_error_info_instance.to_dict()
# create an instance of TaskErrorInfo from a dict
task_error_info_from_dict = TaskErrorInfo.from_dict(task_error_info_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


