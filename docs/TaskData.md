# TaskData


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**payload** | **object** |  | [optional] 
**parameters** | **object** |  | [optional] 

## Example

```python
from magneto_api_client.models.task_data import TaskData

# TODO update the JSON string below
json = "{}"
# create an instance of TaskData from a JSON string
task_data_instance = TaskData.from_json(json)
# print the JSON string representation of the object
print(TaskData.to_json())

# convert the object into a dict
task_data_dict = task_data_instance.to_dict()
# create an instance of TaskData from a dict
task_data_from_dict = TaskData.from_dict(task_data_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


