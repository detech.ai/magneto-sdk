# TimeseriesWrite


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**bucket** | **str** | The timeseries bucket where data is stored in the database | 

## Example

```python
from magneto_api_client.models.timeseries_write import TimeseriesWrite

# TODO update the JSON string below
json = "{}"
# create an instance of TimeseriesWrite from a JSON string
timeseries_write_instance = TimeseriesWrite.from_json(json)
# print the JSON string representation of the object
print(TimeseriesWrite.to_json())

# convert the object into a dict
timeseries_write_dict = timeseries_write_instance.to_dict()
# create an instance of TimeseriesWrite from a dict
timeseries_write_from_dict = TimeseriesWrite.from_dict(timeseries_write_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


