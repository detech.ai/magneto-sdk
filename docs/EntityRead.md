# EntityRead

Entity Read Model.

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** |  | 
**blueprint_id** | **str** |  | 
**title** | **str** |  | 
**description** | **str** |  | [optional] 
**is_active** | **bool** |  | [optional] [default to True]
**is_hidden** | **bool** |  | [optional] [default to False]
**properties** | **object** |  | [optional] 
**relations** | [**Dict[str, EntityRelation]**](EntityRelation.md) |  | [optional] 
**sources** | [**Dict[str, EntitySource]**](EntitySource.md) |  | [optional] 
**created_at** | **datetime** |  | 
**created_by** | **int** |  | [optional] 
**updated_at** | **datetime** |  | 
**updated_by** | **int** |  | [optional] 

## Example

```python
from magneto_api_client.models.entity_read import EntityRead

# TODO update the JSON string below
json = "{}"
# create an instance of EntityRead from a JSON string
entity_read_instance = EntityRead.from_json(json)
# print the JSON string representation of the object
print(EntityRead.to_json())

# convert the object into a dict
entity_read_dict = entity_read_instance.to_dict()
# create an instance of EntityRead from a dict
entity_read_from_dict = EntityRead.from_dict(entity_read_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


