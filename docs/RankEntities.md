# RankEntities

Statistics about entities in a specific rank.

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**count** | **int** |  | [optional] [default to 0]
**percentage** | **float** |  | [optional] [default to 0]
**items** | **List[str]** |  | [optional] 

## Example

```python
from magneto_api_client.models.rank_entities import RankEntities

# TODO update the JSON string below
json = "{}"
# create an instance of RankEntities from a JSON string
rank_entities_instance = RankEntities.from_json(json)
# print the JSON string representation of the object
print(RankEntities.to_json())

# convert the object into a dict
rank_entities_dict = rank_entities_instance.to_dict()
# create an instance of RankEntities from a dict
rank_entities_from_dict = RankEntities.from_dict(rank_entities_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


