# ScorecardResultRule

Base class for models that exclude unset fields.  **Note**: This overrides any `exclude_unset` kwarg passed for the model and its nested models.

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**result** | **bool** |  | 
**context** | **object** |  | [optional] 
**description** | **str** |  | [optional] 
**title** | **str** |  | [optional] 
**rank** | **str** |  | [optional] 

## Example

```python
from magneto_api_client.models.scorecard_result_rule import ScorecardResultRule

# TODO update the JSON string below
json = "{}"
# create an instance of ScorecardResultRule from a JSON string
scorecard_result_rule_instance = ScorecardResultRule.from_json(json)
# print the JSON string representation of the object
print(ScorecardResultRule.to_json())

# convert the object into a dict
scorecard_result_rule_dict = scorecard_result_rule_instance.to_dict()
# create an instance of ScorecardResultRule from a dict
scorecard_result_rule_from_dict = ScorecardResultRule.from_dict(scorecard_result_rule_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


