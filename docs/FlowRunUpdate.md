# FlowRunUpdate

FlowRun Update Model.

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** |  | [optional] 
**flow_id** | **str** |  | 
**flow_type** | [**FlowType**](FlowType.md) |  | 
**args** | **object** |  | [optional] 
**phase** | [**StatusPhase**](StatusPhase.md) |  | [optional] 
**start_time** | **datetime** |  | [optional] 
**end_time** | **datetime** |  | [optional] 
**actions** | [**List[ActionDetails]**](ActionDetails.md) |  | [optional] 
**outputs** | [**Dict[str, RunOutput]**](RunOutput.md) |  | [optional] 
**logs** | [**List[LogEntry]**](LogEntry.md) |  | [optional] 
**tags** | [**List[Tag]**](Tag.md) |  | [optional] 

## Example

```python
from magneto_api_client.models.flow_run_update import FlowRunUpdate

# TODO update the JSON string below
json = "{}"
# create an instance of FlowRunUpdate from a JSON string
flow_run_update_instance = FlowRunUpdate.from_json(json)
# print the JSON string representation of the object
print(FlowRunUpdate.to_json())

# convert the object into a dict
flow_run_update_dict = flow_run_update_instance.to_dict()
# create an instance of FlowRunUpdate from a dict
flow_run_update_from_dict = FlowRunUpdate.from_dict(flow_run_update_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


