# CalculationPropertyOutputType

An enumeration.

## Enum

* `NUMBER` (value: `'number'`)

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


