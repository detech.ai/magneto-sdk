# PluginLegacyFilesToCheckUpdate


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**files_to_check** | [**List[PluginLegacyFileToCheck]**](PluginLegacyFileToCheck.md) |  | [optional] 

## Example

```python
from magneto_api_client.models.plugin_legacy_files_to_check_update import PluginLegacyFilesToCheckUpdate

# TODO update the JSON string below
json = "{}"
# create an instance of PluginLegacyFilesToCheckUpdate from a JSON string
plugin_legacy_files_to_check_update_instance = PluginLegacyFilesToCheckUpdate.from_json(json)
# print the JSON string representation of the object
print(PluginLegacyFilesToCheckUpdate.to_json())

# convert the object into a dict
plugin_legacy_files_to_check_update_dict = plugin_legacy_files_to_check_update_instance.to_dict()
# create an instance of PluginLegacyFilesToCheckUpdate from a dict
plugin_legacy_files_to_check_update_from_dict = PluginLegacyFilesToCheckUpdate.from_dict(plugin_legacy_files_to_check_update_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


