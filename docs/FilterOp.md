# FilterOp

An enumeration.

## Enum

* `EQ` (value: `'eq'`)

* `NEQ` (value: `'neq'`)

* `LT` (value: `'lt'`)

* `LTE` (value: `'lte'`)

* `GT` (value: `'gt'`)

* `GTE` (value: `'gte'`)

* `IN` (value: `'in'`)

* `NOTIN` (value: `'notIn'`)

* `LIKE` (value: `'like'`)

* `NOTLIKE` (value: `'notLike'`)

* `ILIKE` (value: `'ilike'`)

* `NOTILIKE` (value: `'notILike'`)

* `CONTAINS` (value: `'contains'`)

* `NOTCONTAINS` (value: `'notContains'`)

* `CONTAINSANY` (value: `'containsAny'`)

* `FUZZYSEARCH` (value: `'fuzzySearch'`)

* `ISNULL` (value: `'isnull'`)

* `ISNOTNULL` (value: `'isnotnull'`)

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


