# AggregationFunction

An enumeration.

## Enum

* `COUNT` (value: `'count'`)

* `SUM` (value: `'sum'`)

* `MEAN` (value: `'mean'`)

* `AVG` (value: `'avg'`)

* `MIN` (value: `'min'`)

* `MAX` (value: `'max'`)

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


