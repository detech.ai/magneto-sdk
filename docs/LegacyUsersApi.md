# magneto_api_client.LegacyUsersApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**create_and_invite_user_api_v1_legacy_users_invite_post**](LegacyUsersApi.md#create_and_invite_user_api_v1_legacy_users_invite_post) | **POST** /api/v1/legacy/users/invite | Create And Invite User
[**create_or_update_self_user_entity_api_v1_legacy_users_me_put**](LegacyUsersApi.md#create_or_update_self_user_entity_api_v1_legacy_users_me_put) | **PUT** /api/v1/legacy/users/me | Create Or Update Self User Entity
[**create_or_update_view_config_api_v1_legacy_users_me_view_configs_put**](LegacyUsersApi.md#create_or_update_view_config_api_v1_legacy_users_me_view_configs_put) | **PUT** /api/v1/legacy/users/me/view-configs | Create Or Update View Config
[**delete_user_api_v1_legacy_users_id_delete**](LegacyUsersApi.md#delete_user_api_v1_legacy_users_id_delete) | **DELETE** /api/v1/legacy/users/{id_} | Delete User
[**get_long_lived_token_api_v1_legacy_users_token_get**](LegacyUsersApi.md#get_long_lived_token_api_v1_legacy_users_token_get) | **GET** /api/v1/legacy/users/token | Get Long Lived Token
[**get_self_user_api_v1_legacy_users_me_get**](LegacyUsersApi.md#get_self_user_api_v1_legacy_users_me_get) | **GET** /api/v1/legacy/users/me | Get Self User
[**get_service_account_token_api_v1_legacy_users_service_accounts_type_get**](LegacyUsersApi.md#get_service_account_token_api_v1_legacy_users_service_accounts_type_get) | **GET** /api/v1/legacy/users/service-accounts/{type_} | Get Service Account Token
[**get_users_api_v1_legacy_users_get**](LegacyUsersApi.md#get_users_api_v1_legacy_users_get) | **GET** /api/v1/legacy/users | Get Users
[**get_view_config_api_v1_legacy_users_me_view_configs_get**](LegacyUsersApi.md#get_view_config_api_v1_legacy_users_me_view_configs_get) | **GET** /api/v1/legacy/users/me/view-configs | Get View Config
[**update_user_api_v1_legacy_users_id_put**](LegacyUsersApi.md#update_user_api_v1_legacy_users_id_put) | **PUT** /api/v1/legacy/users/{id_} | Update User


# **create_and_invite_user_api_v1_legacy_users_invite_post**
> UserLegacyRead create_and_invite_user_api_v1_legacy_users_invite_post(user_legacy_invite_create)

Create And Invite User

### Example

* Bearer Authentication (bearerAuth):

```python
import magneto_api_client
from magneto_api_client.models.user_legacy_invite_create import UserLegacyInviteCreate
from magneto_api_client.models.user_legacy_read import UserLegacyRead
from magneto_api_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = magneto_api_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: bearerAuth
configuration = magneto_api_client.Configuration(
    access_token = os.environ["BEARER_TOKEN"]
)

# Enter a context with an instance of the API client
async with magneto_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = magneto_api_client.LegacyUsersApi(api_client)
    user_legacy_invite_create = magneto_api_client.UserLegacyInviteCreate() # UserLegacyInviteCreate | 

    try:
        # Create And Invite User
        api_response = await api_instance.create_and_invite_user_api_v1_legacy_users_invite_post(user_legacy_invite_create)
        print("The response of LegacyUsersApi->create_and_invite_user_api_v1_legacy_users_invite_post:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling LegacyUsersApi->create_and_invite_user_api_v1_legacy_users_invite_post: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **user_legacy_invite_create** | [**UserLegacyInviteCreate**](UserLegacyInviteCreate.md)|  | 

### Return type

[**UserLegacyRead**](UserLegacyRead.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**201** | Successful Response |  -  |
**422** | Validation Error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **create_or_update_self_user_entity_api_v1_legacy_users_me_put**
> Entity create_or_update_self_user_entity_api_v1_legacy_users_me_put(user_entity_update)

Create Or Update Self User Entity

### Example

* Bearer Authentication (bearerAuth):

```python
import magneto_api_client
from magneto_api_client.models.entity import Entity
from magneto_api_client.models.user_entity_update import UserEntityUpdate
from magneto_api_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = magneto_api_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: bearerAuth
configuration = magneto_api_client.Configuration(
    access_token = os.environ["BEARER_TOKEN"]
)

# Enter a context with an instance of the API client
async with magneto_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = magneto_api_client.LegacyUsersApi(api_client)
    user_entity_update = magneto_api_client.UserEntityUpdate() # UserEntityUpdate | 

    try:
        # Create Or Update Self User Entity
        api_response = await api_instance.create_or_update_self_user_entity_api_v1_legacy_users_me_put(user_entity_update)
        print("The response of LegacyUsersApi->create_or_update_self_user_entity_api_v1_legacy_users_me_put:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling LegacyUsersApi->create_or_update_self_user_entity_api_v1_legacy_users_me_put: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **user_entity_update** | [**UserEntityUpdate**](UserEntityUpdate.md)|  | 

### Return type

[**Entity**](Entity.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**201** | Successful Response |  -  |
**422** | Validation Error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **create_or_update_view_config_api_v1_legacy_users_me_view_configs_put**
> UserLegacyViewConfigRead create_or_update_view_config_api_v1_legacy_users_me_view_configs_put(user_legacy_view_config_create)

Create Or Update View Config

### Example

* Bearer Authentication (bearerAuth):

```python
import magneto_api_client
from magneto_api_client.models.user_legacy_view_config_create import UserLegacyViewConfigCreate
from magneto_api_client.models.user_legacy_view_config_read import UserLegacyViewConfigRead
from magneto_api_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = magneto_api_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: bearerAuth
configuration = magneto_api_client.Configuration(
    access_token = os.environ["BEARER_TOKEN"]
)

# Enter a context with an instance of the API client
async with magneto_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = magneto_api_client.LegacyUsersApi(api_client)
    user_legacy_view_config_create = magneto_api_client.UserLegacyViewConfigCreate() # UserLegacyViewConfigCreate | 

    try:
        # Create Or Update View Config
        api_response = await api_instance.create_or_update_view_config_api_v1_legacy_users_me_view_configs_put(user_legacy_view_config_create)
        print("The response of LegacyUsersApi->create_or_update_view_config_api_v1_legacy_users_me_view_configs_put:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling LegacyUsersApi->create_or_update_view_config_api_v1_legacy_users_me_view_configs_put: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **user_legacy_view_config_create** | [**UserLegacyViewConfigCreate**](UserLegacyViewConfigCreate.md)|  | 

### Return type

[**UserLegacyViewConfigRead**](UserLegacyViewConfigRead.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**201** | Successful Response |  -  |
**422** | Validation Error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **delete_user_api_v1_legacy_users_id_delete**
> delete_user_api_v1_legacy_users_id_delete(id_)

Delete User

### Example

* Bearer Authentication (bearerAuth):

```python
import magneto_api_client
from magneto_api_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = magneto_api_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: bearerAuth
configuration = magneto_api_client.Configuration(
    access_token = os.environ["BEARER_TOKEN"]
)

# Enter a context with an instance of the API client
async with magneto_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = magneto_api_client.LegacyUsersApi(api_client)
    id_ = 56 # int | 

    try:
        # Delete User
        await api_instance.delete_user_api_v1_legacy_users_id_delete(id_)
    except Exception as e:
        print("Exception when calling LegacyUsersApi->delete_user_api_v1_legacy_users_id_delete: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id_** | **int**|  | 

### Return type

void (empty response body)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**204** | Successful Response |  -  |
**422** | Validation Error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_long_lived_token_api_v1_legacy_users_token_get**
> UserTokenCreate get_long_lived_token_api_v1_legacy_users_token_get()

Get Long Lived Token

### Example

* Bearer Authentication (bearerAuth):

```python
import magneto_api_client
from magneto_api_client.models.user_token_create import UserTokenCreate
from magneto_api_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = magneto_api_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: bearerAuth
configuration = magneto_api_client.Configuration(
    access_token = os.environ["BEARER_TOKEN"]
)

# Enter a context with an instance of the API client
async with magneto_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = magneto_api_client.LegacyUsersApi(api_client)

    try:
        # Get Long Lived Token
        api_response = await api_instance.get_long_lived_token_api_v1_legacy_users_token_get()
        print("The response of LegacyUsersApi->get_long_lived_token_api_v1_legacy_users_token_get:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling LegacyUsersApi->get_long_lived_token_api_v1_legacy_users_token_get: %s\n" % e)
```



### Parameters

This endpoint does not need any parameter.

### Return type

[**UserTokenCreate**](UserTokenCreate.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful Response |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_self_user_api_v1_legacy_users_me_get**
> UserLegacyRead get_self_user_api_v1_legacy_users_me_get()

Get Self User

### Example

* Bearer Authentication (bearerAuth):

```python
import magneto_api_client
from magneto_api_client.models.user_legacy_read import UserLegacyRead
from magneto_api_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = magneto_api_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: bearerAuth
configuration = magneto_api_client.Configuration(
    access_token = os.environ["BEARER_TOKEN"]
)

# Enter a context with an instance of the API client
async with magneto_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = magneto_api_client.LegacyUsersApi(api_client)

    try:
        # Get Self User
        api_response = await api_instance.get_self_user_api_v1_legacy_users_me_get()
        print("The response of LegacyUsersApi->get_self_user_api_v1_legacy_users_me_get:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling LegacyUsersApi->get_self_user_api_v1_legacy_users_me_get: %s\n" % e)
```



### Parameters

This endpoint does not need any parameter.

### Return type

[**UserLegacyRead**](UserLegacyRead.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful Response |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_service_account_token_api_v1_legacy_users_service_accounts_type_get**
> UserTokenCreate get_service_account_token_api_v1_legacy_users_service_accounts_type_get(type_)

Get Service Account Token

### Example

* Bearer Authentication (bearerAuth):

```python
import magneto_api_client
from magneto_api_client.models.service_account_type import ServiceAccountType
from magneto_api_client.models.user_token_create import UserTokenCreate
from magneto_api_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = magneto_api_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: bearerAuth
configuration = magneto_api_client.Configuration(
    access_token = os.environ["BEARER_TOKEN"]
)

# Enter a context with an instance of the API client
async with magneto_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = magneto_api_client.LegacyUsersApi(api_client)
    type_ = magneto_api_client.ServiceAccountType() # ServiceAccountType | 

    try:
        # Get Service Account Token
        api_response = await api_instance.get_service_account_token_api_v1_legacy_users_service_accounts_type_get(type_)
        print("The response of LegacyUsersApi->get_service_account_token_api_v1_legacy_users_service_accounts_type_get:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling LegacyUsersApi->get_service_account_token_api_v1_legacy_users_service_accounts_type_get: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **type_** | [**ServiceAccountType**](.md)|  | 

### Return type

[**UserTokenCreate**](UserTokenCreate.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful Response |  -  |
**422** | Validation Error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_users_api_v1_legacy_users_get**
> List[UserLegacyRead] get_users_api_v1_legacy_users_get()

Get Users

### Example

* Bearer Authentication (bearerAuth):

```python
import magneto_api_client
from magneto_api_client.models.user_legacy_read import UserLegacyRead
from magneto_api_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = magneto_api_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: bearerAuth
configuration = magneto_api_client.Configuration(
    access_token = os.environ["BEARER_TOKEN"]
)

# Enter a context with an instance of the API client
async with magneto_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = magneto_api_client.LegacyUsersApi(api_client)

    try:
        # Get Users
        api_response = await api_instance.get_users_api_v1_legacy_users_get()
        print("The response of LegacyUsersApi->get_users_api_v1_legacy_users_get:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling LegacyUsersApi->get_users_api_v1_legacy_users_get: %s\n" % e)
```



### Parameters

This endpoint does not need any parameter.

### Return type

[**List[UserLegacyRead]**](UserLegacyRead.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful Response |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_view_config_api_v1_legacy_users_me_view_configs_get**
> UserLegacyViewConfigRead get_view_config_api_v1_legacy_users_me_view_configs_get(path)

Get View Config

### Example

* Bearer Authentication (bearerAuth):

```python
import magneto_api_client
from magneto_api_client.models.user_legacy_view_config_read import UserLegacyViewConfigRead
from magneto_api_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = magneto_api_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: bearerAuth
configuration = magneto_api_client.Configuration(
    access_token = os.environ["BEARER_TOKEN"]
)

# Enter a context with an instance of the API client
async with magneto_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = magneto_api_client.LegacyUsersApi(api_client)
    path = 'path_example' # str | 

    try:
        # Get View Config
        api_response = await api_instance.get_view_config_api_v1_legacy_users_me_view_configs_get(path)
        print("The response of LegacyUsersApi->get_view_config_api_v1_legacy_users_me_view_configs_get:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling LegacyUsersApi->get_view_config_api_v1_legacy_users_me_view_configs_get: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **path** | **str**|  | 

### Return type

[**UserLegacyViewConfigRead**](UserLegacyViewConfigRead.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful Response |  -  |
**422** | Validation Error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **update_user_api_v1_legacy_users_id_put**
> UserLegacyRead update_user_api_v1_legacy_users_id_put(id_, user_legacy_update)

Update User

### Example

* Bearer Authentication (bearerAuth):

```python
import magneto_api_client
from magneto_api_client.models.user_legacy_read import UserLegacyRead
from magneto_api_client.models.user_legacy_update import UserLegacyUpdate
from magneto_api_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = magneto_api_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: bearerAuth
configuration = magneto_api_client.Configuration(
    access_token = os.environ["BEARER_TOKEN"]
)

# Enter a context with an instance of the API client
async with magneto_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = magneto_api_client.LegacyUsersApi(api_client)
    id_ = 56 # int | 
    user_legacy_update = magneto_api_client.UserLegacyUpdate() # UserLegacyUpdate | 

    try:
        # Update User
        api_response = await api_instance.update_user_api_v1_legacy_users_id_put(id_, user_legacy_update)
        print("The response of LegacyUsersApi->update_user_api_v1_legacy_users_id_put:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling LegacyUsersApi->update_user_api_v1_legacy_users_id_put: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id_** | **int**|  | 
 **user_legacy_update** | [**UserLegacyUpdate**](UserLegacyUpdate.md)|  | 

### Return type

[**UserLegacyRead**](UserLegacyRead.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful Response |  -  |
**422** | Validation Error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

