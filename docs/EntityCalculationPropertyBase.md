# EntityCalculationPropertyBase


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**timeseries** | **List[List[TimeseriesInnerInner]]** |  | [optional] 

## Example

```python
from magneto_api_client.models.entity_calculation_property_base import EntityCalculationPropertyBase

# TODO update the JSON string below
json = "{}"
# create an instance of EntityCalculationPropertyBase from a JSON string
entity_calculation_property_base_instance = EntityCalculationPropertyBase.from_json(json)
# print the JSON string representation of the object
print(EntityCalculationPropertyBase.to_json())

# convert the object into a dict
entity_calculation_property_base_dict = entity_calculation_property_base_instance.to_dict()
# create an instance of EntityCalculationPropertyBase from a dict
entity_calculation_property_base_from_dict = EntityCalculationPropertyBase.from_dict(entity_calculation_property_base_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


