# TimeseriesInnerInner


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------

## Example

```python
from magneto_api_client.models.timeseries_inner_inner import TimeseriesInnerInner

# TODO update the JSON string below
json = "{}"
# create an instance of TimeseriesInnerInner from a JSON string
timeseries_inner_inner_instance = TimeseriesInnerInner.from_json(json)
# print the JSON string representation of the object
print(TimeseriesInnerInner.to_json())

# convert the object into a dict
timeseries_inner_inner_dict = timeseries_inner_inner_instance.to_dict()
# create an instance of TimeseriesInnerInner from a dict
timeseries_inner_inner_from_dict = TimeseriesInnerInner.from_dict(timeseries_inner_inner_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


