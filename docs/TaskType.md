# TaskType

An enumeration.

## Enum

* `ENTITIES_BULK_UPSERT` (value: `'ENTITIES_BULK_UPSERT'`)

* `ENTITIES_CALCULATION_PROPERTIES` (value: `'ENTITIES_CALCULATION_PROPERTIES'`)

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


