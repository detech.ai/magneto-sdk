# FlowAction

Base class for models that exclude unset fields.  **Note**: This overrides any `exclude_unset` kwarg passed for the model and its nested models.

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**type** | [**FlowActionType**](FlowActionType.md) |  | 
**args** | **object** |  | [optional] 
**id** | **str** |  | [optional] 
**title** | **str** |  | [optional] 
**description** | **str** |  | [optional] 
**conditions** | [**List[Condition]**](Condition.md) |  | [optional] 

## Example

```python
from magneto_api_client.models.flow_action import FlowAction

# TODO update the JSON string below
json = "{}"
# create an instance of FlowAction from a JSON string
flow_action_instance = FlowAction.from_json(json)
# print the JSON string representation of the object
print(FlowAction.to_json())

# convert the object into a dict
flow_action_dict = flow_action_instance.to_dict()
# create an instance of FlowAction from a dict
flow_action_from_dict = FlowAction.from_dict(flow_action_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


