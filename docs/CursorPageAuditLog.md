# CursorPageAuditLog


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**items** | [**List[AuditLog]**](AuditLog.md) |  | 
**total** | **int** | Total items | [optional] 
**current_page** | **str** | Cursor to refetch the current page | [optional] 
**current_page_backwards** | **str** | Cursor to refetch the current page starting from the last item | [optional] 
**previous_page** | **str** | Cursor for the previous page | [optional] 
**next_page** | **str** | Cursor for the next page | [optional] 

## Example

```python
from magneto_api_client.models.cursor_page_audit_log import CursorPageAuditLog

# TODO update the JSON string below
json = "{}"
# create an instance of CursorPageAuditLog from a JSON string
cursor_page_audit_log_instance = CursorPageAuditLog.from_json(json)
# print the JSON string representation of the object
print(CursorPageAuditLog.to_json())

# convert the object into a dict
cursor_page_audit_log_dict = cursor_page_audit_log_instance.to_dict()
# create an instance of CursorPageAuditLog from a dict
cursor_page_audit_log_from_dict = CursorPageAuditLog.from_dict(cursor_page_audit_log_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


