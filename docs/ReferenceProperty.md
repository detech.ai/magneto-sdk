# ReferenceProperty

Base class for models that exclude unset fields.  **Note**: This overrides any `exclude_unset` kwarg passed for the model and its nested models.

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**title** | **str** |  | 
**description** | **str** |  | [optional] 
**relation** | **str** |  | 
**var_property** | **str** |  | 
**var_schema** | [**JsonSchema**](JsonSchema.md) | JSON schema for the reference property. This is not stored in the database, but is generated from the relation and property fields. | [optional] 

## Example

```python
from magneto_api_client.models.reference_property import ReferenceProperty

# TODO update the JSON string below
json = "{}"
# create an instance of ReferenceProperty from a JSON string
reference_property_instance = ReferenceProperty.from_json(json)
# print the JSON string representation of the object
print(ReferenceProperty.to_json())

# convert the object into a dict
reference_property_dict = reference_property_instance.to_dict()
# create an instance of ReferenceProperty from a dict
reference_property_from_dict = ReferenceProperty.from_dict(reference_property_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


