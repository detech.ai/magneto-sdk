# TaskStatus

An enumeration.

## Enum

* `CREATED` (value: `'CREATED'`)

* `RUNNING` (value: `'RUNNING'`)

* `SUCCESS` (value: `'SUCCESS'`)

* `FAILED` (value: `'FAILED'`)

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


