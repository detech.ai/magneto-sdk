# PluginLegacyCreate


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **str** |  | 
**source_key** | **str** |  | [optional] 
**status** | **int** |  | [optional] [default to 1]
**customer_managed** | **bool** |  | [optional] [default to False]
**assets_for_discovery** | **List[str]** |  | [optional] 
**data_source** | **int** |  | 
**credentials** | **object** |  | [optional] 
**properties** | **object** |  | [optional] 

## Example

```python
from magneto_api_client.models.plugin_legacy_create import PluginLegacyCreate

# TODO update the JSON string below
json = "{}"
# create an instance of PluginLegacyCreate from a JSON string
plugin_legacy_create_instance = PluginLegacyCreate.from_json(json)
# print the JSON string representation of the object
print(PluginLegacyCreate.to_json())

# convert the object into a dict
plugin_legacy_create_dict = plugin_legacy_create_instance.to_dict()
# create an instance of PluginLegacyCreate from a dict
plugin_legacy_create_from_dict = PluginLegacyCreate.from_dict(plugin_legacy_create_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


