# magneto_api_client.TimeSeriesApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**timeseries_query_api_v1_timeseries_query_post**](TimeSeriesApi.md#timeseries_query_api_v1_timeseries_query_post) | **POST** /api/v1/timeseries/query | Timeseries Query
[**timeseries_write_api_v1_timeseries_write_post**](TimeSeriesApi.md#timeseries_write_api_v1_timeseries_write_post) | **POST** /api/v1/timeseries/write | Timeseries Write


# **timeseries_query_api_v1_timeseries_query_post**
> List[object] timeseries_query_api_v1_timeseries_query_post(timeseries_query)

Timeseries Query

### Example

* Bearer Authentication (bearerAuth):

```python
import magneto_api_client
from magneto_api_client.models.timeseries_query import TimeseriesQuery
from magneto_api_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = magneto_api_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: bearerAuth
configuration = magneto_api_client.Configuration(
    access_token = os.environ["BEARER_TOKEN"]
)

# Enter a context with an instance of the API client
async with magneto_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = magneto_api_client.TimeSeriesApi(api_client)
    timeseries_query = magneto_api_client.TimeseriesQuery() # TimeseriesQuery | 

    try:
        # Timeseries Query
        api_response = await api_instance.timeseries_query_api_v1_timeseries_query_post(timeseries_query)
        print("The response of TimeSeriesApi->timeseries_query_api_v1_timeseries_query_post:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling TimeSeriesApi->timeseries_query_api_v1_timeseries_query_post: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **timeseries_query** | [**TimeseriesQuery**](TimeseriesQuery.md)|  | 

### Return type

**List[object]**

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful Response |  -  |
**422** | Validation Error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **timeseries_write_api_v1_timeseries_write_post**
> object timeseries_write_api_v1_timeseries_write_post(timeseries_write)

Timeseries Write

### Example

* Bearer Authentication (bearerAuth):

```python
import magneto_api_client
from magneto_api_client.models.timeseries_write import TimeseriesWrite
from magneto_api_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = magneto_api_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: bearerAuth
configuration = magneto_api_client.Configuration(
    access_token = os.environ["BEARER_TOKEN"]
)

# Enter a context with an instance of the API client
async with magneto_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = magneto_api_client.TimeSeriesApi(api_client)
    timeseries_write = magneto_api_client.TimeseriesWrite() # TimeseriesWrite | 

    try:
        # Timeseries Write
        api_response = await api_instance.timeseries_write_api_v1_timeseries_write_post(timeseries_write)
        print("The response of TimeSeriesApi->timeseries_write_api_v1_timeseries_write_post:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling TimeSeriesApi->timeseries_write_api_v1_timeseries_write_post: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **timeseries_write** | [**TimeseriesWrite**](TimeseriesWrite.md)|  | 

### Return type

**object**

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful Response |  -  |
**422** | Validation Error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

