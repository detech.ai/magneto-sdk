# PluginTokenRead


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**token** | **str** |  | 

## Example

```python
from magneto_api_client.models.plugin_token_read import PluginTokenRead

# TODO update the JSON string below
json = "{}"
# create an instance of PluginTokenRead from a JSON string
plugin_token_read_instance = PluginTokenRead.from_json(json)
# print the JSON string representation of the object
print(PluginTokenRead.to_json())

# convert the object into a dict
plugin_token_read_dict = plugin_token_read_instance.to_dict()
# create an instance of PluginTokenRead from a dict
plugin_token_read_from_dict = PluginTokenRead.from_dict(plugin_token_read_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


