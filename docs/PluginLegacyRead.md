# PluginLegacyRead


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **str** |  | 
**source_key** | **str** |  | [optional] 
**status** | **int** |  | [optional] [default to 1]
**customer_managed** | **bool** |  | [optional] [default to False]
**assets_for_discovery** | **List[str]** |  | [optional] 
**id** | **str** |  | 
**data_source** | **str** |  | 
**fetch_status** | [**FetchStatus**](FetchStatus.md) |  | 
**capabilities** | [**List[PluginCapability]**](PluginCapability.md) |  | [optional] 
**properties** | **object** |  | [optional] 
**files_to_check** | [**List[PluginLegacyFileToCheck]**](PluginLegacyFileToCheck.md) |  | [optional] 

## Example

```python
from magneto_api_client.models.plugin_legacy_read import PluginLegacyRead

# TODO update the JSON string below
json = "{}"
# create an instance of PluginLegacyRead from a JSON string
plugin_legacy_read_instance = PluginLegacyRead.from_json(json)
# print the JSON string representation of the object
print(PluginLegacyRead.to_json())

# convert the object into a dict
plugin_legacy_read_dict = plugin_legacy_read_instance.to_dict()
# create an instance of PluginLegacyRead from a dict
plugin_legacy_read_from_dict = PluginLegacyRead.from_dict(plugin_legacy_read_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


