# Entity

Entity Model.

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**sid** | **str** |  | [optional] 
**created_at** | **datetime** |  | 
**created_by** | **int** |  | [optional] 
**updated_at** | **datetime** |  | 
**updated_by** | **int** |  | [optional] 
**id** | **str** |  | 
**blueprint_id** | **str** |  | 
**title** | **str** |  | 
**description** | **str** |  | [optional] 
**is_active** | **bool** |  | [optional] [default to True]
**is_hidden** | **bool** |  | [optional] [default to False]
**properties** | **object** |  | [optional] 
**relations** | [**Dict[str, EntityRelation]**](EntityRelation.md) |  | [optional] 
**sources** | [**Dict[str, EntitySource]**](EntitySource.md) |  | [optional] 
**organization_id** | **int** |  | [optional] 
**checksum** | **str** |  | [optional] 
**search_vector** | **object** |  | [optional] 
**calculation_properties** | [**Dict[str, EntityCalculationPropertyAllIntervals]**](EntityCalculationPropertyAllIntervals.md) |  | [optional] 

## Example

```python
from magneto_api_client.models.entity import Entity

# TODO update the JSON string below
json = "{}"
# create an instance of Entity from a JSON string
entity_instance = Entity.from_json(json)
# print the JSON string representation of the object
print(Entity.to_json())

# convert the object into a dict
entity_dict = entity_instance.to_dict()
# create an instance of Entity from a dict
entity_from_dict = Entity.from_dict(entity_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


