# UserTokenCreate


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**token** | **str** |  | 

## Example

```python
from magneto_api_client.models.user_token_create import UserTokenCreate

# TODO update the JSON string below
json = "{}"
# create an instance of UserTokenCreate from a JSON string
user_token_create_instance = UserTokenCreate.from_json(json)
# print the JSON string representation of the object
print(UserTokenCreate.to_json())

# convert the object into a dict
user_token_create_dict = user_token_create_instance.to_dict()
# create an instance of UserTokenCreate from a dict
user_token_create_from_dict = UserTokenCreate.from_dict(user_token_create_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


