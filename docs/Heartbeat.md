# Heartbeat


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**timestamp** | **datetime** |  | 

## Example

```python
from magneto_api_client.models.heartbeat import Heartbeat

# TODO update the JSON string below
json = "{}"
# create an instance of Heartbeat from a JSON string
heartbeat_instance = Heartbeat.from_json(json)
# print the JSON string representation of the object
print(Heartbeat.to_json())

# convert the object into a dict
heartbeat_dict = heartbeat_instance.to_dict()
# create an instance of Heartbeat from a dict
heartbeat_from_dict = Heartbeat.from_dict(heartbeat_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


