# FlowType

An enumeration.

## Enum

* `AUTOMATION` (value: `'automation'`)

* `SELFSERVICEACTION` (value: `'selfServiceAction'`)

* `SCORECARD` (value: `'scorecard'`)

* `REFERENCE` (value: `'reference'`)

* `WORKFLOW` (value: `'workflow'`)

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


