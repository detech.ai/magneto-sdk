# ReferencepropertiesValue


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**entity_id** | **str** |  | [optional] 
**value** | **object** |  | [optional] 

## Example

```python
from magneto_api_client.models.referenceproperties_value import ReferencepropertiesValue

# TODO update the JSON string below
json = "{}"
# create an instance of ReferencepropertiesValue from a JSON string
referenceproperties_value_instance = ReferencepropertiesValue.from_json(json)
# print the JSON string representation of the object
print(ReferencepropertiesValue.to_json())

# convert the object into a dict
referenceproperties_value_dict = referenceproperties_value_instance.to_dict()
# create an instance of ReferencepropertiesValue from a dict
referenceproperties_value_from_dict = ReferencepropertiesValue.from_dict(referenceproperties_value_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


