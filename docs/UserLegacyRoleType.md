# UserLegacyRoleType

An enumeration.

## Enum

* `ADMIN` (value: `'ADMIN'`)

* `READ_ONLY` (value: `'READ_ONLY'`)

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


