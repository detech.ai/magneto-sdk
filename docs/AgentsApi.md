# magneto_api_client.AgentsApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**get_agents_api_v1_agents_get**](AgentsApi.md#get_agents_api_v1_agents_get) | **GET** /api/v1/agents | Get Agents
[**register_agent_api_v1_agents_post**](AgentsApi.md#register_agent_api_v1_agents_post) | **POST** /api/v1/agents | Register Agent
[**update_agent_heartbeat_api_v1_agents_id_heartbeat_post**](AgentsApi.md#update_agent_heartbeat_api_v1_agents_id_heartbeat_post) | **POST** /api/v1/agents/{id_}/heartbeat | Update Agent Heartbeat


# **get_agents_api_v1_agents_get**
> CursorPageAgentRead get_agents_api_v1_agents_get(type_=type_, status=status, cursor=cursor, size=size, include_total=include_total)

Get Agents

### Example

* Bearer Authentication (bearerAuth):

```python
import magneto_api_client
from magneto_api_client.models.agent_status import AgentStatus
from magneto_api_client.models.agent_type import AgentType
from magneto_api_client.models.cursor_page_agent_read import CursorPageAgentRead
from magneto_api_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = magneto_api_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: bearerAuth
configuration = magneto_api_client.Configuration(
    access_token = os.environ["BEARER_TOKEN"]
)

# Enter a context with an instance of the API client
async with magneto_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = magneto_api_client.AgentsApi(api_client)
    type_ = magneto_api_client.AgentType() # AgentType |  (optional)
    status = magneto_api_client.AgentStatus() # AgentStatus |  (optional)
    cursor = 'cursor_example' # str |  (optional)
    size = 50 # int |  (optional) (default to 50)
    include_total = False # bool |  (optional) (default to False)

    try:
        # Get Agents
        api_response = await api_instance.get_agents_api_v1_agents_get(type_=type_, status=status, cursor=cursor, size=size, include_total=include_total)
        print("The response of AgentsApi->get_agents_api_v1_agents_get:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling AgentsApi->get_agents_api_v1_agents_get: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **type_** | [**AgentType**](.md)|  | [optional] 
 **status** | [**AgentStatus**](.md)|  | [optional] 
 **cursor** | **str**|  | [optional] 
 **size** | **int**|  | [optional] [default to 50]
 **include_total** | **bool**|  | [optional] [default to False]

### Return type

[**CursorPageAgentRead**](CursorPageAgentRead.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful Response |  -  |
**422** | Validation Error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **register_agent_api_v1_agents_post**
> AgentRead register_agent_api_v1_agents_post(agent_create)

Register Agent

### Example

* Bearer Authentication (bearerAuth):

```python
import magneto_api_client
from magneto_api_client.models.agent_create import AgentCreate
from magneto_api_client.models.agent_read import AgentRead
from magneto_api_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = magneto_api_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: bearerAuth
configuration = magneto_api_client.Configuration(
    access_token = os.environ["BEARER_TOKEN"]
)

# Enter a context with an instance of the API client
async with magneto_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = magneto_api_client.AgentsApi(api_client)
    agent_create = magneto_api_client.AgentCreate() # AgentCreate | 

    try:
        # Register Agent
        api_response = await api_instance.register_agent_api_v1_agents_post(agent_create)
        print("The response of AgentsApi->register_agent_api_v1_agents_post:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling AgentsApi->register_agent_api_v1_agents_post: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **agent_create** | [**AgentCreate**](AgentCreate.md)|  | 

### Return type

[**AgentRead**](AgentRead.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**201** | Successful Response |  -  |
**422** | Validation Error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **update_agent_heartbeat_api_v1_agents_id_heartbeat_post**
> AgentRead update_agent_heartbeat_api_v1_agents_id_heartbeat_post(id_, heartbeat)

Update Agent Heartbeat

### Example

* Bearer Authentication (bearerAuth):

```python
import magneto_api_client
from magneto_api_client.models.agent_read import AgentRead
from magneto_api_client.models.heartbeat import Heartbeat
from magneto_api_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = magneto_api_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: bearerAuth
configuration = magneto_api_client.Configuration(
    access_token = os.environ["BEARER_TOKEN"]
)

# Enter a context with an instance of the API client
async with magneto_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = magneto_api_client.AgentsApi(api_client)
    id_ = 'id__example' # str | 
    heartbeat = magneto_api_client.Heartbeat() # Heartbeat | 

    try:
        # Update Agent Heartbeat
        api_response = await api_instance.update_agent_heartbeat_api_v1_agents_id_heartbeat_post(id_, heartbeat)
        print("The response of AgentsApi->update_agent_heartbeat_api_v1_agents_id_heartbeat_post:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling AgentsApi->update_agent_heartbeat_api_v1_agents_id_heartbeat_post: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id_** | **str**|  | 
 **heartbeat** | [**Heartbeat**](Heartbeat.md)|  | 

### Return type

[**AgentRead**](AgentRead.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful Response |  -  |
**422** | Validation Error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

