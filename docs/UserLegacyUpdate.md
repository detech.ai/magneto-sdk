# UserLegacyUpdate


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **str** |  | [optional] 
**email** | **str** |  | [optional] 
**role** | [**UserLegacyRoleType**](UserLegacyRoleType.md) |  | [optional] 

## Example

```python
from magneto_api_client.models.user_legacy_update import UserLegacyUpdate

# TODO update the JSON string below
json = "{}"
# create an instance of UserLegacyUpdate from a JSON string
user_legacy_update_instance = UserLegacyUpdate.from_json(json)
# print the JSON string representation of the object
print(UserLegacyUpdate.to_json())

# convert the object into a dict
user_legacy_update_dict = user_legacy_update_instance.to_dict()
# create an instance of UserLegacyUpdate from a dict
user_legacy_update_from_dict = UserLegacyUpdate.from_dict(user_legacy_update_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


