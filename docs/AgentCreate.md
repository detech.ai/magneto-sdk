# AgentCreate


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** |  | 
**type** | [**AgentType**](AgentType.md) |  | 
**version** | **str** |  | 
**inactive_threshold** | **int** |  | [optional] [default to 300]

## Example

```python
from magneto_api_client.models.agent_create import AgentCreate

# TODO update the JSON string below
json = "{}"
# create an instance of AgentCreate from a JSON string
agent_create_instance = AgentCreate.from_json(json)
# print the JSON string representation of the object
print(AgentCreate.to_json())

# convert the object into a dict
agent_create_dict = agent_create_instance.to_dict()
# create an instance of AgentCreate from a dict
agent_create_from_dict = AgentCreate.from_dict(agent_create_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


