# ScorecardInsights

Complete scorecard insights model providing detailed analytics about ranks and rules.

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**metadata** | [**InsightsMetadata**](InsightsMetadata.md) |  | [optional] 
**insights** | [**ScorecardInsightsData**](ScorecardInsightsData.md) |  | [optional] 

## Example

```python
from magneto_api_client.models.scorecard_insights import ScorecardInsights

# TODO update the JSON string below
json = "{}"
# create an instance of ScorecardInsights from a JSON string
scorecard_insights_instance = ScorecardInsights.from_json(json)
# print the JSON string representation of the object
print(ScorecardInsights.to_json())

# convert the object into a dict
scorecard_insights_dict = scorecard_insights_instance.to_dict()
# create an instance of ScorecardInsights from a dict
scorecard_insights_from_dict = ScorecardInsights.from_dict(scorecard_insights_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


