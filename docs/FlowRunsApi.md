# magneto_api_client.FlowRunsApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**approve_flow_run_api_v1_flow_runs_id_approve_post**](FlowRunsApi.md#approve_flow_run_api_v1_flow_runs_id_approve_post) | **POST** /api/v1/flow-runs/{id_}/approve | Approve Flow Run
[**create_flow_run_api_v1_flow_runs_post**](FlowRunsApi.md#create_flow_run_api_v1_flow_runs_post) | **POST** /api/v1/flow-runs | Create Flow Run
[**delete_flow_run_api_v1_flow_runs_id_delete**](FlowRunsApi.md#delete_flow_run_api_v1_flow_runs_id_delete) | **DELETE** /api/v1/flow-runs/{id_} | Delete Flow Run
[**deny_flow_run_api_v1_flow_runs_id_deny_post**](FlowRunsApi.md#deny_flow_run_api_v1_flow_runs_id_deny_post) | **POST** /api/v1/flow-runs/{id_}/deny | Deny Flow Run
[**get_flow_run_api_v1_flow_runs_id_get**](FlowRunsApi.md#get_flow_run_api_v1_flow_runs_id_get) | **GET** /api/v1/flow-runs/{id_} | Get Flow Run
[**get_flow_runs_api_v1_flow_runs_get**](FlowRunsApi.md#get_flow_runs_api_v1_flow_runs_get) | **GET** /api/v1/flow-runs | Get Flow Runs
[**upsert_flow_run_api_v1_flow_runs_id_put**](FlowRunsApi.md#upsert_flow_run_api_v1_flow_runs_id_put) | **PUT** /api/v1/flow-runs/{id_} | Upsert Flow Run


# **approve_flow_run_api_v1_flow_runs_id_approve_post**
> FlowRunRead approve_flow_run_api_v1_flow_runs_id_approve_post(id_)

Approve Flow Run

### Example

* Bearer Authentication (bearerAuth):

```python
import magneto_api_client
from magneto_api_client.models.flow_run_read import FlowRunRead
from magneto_api_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = magneto_api_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: bearerAuth
configuration = magneto_api_client.Configuration(
    access_token = os.environ["BEARER_TOKEN"]
)

# Enter a context with an instance of the API client
async with magneto_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = magneto_api_client.FlowRunsApi(api_client)
    id_ = 'id__example' # str | 

    try:
        # Approve Flow Run
        api_response = await api_instance.approve_flow_run_api_v1_flow_runs_id_approve_post(id_)
        print("The response of FlowRunsApi->approve_flow_run_api_v1_flow_runs_id_approve_post:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling FlowRunsApi->approve_flow_run_api_v1_flow_runs_id_approve_post: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id_** | **str**|  | 

### Return type

[**FlowRunRead**](FlowRunRead.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful Response |  -  |
**422** | Validation Error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **create_flow_run_api_v1_flow_runs_post**
> FlowRunRead create_flow_run_api_v1_flow_runs_post(flow_run_create)

Create Flow Run

### Example

* Bearer Authentication (bearerAuth):

```python
import magneto_api_client
from magneto_api_client.models.flow_run_create import FlowRunCreate
from magneto_api_client.models.flow_run_read import FlowRunRead
from magneto_api_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = magneto_api_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: bearerAuth
configuration = magneto_api_client.Configuration(
    access_token = os.environ["BEARER_TOKEN"]
)

# Enter a context with an instance of the API client
async with magneto_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = magneto_api_client.FlowRunsApi(api_client)
    flow_run_create = magneto_api_client.FlowRunCreate() # FlowRunCreate | 

    try:
        # Create Flow Run
        api_response = await api_instance.create_flow_run_api_v1_flow_runs_post(flow_run_create)
        print("The response of FlowRunsApi->create_flow_run_api_v1_flow_runs_post:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling FlowRunsApi->create_flow_run_api_v1_flow_runs_post: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **flow_run_create** | [**FlowRunCreate**](FlowRunCreate.md)|  | 

### Return type

[**FlowRunRead**](FlowRunRead.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**201** | Successful Response |  -  |
**422** | Validation Error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **delete_flow_run_api_v1_flow_runs_id_delete**
> delete_flow_run_api_v1_flow_runs_id_delete(id_)

Delete Flow Run

### Example

* Bearer Authentication (bearerAuth):

```python
import magneto_api_client
from magneto_api_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = magneto_api_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: bearerAuth
configuration = magneto_api_client.Configuration(
    access_token = os.environ["BEARER_TOKEN"]
)

# Enter a context with an instance of the API client
async with magneto_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = magneto_api_client.FlowRunsApi(api_client)
    id_ = 'id__example' # str | 

    try:
        # Delete Flow Run
        await api_instance.delete_flow_run_api_v1_flow_runs_id_delete(id_)
    except Exception as e:
        print("Exception when calling FlowRunsApi->delete_flow_run_api_v1_flow_runs_id_delete: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id_** | **str**|  | 

### Return type

void (empty response body)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**204** | Successful Response |  -  |
**422** | Validation Error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **deny_flow_run_api_v1_flow_runs_id_deny_post**
> FlowRunRead deny_flow_run_api_v1_flow_runs_id_deny_post(id_)

Deny Flow Run

### Example

* Bearer Authentication (bearerAuth):

```python
import magneto_api_client
from magneto_api_client.models.flow_run_read import FlowRunRead
from magneto_api_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = magneto_api_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: bearerAuth
configuration = magneto_api_client.Configuration(
    access_token = os.environ["BEARER_TOKEN"]
)

# Enter a context with an instance of the API client
async with magneto_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = magneto_api_client.FlowRunsApi(api_client)
    id_ = 'id__example' # str | 

    try:
        # Deny Flow Run
        api_response = await api_instance.deny_flow_run_api_v1_flow_runs_id_deny_post(id_)
        print("The response of FlowRunsApi->deny_flow_run_api_v1_flow_runs_id_deny_post:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling FlowRunsApi->deny_flow_run_api_v1_flow_runs_id_deny_post: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id_** | **str**|  | 

### Return type

[**FlowRunRead**](FlowRunRead.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful Response |  -  |
**422** | Validation Error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_flow_run_api_v1_flow_runs_id_get**
> FlowRunRead get_flow_run_api_v1_flow_runs_id_get(id_)

Get Flow Run

### Example

* Bearer Authentication (bearerAuth):

```python
import magneto_api_client
from magneto_api_client.models.flow_run_read import FlowRunRead
from magneto_api_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = magneto_api_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: bearerAuth
configuration = magneto_api_client.Configuration(
    access_token = os.environ["BEARER_TOKEN"]
)

# Enter a context with an instance of the API client
async with magneto_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = magneto_api_client.FlowRunsApi(api_client)
    id_ = 'id__example' # str | 

    try:
        # Get Flow Run
        api_response = await api_instance.get_flow_run_api_v1_flow_runs_id_get(id_)
        print("The response of FlowRunsApi->get_flow_run_api_v1_flow_runs_id_get:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling FlowRunsApi->get_flow_run_api_v1_flow_runs_id_get: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id_** | **str**|  | 

### Return type

[**FlowRunRead**](FlowRunRead.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful Response |  -  |
**422** | Validation Error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_flow_runs_api_v1_flow_runs_get**
> CursorPageFlowRunRead get_flow_runs_api_v1_flow_runs_get(filters=filters, sorts=sorts, cursor=cursor, size=size, include_total=include_total)

Get Flow Runs

### Example

* Bearer Authentication (bearerAuth):

```python
import magneto_api_client
from magneto_api_client.models.cursor_page_flow_run_read import CursorPageFlowRunRead
from magneto_api_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = magneto_api_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: bearerAuth
configuration = magneto_api_client.Configuration(
    access_token = os.environ["BEARER_TOKEN"]
)

# Enter a context with an instance of the API client
async with magneto_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = magneto_api_client.FlowRunsApi(api_client)
    filters = ['filters_example'] # List[str] |  (optional)
    sorts = ['sorts_example'] # List[str] |  (optional)
    cursor = 'cursor_example' # str |  (optional)
    size = 50 # int |  (optional) (default to 50)
    include_total = False # bool |  (optional) (default to False)

    try:
        # Get Flow Runs
        api_response = await api_instance.get_flow_runs_api_v1_flow_runs_get(filters=filters, sorts=sorts, cursor=cursor, size=size, include_total=include_total)
        print("The response of FlowRunsApi->get_flow_runs_api_v1_flow_runs_get:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling FlowRunsApi->get_flow_runs_api_v1_flow_runs_get: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filters** | [**List[str]**](str.md)|  | [optional] 
 **sorts** | [**List[str]**](str.md)|  | [optional] 
 **cursor** | **str**|  | [optional] 
 **size** | **int**|  | [optional] [default to 50]
 **include_total** | **bool**|  | [optional] [default to False]

### Return type

[**CursorPageFlowRunRead**](CursorPageFlowRunRead.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful Response |  -  |
**422** | Validation Error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **upsert_flow_run_api_v1_flow_runs_id_put**
> FlowRunRead upsert_flow_run_api_v1_flow_runs_id_put(id_, flow_run_update)

Upsert Flow Run

### Example

* Bearer Authentication (bearerAuth):

```python
import magneto_api_client
from magneto_api_client.models.flow_run_read import FlowRunRead
from magneto_api_client.models.flow_run_update import FlowRunUpdate
from magneto_api_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = magneto_api_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: bearerAuth
configuration = magneto_api_client.Configuration(
    access_token = os.environ["BEARER_TOKEN"]
)

# Enter a context with an instance of the API client
async with magneto_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = magneto_api_client.FlowRunsApi(api_client)
    id_ = 'id__example' # str | 
    flow_run_update = magneto_api_client.FlowRunUpdate() # FlowRunUpdate | 

    try:
        # Upsert Flow Run
        api_response = await api_instance.upsert_flow_run_api_v1_flow_runs_id_put(id_, flow_run_update)
        print("The response of FlowRunsApi->upsert_flow_run_api_v1_flow_runs_id_put:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling FlowRunsApi->upsert_flow_run_api_v1_flow_runs_id_put: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id_** | **str**|  | 
 **flow_run_update** | [**FlowRunUpdate**](FlowRunUpdate.md)|  | 

### Return type

[**FlowRunRead**](FlowRunRead.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful Response |  -  |
**422** | Validation Error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

