# AuditLogCreate

Audit Model.

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**organization_id** | **int** |  | [optional] 
**target_id** | **str** |  | [optional] 
**target_type** | **str** |  | 
**author** | **str** |  | [optional] 
**interface** | **str** |  | 
**action** | **str** |  | 
**status** | **str** |  | 
**return_message** | **str** |  | [optional] 
**changes** | **object** |  | [optional] 

## Example

```python
from magneto_api_client.models.audit_log_create import AuditLogCreate

# TODO update the JSON string below
json = "{}"
# create an instance of AuditLogCreate from a JSON string
audit_log_create_instance = AuditLogCreate.from_json(json)
# print the JSON string representation of the object
print(AuditLogCreate.to_json())

# convert the object into a dict
audit_log_create_dict = audit_log_create_instance.to_dict()
# create an instance of AuditLogCreate from a dict
audit_log_create_from_dict = AuditLogCreate.from_dict(audit_log_create_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


