# UserLegacyViewConfigCreate


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**path** | **str** |  | 
**config** | **object** |  | 

## Example

```python
from magneto_api_client.models.user_legacy_view_config_create import UserLegacyViewConfigCreate

# TODO update the JSON string below
json = "{}"
# create an instance of UserLegacyViewConfigCreate from a JSON string
user_legacy_view_config_create_instance = UserLegacyViewConfigCreate.from_json(json)
# print the JSON string representation of the object
print(UserLegacyViewConfigCreate.to_json())

# convert the object into a dict
user_legacy_view_config_create_dict = user_legacy_view_config_create_instance.to_dict()
# create an instance of UserLegacyViewConfigCreate from a dict
user_legacy_view_config_create_from_dict = UserLegacyViewConfigCreate.from_dict(user_legacy_view_config_create_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


