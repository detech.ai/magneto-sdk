# RankInsight

Detailed insight information about a specific rank.

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** |  | 
**entities** | [**RankEntities**](RankEntities.md) |  | [optional] 
**rules** | [**List[RuleInsight]**](RuleInsight.md) |  | [optional] 

## Example

```python
from magneto_api_client.models.rank_insight import RankInsight

# TODO update the JSON string below
json = "{}"
# create an instance of RankInsight from a JSON string
rank_insight_instance = RankInsight.from_json(json)
# print the JSON string representation of the object
print(RankInsight.to_json())

# convert the object into a dict
rank_insight_dict = rank_insight_instance.to_dict()
# create an instance of RankInsight from a dict
rank_insight_from_dict = RankInsight.from_dict(rank_insight_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


