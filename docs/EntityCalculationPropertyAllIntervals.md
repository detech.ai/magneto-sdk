# EntityCalculationPropertyAllIntervals


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**unit** | **str** |  | 
**var_1h** | [**EntityCalculationPropertyBase**](EntityCalculationPropertyBase.md) |  | [optional] 
**var_1d** | [**EntityCalculationPropertyBase**](EntityCalculationPropertyBase.md) |  | [optional] 
**var_1w** | [**EntityCalculationPropertyBase**](EntityCalculationPropertyBase.md) |  | [optional] 
**var_1m** | [**EntityCalculationPropertyBase**](EntityCalculationPropertyBase.md) |  | [optional] 

## Example

```python
from magneto_api_client.models.entity_calculation_property_all_intervals import EntityCalculationPropertyAllIntervals

# TODO update the JSON string below
json = "{}"
# create an instance of EntityCalculationPropertyAllIntervals from a JSON string
entity_calculation_property_all_intervals_instance = EntityCalculationPropertyAllIntervals.from_json(json)
# print the JSON string representation of the object
print(EntityCalculationPropertyAllIntervals.to_json())

# convert the object into a dict
entity_calculation_property_all_intervals_dict = entity_calculation_property_all_intervals_instance.to_dict()
# create an instance of EntityCalculationPropertyAllIntervals from a dict
entity_calculation_property_all_intervals_from_dict = EntityCalculationPropertyAllIntervals.from_dict(entity_calculation_property_all_intervals_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


