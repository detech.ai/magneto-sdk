# BlueprintPropertiesGroup


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**order** | **int** |  | 
**title** | **str** |  | 
**description** | **str** |  | [optional] [default to '']
**properties** | **List[str]** |  | [optional] 

## Example

```python
from magneto_api_client.models.blueprint_properties_group import BlueprintPropertiesGroup

# TODO update the JSON string below
json = "{}"
# create an instance of BlueprintPropertiesGroup from a JSON string
blueprint_properties_group_instance = BlueprintPropertiesGroup.from_json(json)
# print the JSON string representation of the object
print(BlueprintPropertiesGroup.to_json())

# convert the object into a dict
blueprint_properties_group_dict = blueprint_properties_group_instance.to_dict()
# create an instance of BlueprintPropertiesGroup from a dict
blueprint_properties_group_from_dict = BlueprintPropertiesGroup.from_dict(blueprint_properties_group_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


