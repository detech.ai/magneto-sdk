# ServiceAccountType

An enumeration.

## Enum

* `SELF_MINUS_SERVICE_MINUS_AGENT` (value: `'self-service-agent'`)

* `GALAXY` (value: `'galaxy'`)

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


