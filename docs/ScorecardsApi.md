# magneto_api_client.ScorecardsApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**create_scorecard_api_v1_scorecards_post**](ScorecardsApi.md#create_scorecard_api_v1_scorecards_post) | **POST** /api/v1/scorecards | Create Scorecard
[**delete_scorecard_api_v1_scorecards_id_delete**](ScorecardsApi.md#delete_scorecard_api_v1_scorecards_id_delete) | **DELETE** /api/v1/scorecards/{id_} | Delete Scorecard
[**evaluate_scorecard_api_v1_scorecards_id_evaluate_post**](ScorecardsApi.md#evaluate_scorecard_api_v1_scorecards_id_evaluate_post) | **POST** /api/v1/scorecards/{id_}/evaluate | Evaluate Scorecard
[**get_scorecard_api_v1_scorecards_id_get**](ScorecardsApi.md#get_scorecard_api_v1_scorecards_id_get) | **GET** /api/v1/scorecards/{id_} | Get Scorecard
[**get_scorecard_insights_api_v1_scorecards_id_insights_get**](ScorecardsApi.md#get_scorecard_insights_api_v1_scorecards_id_insights_get) | **GET** /api/v1/scorecards/{id_}/insights | Get Scorecard Insights
[**get_scorecard_insights_by_entity_api_v1_scorecards_insights_get**](ScorecardsApi.md#get_scorecard_insights_by_entity_api_v1_scorecards_insights_get) | **GET** /api/v1/scorecards/insights | Get Scorecard Insights By Entity
[**get_scorecard_results_api_v1_scorecards_results_get**](ScorecardsApi.md#get_scorecard_results_api_v1_scorecards_results_get) | **GET** /api/v1/scorecards/results | Get Scorecard Results
[**get_scorecard_results_by_scorecard_api_v1_scorecards_id_results_get**](ScorecardsApi.md#get_scorecard_results_by_scorecard_api_v1_scorecards_id_results_get) | **GET** /api/v1/scorecards/{id_}/results | Get Scorecard Results By Scorecard
[**get_scorecards_api_v1_scorecards_get**](ScorecardsApi.md#get_scorecards_api_v1_scorecards_get) | **GET** /api/v1/scorecards | Get Scorecards
[**update_scorecard_result_api_v1_scorecards_results_post**](ScorecardsApi.md#update_scorecard_result_api_v1_scorecards_results_post) | **POST** /api/v1/scorecards/results | Update Scorecard Result
[**upsert_scorecard_api_v1_scorecards_id_put**](ScorecardsApi.md#upsert_scorecard_api_v1_scorecards_id_put) | **PUT** /api/v1/scorecards/{id_} | Upsert Scorecard


# **create_scorecard_api_v1_scorecards_post**
> ScorecardRead create_scorecard_api_v1_scorecards_post(scorecard_create)

Create Scorecard

### Example

* Bearer Authentication (bearerAuth):

```python
import magneto_api_client
from magneto_api_client.models.scorecard_create import ScorecardCreate
from magneto_api_client.models.scorecard_read import ScorecardRead
from magneto_api_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = magneto_api_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: bearerAuth
configuration = magneto_api_client.Configuration(
    access_token = os.environ["BEARER_TOKEN"]
)

# Enter a context with an instance of the API client
async with magneto_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = magneto_api_client.ScorecardsApi(api_client)
    scorecard_create = magneto_api_client.ScorecardCreate() # ScorecardCreate | 

    try:
        # Create Scorecard
        api_response = await api_instance.create_scorecard_api_v1_scorecards_post(scorecard_create)
        print("The response of ScorecardsApi->create_scorecard_api_v1_scorecards_post:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling ScorecardsApi->create_scorecard_api_v1_scorecards_post: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **scorecard_create** | [**ScorecardCreate**](ScorecardCreate.md)|  | 

### Return type

[**ScorecardRead**](ScorecardRead.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**201** | Successful Response |  -  |
**422** | Validation Error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **delete_scorecard_api_v1_scorecards_id_delete**
> delete_scorecard_api_v1_scorecards_id_delete(id_)

Delete Scorecard

### Example

* Bearer Authentication (bearerAuth):

```python
import magneto_api_client
from magneto_api_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = magneto_api_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: bearerAuth
configuration = magneto_api_client.Configuration(
    access_token = os.environ["BEARER_TOKEN"]
)

# Enter a context with an instance of the API client
async with magneto_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = magneto_api_client.ScorecardsApi(api_client)
    id_ = 'id__example' # str | 

    try:
        # Delete Scorecard
        await api_instance.delete_scorecard_api_v1_scorecards_id_delete(id_)
    except Exception as e:
        print("Exception when calling ScorecardsApi->delete_scorecard_api_v1_scorecards_id_delete: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id_** | **str**|  | 

### Return type

void (empty response body)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**204** | Successful Response |  -  |
**422** | Validation Error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **evaluate_scorecard_api_v1_scorecards_id_evaluate_post**
> evaluate_scorecard_api_v1_scorecards_id_evaluate_post(id_)

Evaluate Scorecard

### Example

* Bearer Authentication (bearerAuth):

```python
import magneto_api_client
from magneto_api_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = magneto_api_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: bearerAuth
configuration = magneto_api_client.Configuration(
    access_token = os.environ["BEARER_TOKEN"]
)

# Enter a context with an instance of the API client
async with magneto_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = magneto_api_client.ScorecardsApi(api_client)
    id_ = 'id__example' # str | 

    try:
        # Evaluate Scorecard
        await api_instance.evaluate_scorecard_api_v1_scorecards_id_evaluate_post(id_)
    except Exception as e:
        print("Exception when calling ScorecardsApi->evaluate_scorecard_api_v1_scorecards_id_evaluate_post: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id_** | **str**|  | 

### Return type

void (empty response body)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**204** | Successful Response |  -  |
**422** | Validation Error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_scorecard_api_v1_scorecards_id_get**
> ScorecardRead get_scorecard_api_v1_scorecards_id_get(id_)

Get Scorecard

### Example

* Bearer Authentication (bearerAuth):

```python
import magneto_api_client
from magneto_api_client.models.scorecard_read import ScorecardRead
from magneto_api_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = magneto_api_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: bearerAuth
configuration = magneto_api_client.Configuration(
    access_token = os.environ["BEARER_TOKEN"]
)

# Enter a context with an instance of the API client
async with magneto_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = magneto_api_client.ScorecardsApi(api_client)
    id_ = 'id__example' # str | 

    try:
        # Get Scorecard
        api_response = await api_instance.get_scorecard_api_v1_scorecards_id_get(id_)
        print("The response of ScorecardsApi->get_scorecard_api_v1_scorecards_id_get:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling ScorecardsApi->get_scorecard_api_v1_scorecards_id_get: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id_** | **str**|  | 

### Return type

[**ScorecardRead**](ScorecardRead.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful Response |  -  |
**422** | Validation Error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_scorecard_insights_api_v1_scorecards_id_insights_get**
> ScorecardInsights get_scorecard_insights_api_v1_scorecards_id_insights_get(id_, entity_ids=entity_ids, include_entity_ids=include_entity_ids, entity_filters=entity_filters, q=q, request_body=request_body)

Get Scorecard Insights

Get insights about a scorecard's performance across entities.

Args:
    id_: ID of the scorecard to analyze
    entity_ids: List of entity IDs to include or exclude based on filter_mode
    include_entity_ids: Whether to include entity IDs in the response

Returns:
    ScorecardInsights with statistics about ranks and rules

### Example

* Bearer Authentication (bearerAuth):

```python
import magneto_api_client
from magneto_api_client.models.scorecard_insights import ScorecardInsights
from magneto_api_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = magneto_api_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: bearerAuth
configuration = magneto_api_client.Configuration(
    access_token = os.environ["BEARER_TOKEN"]
)

# Enter a context with an instance of the API client
async with magneto_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = magneto_api_client.ScorecardsApi(api_client)
    id_ = 'id__example' # str | 
    entity_ids = ['entity_ids_example'] # List[str] |  (optional)
    include_entity_ids = False # bool |  (optional) (default to False)
    entity_filters = ['entity_filters_example'] # List[str] |  (optional)
    q = 'q_example' # str |  (optional)
    request_body = ['request_body_example'] # List[str] |  (optional)

    try:
        # Get Scorecard Insights
        api_response = await api_instance.get_scorecard_insights_api_v1_scorecards_id_insights_get(id_, entity_ids=entity_ids, include_entity_ids=include_entity_ids, entity_filters=entity_filters, q=q, request_body=request_body)
        print("The response of ScorecardsApi->get_scorecard_insights_api_v1_scorecards_id_insights_get:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling ScorecardsApi->get_scorecard_insights_api_v1_scorecards_id_insights_get: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id_** | **str**|  | 
 **entity_ids** | [**List[str]**](str.md)|  | [optional] 
 **include_entity_ids** | **bool**|  | [optional] [default to False]
 **entity_filters** | [**List[str]**](str.md)|  | [optional] 
 **q** | **str**|  | [optional] 
 **request_body** | [**List[str]**](str.md)|  | [optional] 

### Return type

[**ScorecardInsights**](ScorecardInsights.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful Response |  -  |
**422** | Validation Error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_scorecard_insights_by_entity_api_v1_scorecards_insights_get**
> EntityScorecardsInsights get_scorecard_insights_by_entity_api_v1_scorecards_insights_get(entity_id)

Get Scorecard Insights By Entity

### Example

* Bearer Authentication (bearerAuth):

```python
import magneto_api_client
from magneto_api_client.models.entity_scorecards_insights import EntityScorecardsInsights
from magneto_api_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = magneto_api_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: bearerAuth
configuration = magneto_api_client.Configuration(
    access_token = os.environ["BEARER_TOKEN"]
)

# Enter a context with an instance of the API client
async with magneto_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = magneto_api_client.ScorecardsApi(api_client)
    entity_id = 'entity_id_example' # str | 

    try:
        # Get Scorecard Insights By Entity
        api_response = await api_instance.get_scorecard_insights_by_entity_api_v1_scorecards_insights_get(entity_id)
        print("The response of ScorecardsApi->get_scorecard_insights_by_entity_api_v1_scorecards_insights_get:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling ScorecardsApi->get_scorecard_insights_by_entity_api_v1_scorecards_insights_get: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **entity_id** | **str**|  | 

### Return type

[**EntityScorecardsInsights**](EntityScorecardsInsights.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful Response |  -  |
**422** | Validation Error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_scorecard_results_api_v1_scorecards_results_get**
> CursorPageScorecardResultRead get_scorecard_results_api_v1_scorecards_results_get(filters=filters, sorts=sorts, cursor=cursor, size=size, include_total=include_total)

Get Scorecard Results

### Example

* Bearer Authentication (bearerAuth):

```python
import magneto_api_client
from magneto_api_client.models.cursor_page_scorecard_result_read import CursorPageScorecardResultRead
from magneto_api_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = magneto_api_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: bearerAuth
configuration = magneto_api_client.Configuration(
    access_token = os.environ["BEARER_TOKEN"]
)

# Enter a context with an instance of the API client
async with magneto_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = magneto_api_client.ScorecardsApi(api_client)
    filters = ['filters_example'] # List[str] |  (optional)
    sorts = ['sorts_example'] # List[str] |  (optional)
    cursor = 'cursor_example' # str |  (optional)
    size = 50 # int |  (optional) (default to 50)
    include_total = False # bool |  (optional) (default to False)

    try:
        # Get Scorecard Results
        api_response = await api_instance.get_scorecard_results_api_v1_scorecards_results_get(filters=filters, sorts=sorts, cursor=cursor, size=size, include_total=include_total)
        print("The response of ScorecardsApi->get_scorecard_results_api_v1_scorecards_results_get:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling ScorecardsApi->get_scorecard_results_api_v1_scorecards_results_get: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filters** | [**List[str]**](str.md)|  | [optional] 
 **sorts** | [**List[str]**](str.md)|  | [optional] 
 **cursor** | **str**|  | [optional] 
 **size** | **int**|  | [optional] [default to 50]
 **include_total** | **bool**|  | [optional] [default to False]

### Return type

[**CursorPageScorecardResultRead**](CursorPageScorecardResultRead.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful Response |  -  |
**422** | Validation Error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_scorecard_results_by_scorecard_api_v1_scorecards_id_results_get**
> CursorPageScorecardResultRead get_scorecard_results_by_scorecard_api_v1_scorecards_id_results_get(id_, rules=rules, filters=filters, sorts=sorts, entity_filters=entity_filters, q=q, cursor=cursor, size=size, include_total=include_total)

Get Scorecard Results By Scorecard

### Example

* Bearer Authentication (bearerAuth):

```python
import magneto_api_client
from magneto_api_client.models.cursor_page_scorecard_result_read import CursorPageScorecardResultRead
from magneto_api_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = magneto_api_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: bearerAuth
configuration = magneto_api_client.Configuration(
    access_token = os.environ["BEARER_TOKEN"]
)

# Enter a context with an instance of the API client
async with magneto_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = magneto_api_client.ScorecardsApi(api_client)
    id_ = 'id__example' # str | 
    rules = ['rules_example'] # List[str] |  (optional)
    filters = ['filters_example'] # List[str] |  (optional)
    sorts = ['sorts_example'] # List[str] |  (optional)
    entity_filters = ['entity_filters_example'] # List[str] |  (optional)
    q = 'q_example' # str |  (optional)
    cursor = 'cursor_example' # str |  (optional)
    size = 50 # int |  (optional) (default to 50)
    include_total = False # bool |  (optional) (default to False)

    try:
        # Get Scorecard Results By Scorecard
        api_response = await api_instance.get_scorecard_results_by_scorecard_api_v1_scorecards_id_results_get(id_, rules=rules, filters=filters, sorts=sorts, entity_filters=entity_filters, q=q, cursor=cursor, size=size, include_total=include_total)
        print("The response of ScorecardsApi->get_scorecard_results_by_scorecard_api_v1_scorecards_id_results_get:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling ScorecardsApi->get_scorecard_results_by_scorecard_api_v1_scorecards_id_results_get: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id_** | **str**|  | 
 **rules** | [**List[str]**](str.md)|  | [optional] 
 **filters** | [**List[str]**](str.md)|  | [optional] 
 **sorts** | [**List[str]**](str.md)|  | [optional] 
 **entity_filters** | [**List[str]**](str.md)|  | [optional] 
 **q** | **str**|  | [optional] 
 **cursor** | **str**|  | [optional] 
 **size** | **int**|  | [optional] [default to 50]
 **include_total** | **bool**|  | [optional] [default to False]

### Return type

[**CursorPageScorecardResultRead**](CursorPageScorecardResultRead.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful Response |  -  |
**422** | Validation Error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_scorecards_api_v1_scorecards_get**
> CursorPageScorecardRead get_scorecards_api_v1_scorecards_get(filters=filters, sorts=sorts, cursor=cursor, size=size, include_total=include_total)

Get Scorecards

### Example

* Bearer Authentication (bearerAuth):

```python
import magneto_api_client
from magneto_api_client.models.cursor_page_scorecard_read import CursorPageScorecardRead
from magneto_api_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = magneto_api_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: bearerAuth
configuration = magneto_api_client.Configuration(
    access_token = os.environ["BEARER_TOKEN"]
)

# Enter a context with an instance of the API client
async with magneto_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = magneto_api_client.ScorecardsApi(api_client)
    filters = ['filters_example'] # List[str] |  (optional)
    sorts = ['sorts_example'] # List[str] |  (optional)
    cursor = 'cursor_example' # str |  (optional)
    size = 50 # int |  (optional) (default to 50)
    include_total = False # bool |  (optional) (default to False)

    try:
        # Get Scorecards
        api_response = await api_instance.get_scorecards_api_v1_scorecards_get(filters=filters, sorts=sorts, cursor=cursor, size=size, include_total=include_total)
        print("The response of ScorecardsApi->get_scorecards_api_v1_scorecards_get:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling ScorecardsApi->get_scorecards_api_v1_scorecards_get: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filters** | [**List[str]**](str.md)|  | [optional] 
 **sorts** | [**List[str]**](str.md)|  | [optional] 
 **cursor** | **str**|  | [optional] 
 **size** | **int**|  | [optional] [default to 50]
 **include_total** | **bool**|  | [optional] [default to False]

### Return type

[**CursorPageScorecardRead**](CursorPageScorecardRead.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful Response |  -  |
**422** | Validation Error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **update_scorecard_result_api_v1_scorecards_results_post**
> ScorecardResultRead update_scorecard_result_api_v1_scorecards_results_post(scorecard_result_update)

Update Scorecard Result

### Example

* Bearer Authentication (bearerAuth):

```python
import magneto_api_client
from magneto_api_client.models.scorecard_result_read import ScorecardResultRead
from magneto_api_client.models.scorecard_result_update import ScorecardResultUpdate
from magneto_api_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = magneto_api_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: bearerAuth
configuration = magneto_api_client.Configuration(
    access_token = os.environ["BEARER_TOKEN"]
)

# Enter a context with an instance of the API client
async with magneto_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = magneto_api_client.ScorecardsApi(api_client)
    scorecard_result_update = magneto_api_client.ScorecardResultUpdate() # ScorecardResultUpdate | 

    try:
        # Update Scorecard Result
        api_response = await api_instance.update_scorecard_result_api_v1_scorecards_results_post(scorecard_result_update)
        print("The response of ScorecardsApi->update_scorecard_result_api_v1_scorecards_results_post:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling ScorecardsApi->update_scorecard_result_api_v1_scorecards_results_post: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **scorecard_result_update** | [**ScorecardResultUpdate**](ScorecardResultUpdate.md)|  | 

### Return type

[**ScorecardResultRead**](ScorecardResultRead.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful Response |  -  |
**422** | Validation Error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **upsert_scorecard_api_v1_scorecards_id_put**
> ScorecardRead upsert_scorecard_api_v1_scorecards_id_put(id_, scorecard_update)

Upsert Scorecard

### Example

* Bearer Authentication (bearerAuth):

```python
import magneto_api_client
from magneto_api_client.models.scorecard_read import ScorecardRead
from magneto_api_client.models.scorecard_update import ScorecardUpdate
from magneto_api_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = magneto_api_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: bearerAuth
configuration = magneto_api_client.Configuration(
    access_token = os.environ["BEARER_TOKEN"]
)

# Enter a context with an instance of the API client
async with magneto_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = magneto_api_client.ScorecardsApi(api_client)
    id_ = 'id__example' # str | 
    scorecard_update = magneto_api_client.ScorecardUpdate() # ScorecardUpdate | 

    try:
        # Upsert Scorecard
        api_response = await api_instance.upsert_scorecard_api_v1_scorecards_id_put(id_, scorecard_update)
        print("The response of ScorecardsApi->upsert_scorecard_api_v1_scorecards_id_put:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling ScorecardsApi->upsert_scorecard_api_v1_scorecards_id_put: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id_** | **str**|  | 
 **scorecard_update** | [**ScorecardUpdate**](ScorecardUpdate.md)|  | 

### Return type

[**ScorecardRead**](ScorecardRead.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful Response |  -  |
**422** | Validation Error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

