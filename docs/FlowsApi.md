# magneto_api_client.FlowsApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**create_flow_api_v1_flows_post**](FlowsApi.md#create_flow_api_v1_flows_post) | **POST** /api/v1/flows | Create Flow
[**delete_flow_api_v1_flows_id_delete**](FlowsApi.md#delete_flow_api_v1_flows_id_delete) | **DELETE** /api/v1/flows/{id_} | Delete Flow
[**get_flow_api_v1_flows_id_get**](FlowsApi.md#get_flow_api_v1_flows_id_get) | **GET** /api/v1/flows/{id_} | Get Flow
[**get_flows_api_v1_flows_get**](FlowsApi.md#get_flows_api_v1_flows_get) | **GET** /api/v1/flows | Get Flows
[**upsert_flow_api_v1_flows_id_put**](FlowsApi.md#upsert_flow_api_v1_flows_id_put) | **PUT** /api/v1/flows/{id_} | Upsert Flow


# **create_flow_api_v1_flows_post**
> FlowRead create_flow_api_v1_flows_post(flow_create, trigger=trigger)

Create Flow

### Example

* Bearer Authentication (bearerAuth):

```python
import magneto_api_client
from magneto_api_client.models.flow_create import FlowCreate
from magneto_api_client.models.flow_read import FlowRead
from magneto_api_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = magneto_api_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: bearerAuth
configuration = magneto_api_client.Configuration(
    access_token = os.environ["BEARER_TOKEN"]
)

# Enter a context with an instance of the API client
async with magneto_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = magneto_api_client.FlowsApi(api_client)
    flow_create = magneto_api_client.FlowCreate() # FlowCreate | 
    trigger = True # bool |  (optional) (default to True)

    try:
        # Create Flow
        api_response = await api_instance.create_flow_api_v1_flows_post(flow_create, trigger=trigger)
        print("The response of FlowsApi->create_flow_api_v1_flows_post:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling FlowsApi->create_flow_api_v1_flows_post: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **flow_create** | [**FlowCreate**](FlowCreate.md)|  | 
 **trigger** | **bool**|  | [optional] [default to True]

### Return type

[**FlowRead**](FlowRead.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**201** | Successful Response |  -  |
**422** | Validation Error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **delete_flow_api_v1_flows_id_delete**
> delete_flow_api_v1_flows_id_delete(id_)

Delete Flow

### Example

* Bearer Authentication (bearerAuth):

```python
import magneto_api_client
from magneto_api_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = magneto_api_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: bearerAuth
configuration = magneto_api_client.Configuration(
    access_token = os.environ["BEARER_TOKEN"]
)

# Enter a context with an instance of the API client
async with magneto_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = magneto_api_client.FlowsApi(api_client)
    id_ = 'id__example' # str | 

    try:
        # Delete Flow
        await api_instance.delete_flow_api_v1_flows_id_delete(id_)
    except Exception as e:
        print("Exception when calling FlowsApi->delete_flow_api_v1_flows_id_delete: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id_** | **str**|  | 

### Return type

void (empty response body)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**204** | Successful Response |  -  |
**422** | Validation Error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_flow_api_v1_flows_id_get**
> FlowRead get_flow_api_v1_flows_id_get(id_)

Get Flow

### Example

* Bearer Authentication (bearerAuth):

```python
import magneto_api_client
from magneto_api_client.models.flow_read import FlowRead
from magneto_api_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = magneto_api_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: bearerAuth
configuration = magneto_api_client.Configuration(
    access_token = os.environ["BEARER_TOKEN"]
)

# Enter a context with an instance of the API client
async with magneto_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = magneto_api_client.FlowsApi(api_client)
    id_ = 'id__example' # str | 

    try:
        # Get Flow
        api_response = await api_instance.get_flow_api_v1_flows_id_get(id_)
        print("The response of FlowsApi->get_flow_api_v1_flows_id_get:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling FlowsApi->get_flow_api_v1_flows_id_get: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id_** | **str**|  | 

### Return type

[**FlowRead**](FlowRead.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful Response |  -  |
**422** | Validation Error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_flows_api_v1_flows_get**
> CursorPageFlowRead get_flows_api_v1_flows_get(filters=filters, sorts=sorts, cursor=cursor, size=size, include_total=include_total)

Get Flows

### Example

* Bearer Authentication (bearerAuth):

```python
import magneto_api_client
from magneto_api_client.models.cursor_page_flow_read import CursorPageFlowRead
from magneto_api_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = magneto_api_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: bearerAuth
configuration = magneto_api_client.Configuration(
    access_token = os.environ["BEARER_TOKEN"]
)

# Enter a context with an instance of the API client
async with magneto_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = magneto_api_client.FlowsApi(api_client)
    filters = ['filters_example'] # List[str] |  (optional)
    sorts = ['sorts_example'] # List[str] |  (optional)
    cursor = 'cursor_example' # str |  (optional)
    size = 50 # int |  (optional) (default to 50)
    include_total = False # bool |  (optional) (default to False)

    try:
        # Get Flows
        api_response = await api_instance.get_flows_api_v1_flows_get(filters=filters, sorts=sorts, cursor=cursor, size=size, include_total=include_total)
        print("The response of FlowsApi->get_flows_api_v1_flows_get:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling FlowsApi->get_flows_api_v1_flows_get: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filters** | [**List[str]**](str.md)|  | [optional] 
 **sorts** | [**List[str]**](str.md)|  | [optional] 
 **cursor** | **str**|  | [optional] 
 **size** | **int**|  | [optional] [default to 50]
 **include_total** | **bool**|  | [optional] [default to False]

### Return type

[**CursorPageFlowRead**](CursorPageFlowRead.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful Response |  -  |
**422** | Validation Error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **upsert_flow_api_v1_flows_id_put**
> FlowRead upsert_flow_api_v1_flows_id_put(id_, flow_update, trigger=trigger)

Upsert Flow

### Example

* Bearer Authentication (bearerAuth):

```python
import magneto_api_client
from magneto_api_client.models.flow_read import FlowRead
from magneto_api_client.models.flow_update import FlowUpdate
from magneto_api_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = magneto_api_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: bearerAuth
configuration = magneto_api_client.Configuration(
    access_token = os.environ["BEARER_TOKEN"]
)

# Enter a context with an instance of the API client
async with magneto_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = magneto_api_client.FlowsApi(api_client)
    id_ = 'id__example' # str | 
    flow_update = magneto_api_client.FlowUpdate() # FlowUpdate | 
    trigger = True # bool |  (optional) (default to True)

    try:
        # Upsert Flow
        api_response = await api_instance.upsert_flow_api_v1_flows_id_put(id_, flow_update, trigger=trigger)
        print("The response of FlowsApi->upsert_flow_api_v1_flows_id_put:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling FlowsApi->upsert_flow_api_v1_flows_id_put: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id_** | **str**|  | 
 **flow_update** | [**FlowUpdate**](FlowUpdate.md)|  | 
 **trigger** | **bool**|  | [optional] [default to True]

### Return type

[**FlowRead**](FlowRead.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful Response |  -  |
**422** | Validation Error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

