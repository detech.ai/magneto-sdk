# AuditLog

Base class for models that have a bigint ID.

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**sid** | **int** |  | 
**organization_id** | **int** |  | [optional] 
**target_id** | **str** |  | [optional] 
**target_type** | **str** |  | 
**author** | **str** |  | [optional] 
**interface** | **str** |  | 
**action** | **str** |  | 
**status** | **str** |  | 
**return_message** | **str** |  | [optional] 
**changes** | **object** |  | [optional] 
**var_date** | **datetime** |  | 

## Example

```python
from magneto_api_client.models.audit_log import AuditLog

# TODO update the JSON string below
json = "{}"
# create an instance of AuditLog from a JSON string
audit_log_instance = AuditLog.from_json(json)
# print the JSON string representation of the object
print(AuditLog.to_json())

# convert the object into a dict
audit_log_dict = audit_log_instance.to_dict()
# create an instance of AuditLog from a dict
audit_log_from_dict = AuditLog.from_dict(audit_log_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


