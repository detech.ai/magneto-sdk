# magneto_api_client.TasksApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**create_entities_upsert_task_api_v1_tasks_entities_upsert_post**](TasksApi.md#create_entities_upsert_task_api_v1_tasks_entities_upsert_post) | **POST** /api/v1/tasks/entities/upsert | Create Entities Upsert Task
[**create_task_api_v1_tasks_post**](TasksApi.md#create_task_api_v1_tasks_post) | **POST** /api/v1/tasks | Create Task
[**get_task_by_id_api_v1_tasks_id_get**](TasksApi.md#get_task_by_id_api_v1_tasks_id_get) | **GET** /api/v1/tasks/{id_} | Get Task By Id


# **create_entities_upsert_task_api_v1_tasks_entities_upsert_post**
> TaskRead create_entities_upsert_task_api_v1_tasks_entities_upsert_post(entity_update, patch=patch, unmapped_properties=unmapped_properties, update_properties_and_relations_only=update_properties_and_relations_only, create_relations=create_relations, override_sources=override_sources)

Create Entities Upsert Task

### Example

* Bearer Authentication (bearerAuth):

```python
import magneto_api_client
from magneto_api_client.models.entity_update import EntityUpdate
from magneto_api_client.models.task_read import TaskRead
from magneto_api_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = magneto_api_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: bearerAuth
configuration = magneto_api_client.Configuration(
    access_token = os.environ["BEARER_TOKEN"]
)

# Enter a context with an instance of the API client
async with magneto_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = magneto_api_client.TasksApi(api_client)
    entity_update = [magneto_api_client.EntityUpdate()] # List[EntityUpdate] | 
    patch = False # bool | Patch entity instead of full update (optional) (default to False)
    unmapped_properties = magneto_api_client.UnmappedPropertyValue() # UnmappedPropertyValue |  (optional)
    update_properties_and_relations_only = False # bool | Update only properties and relations (optional) (default to False)
    create_relations = False # bool | Create entity relations if they dont exist (optional) (default to False)
    override_sources = False # bool | Allow updating entity data that have an active source (optional) (default to False)

    try:
        # Create Entities Upsert Task
        api_response = await api_instance.create_entities_upsert_task_api_v1_tasks_entities_upsert_post(entity_update, patch=patch, unmapped_properties=unmapped_properties, update_properties_and_relations_only=update_properties_and_relations_only, create_relations=create_relations, override_sources=override_sources)
        print("The response of TasksApi->create_entities_upsert_task_api_v1_tasks_entities_upsert_post:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling TasksApi->create_entities_upsert_task_api_v1_tasks_entities_upsert_post: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **entity_update** | [**List[EntityUpdate]**](EntityUpdate.md)|  | 
 **patch** | **bool**| Patch entity instead of full update | [optional] [default to False]
 **unmapped_properties** | [**UnmappedPropertyValue**](.md)|  | [optional] 
 **update_properties_and_relations_only** | **bool**| Update only properties and relations | [optional] [default to False]
 **create_relations** | **bool**| Create entity relations if they dont exist | [optional] [default to False]
 **override_sources** | **bool**| Allow updating entity data that have an active source | [optional] [default to False]

### Return type

[**TaskRead**](TaskRead.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**202** | Successful Response |  -  |
**422** | Validation Error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **create_task_api_v1_tasks_post**
> TaskRead create_task_api_v1_tasks_post(task_create)

Create Task

### Example

* Bearer Authentication (bearerAuth):

```python
import magneto_api_client
from magneto_api_client.models.task_create import TaskCreate
from magneto_api_client.models.task_read import TaskRead
from magneto_api_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = magneto_api_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: bearerAuth
configuration = magneto_api_client.Configuration(
    access_token = os.environ["BEARER_TOKEN"]
)

# Enter a context with an instance of the API client
async with magneto_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = magneto_api_client.TasksApi(api_client)
    task_create = magneto_api_client.TaskCreate() # TaskCreate | 

    try:
        # Create Task
        api_response = await api_instance.create_task_api_v1_tasks_post(task_create)
        print("The response of TasksApi->create_task_api_v1_tasks_post:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling TasksApi->create_task_api_v1_tasks_post: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **task_create** | [**TaskCreate**](TaskCreate.md)|  | 

### Return type

[**TaskRead**](TaskRead.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**202** | Successful Response |  -  |
**422** | Validation Error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_task_by_id_api_v1_tasks_id_get**
> TaskRead get_task_by_id_api_v1_tasks_id_get(id_)

Get Task By Id

### Example

* Bearer Authentication (bearerAuth):

```python
import magneto_api_client
from magneto_api_client.models.task_read import TaskRead
from magneto_api_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = magneto_api_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: bearerAuth
configuration = magneto_api_client.Configuration(
    access_token = os.environ["BEARER_TOKEN"]
)

# Enter a context with an instance of the API client
async with magneto_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = magneto_api_client.TasksApi(api_client)
    id_ = 'id__example' # str | 

    try:
        # Get Task By Id
        api_response = await api_instance.get_task_by_id_api_v1_tasks_id_get(id_)
        print("The response of TasksApi->get_task_by_id_api_v1_tasks_id_get:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling TasksApi->get_task_by_id_api_v1_tasks_id_get: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id_** | **str**|  | 

### Return type

[**TaskRead**](TaskRead.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful Response |  -  |
**422** | Validation Error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

