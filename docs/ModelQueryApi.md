# magneto_api_client.ModelQueryApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**execute_query_api_v1_model_query_post**](ModelQueryApi.md#execute_query_api_v1_model_query_post) | **POST** /api/v1/model/query | Execute Query


# **execute_query_api_v1_model_query_post**
> object execute_query_api_v1_model_query_post(query_payload)

Execute Query

### Example

* Bearer Authentication (bearerAuth):

```python
import magneto_api_client
from magneto_api_client.models.query_payload import QueryPayload
from magneto_api_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = magneto_api_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: bearerAuth
configuration = magneto_api_client.Configuration(
    access_token = os.environ["BEARER_TOKEN"]
)

# Enter a context with an instance of the API client
async with magneto_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = magneto_api_client.ModelQueryApi(api_client)
    query_payload = magneto_api_client.QueryPayload() # QueryPayload | 

    try:
        # Execute Query
        api_response = await api_instance.execute_query_api_v1_model_query_post(query_payload)
        print("The response of ModelQueryApi->execute_query_api_v1_model_query_post:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling ModelQueryApi->execute_query_api_v1_model_query_post: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **query_payload** | [**QueryPayload**](QueryPayload.md)|  | 

### Return type

**object**

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful Response |  -  |
**422** | Validation Error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

