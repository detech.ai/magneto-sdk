# JsonSchema

Base class for models that exclude unset fields.  **Note**: This overrides any `exclude_unset` kwarg passed for the model and its nested models.

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**type** | [**Type**](Type.md) |  | [optional] 
**title** | **str** |  | [optional] 
**description** | **str** |  | [optional] 
**format** | **str** |  | [optional] 
**properties** | [**Dict[str, JsonSchema]**](JsonSchema.md) |  | [optional] 
**items** | [**JsonSchema**](JsonSchema.md) |  | [optional] 
**icon** | **str** |  | [optional] 
**enum_colors** | **Dict[str, str]** |  | [optional] 
**enum_icons** | **Dict[str, str]** |  | [optional] 
**enum** | **List[object]** |  | [optional] 
**additional_properties** | **bool** |  | [optional] 
**read_only** | **bool** |  | [optional] 

## Example

```python
from magneto_api_client.models.json_schema import JsonSchema

# TODO update the JSON string below
json = "{}"
# create an instance of JsonSchema from a JSON string
json_schema_instance = JsonSchema.from_json(json)
# print the JSON string representation of the object
print(JsonSchema.to_json())

# convert the object into a dict
json_schema_dict = json_schema_instance.to_dict()
# create an instance of JsonSchema from a dict
json_schema_from_dict = JsonSchema.from_dict(json_schema_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


