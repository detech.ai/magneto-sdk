# OriginType

An enumeration.

## Enum

* `AUTOMATION` (value: `'automation'`)

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


