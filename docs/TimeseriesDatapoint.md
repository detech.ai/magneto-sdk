# TimeseriesDatapoint


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**timestamp** | **datetime** |  | 
**value** | **object** |  | 

## Example

```python
from magneto_api_client.models.timeseries_datapoint import TimeseriesDatapoint

# TODO update the JSON string below
json = "{}"
# create an instance of TimeseriesDatapoint from a JSON string
timeseries_datapoint_instance = TimeseriesDatapoint.from_json(json)
# print the JSON string representation of the object
print(TimeseriesDatapoint.to_json())

# convert the object into a dict
timeseries_datapoint_dict = timeseries_datapoint_instance.to_dict()
# create an instance of TimeseriesDatapoint from a dict
timeseries_datapoint_from_dict = TimeseriesDatapoint.from_dict(timeseries_datapoint_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


