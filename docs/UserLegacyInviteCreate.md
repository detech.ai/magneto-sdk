# UserLegacyInviteCreate


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **str** |  | 
**email** | **str** |  | 
**role** | [**UserLegacyRoleType**](UserLegacyRoleType.md) |  | [optional] 

## Example

```python
from magneto_api_client.models.user_legacy_invite_create import UserLegacyInviteCreate

# TODO update the JSON string below
json = "{}"
# create an instance of UserLegacyInviteCreate from a JSON string
user_legacy_invite_create_instance = UserLegacyInviteCreate.from_json(json)
# print the JSON string representation of the object
print(UserLegacyInviteCreate.to_json())

# convert the object into a dict
user_legacy_invite_create_dict = user_legacy_invite_create_instance.to_dict()
# create an instance of UserLegacyInviteCreate from a dict
user_legacy_invite_create_from_dict = UserLegacyInviteCreate.from_dict(user_legacy_invite_create_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


