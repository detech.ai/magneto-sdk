# FlowUpdate

Flow Update Model.

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** |  | 
**title** | **str** |  | 
**description** | **str** |  | [optional] 
**icon** | **str** |  | [optional] 
**is_active** | **bool** |  | [optional] [default to True]
**needs_approval** | **bool** |  | [optional] [default to False]
**order** | **int** |  | [optional] [default to 0]
**type** | [**FlowType**](FlowType.md) |  | 
**arguments** | **object** |  | [optional] 
**secrets** | **object** |  | [optional] 
**triggers** | [**List[FlowTrigger]**](FlowTrigger.md) |  | [optional] 
**actions** | [**List[FlowAction]**](FlowAction.md) |  | [optional] 
**outputs** | [**Dict[str, FlowOutput]**](FlowOutput.md) |  | [optional] 
**tags** | [**List[Tag]**](Tag.md) |  | [optional] 

## Example

```python
from magneto_api_client.models.flow_update import FlowUpdate

# TODO update the JSON string below
json = "{}"
# create an instance of FlowUpdate from a JSON string
flow_update_instance = FlowUpdate.from_json(json)
# print the JSON string representation of the object
print(FlowUpdate.to_json())

# convert the object into a dict
flow_update_dict = flow_update_instance.to_dict()
# create an instance of FlowUpdate from a dict
flow_update_from_dict = FlowUpdate.from_dict(flow_update_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


