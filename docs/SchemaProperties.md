# SchemaProperties

Base class for models that exclude unset fields.  **Note**: This overrides any `exclude_unset` kwarg passed for the model and its nested models.

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**type** | **str** |  | 
**title** | **str** |  | [optional] 
**description** | **str** |  | [optional] 
**format** | **str** |  | [optional] 
**properties** | [**Dict[str, JsonSchema]**](JsonSchema.md) |  | [optional] 
**items** | [**JsonSchema**](JsonSchema.md) |  | [optional] 
**icon** | **str** |  | [optional] 
**enum_colors** | **Dict[str, str]** |  | [optional] 
**enum_icons** | **Dict[str, str]** |  | [optional] 
**enum** | **List[object]** |  | [optional] 
**additional_properties** | **bool** |  | [optional] 
**read_only** | **bool** |  | [optional] 

## Example

```python
from magneto_api_client.models.schema_properties import SchemaProperties

# TODO update the JSON string below
json = "{}"
# create an instance of SchemaProperties from a JSON string
schema_properties_instance = SchemaProperties.from_json(json)
# print the JSON string representation of the object
print(SchemaProperties.to_json())

# convert the object into a dict
schema_properties_dict = schema_properties_instance.to_dict()
# create an instance of SchemaProperties from a dict
schema_properties_from_dict = SchemaProperties.from_dict(schema_properties_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


