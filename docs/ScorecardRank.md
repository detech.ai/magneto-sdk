# ScorecardRank

Base class for models that exclude unset fields.  **Note**: This overrides any `exclude_unset` kwarg passed for the model and its nested models.

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** |  | 
**title** | **str** |  | [optional] 
**description** | **str** |  | [optional] 
**icon** | **str** |  | [optional] 
**rules** | [**List[ScorecardRule]**](ScorecardRule.md) |  | [optional] 

## Example

```python
from magneto_api_client.models.scorecard_rank import ScorecardRank

# TODO update the JSON string below
json = "{}"
# create an instance of ScorecardRank from a JSON string
scorecard_rank_instance = ScorecardRank.from_json(json)
# print the JSON string representation of the object
print(ScorecardRank.to_json())

# convert the object into a dict
scorecard_rank_dict = scorecard_rank_instance.to_dict()
# create an instance of ScorecardRank from a dict
scorecard_rank_from_dict = ScorecardRank.from_dict(scorecard_rank_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


