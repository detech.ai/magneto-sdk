# SuggestionRead

Suggestion Read Model.

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** |  | [optional] 
**title** | **str** |  | 
**description** | **str** |  | [optional] 
**origin_type** | [**OriginType**](OriginType.md) |  | 
**origin_id** | **str** |  | 
**source_type** | [**SourceType**](SourceType.md) |  | 
**source_id** | **str** |  | 
**source_context** | **object** |  | [optional] 
**target_type** | [**TargetType**](TargetType.md) |  | 
**target_id** | **str** |  | 
**target_context** | **object** |  | [optional] 
**target_id_override** | **str** |  | [optional] 
**data** | **object** |  | [optional] 
**ignored** | **bool** |  | [optional] [default to False]
**auto_apply** | **bool** |  | [optional] [default to False]
**last_applied_at** | **datetime** |  | [optional] 
**created_at** | **datetime** |  | 
**created_by** | **int** |  | [optional] 
**updated_at** | **datetime** |  | 
**updated_by** | **int** |  | [optional] 
**additional_info** | **object** |  | [optional] 

## Example

```python
from magneto_api_client.models.suggestion_read import SuggestionRead

# TODO update the JSON string below
json = "{}"
# create an instance of SuggestionRead from a JSON string
suggestion_read_instance = SuggestionRead.from_json(json)
# print the JSON string representation of the object
print(SuggestionRead.to_json())

# convert the object into a dict
suggestion_read_dict = suggestion_read_instance.to_dict()
# create an instance of SuggestionRead from a dict
suggestion_read_from_dict = SuggestionRead.from_dict(suggestion_read_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


