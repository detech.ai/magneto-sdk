# AgentRead


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** |  | 
**type** | [**AgentType**](AgentType.md) |  | 
**version** | **str** |  | 
**inactive_threshold** | **int** |  | [optional] [default to 300]
**created_at** | **datetime** |  | 
**last_heartbeat_at** | **datetime** |  | [optional] 
**status** | [**AgentStatus**](AgentStatus.md) |  | 

## Example

```python
from magneto_api_client.models.agent_read import AgentRead

# TODO update the JSON string below
json = "{}"
# create an instance of AgentRead from a JSON string
agent_read_instance = AgentRead.from_json(json)
# print the JSON string representation of the object
print(AgentRead.to_json())

# convert the object into a dict
agent_read_dict = agent_read_instance.to_dict()
# create an instance of AgentRead from a dict
agent_read_from_dict = AgentRead.from_dict(agent_read_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


