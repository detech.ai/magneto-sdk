# BlueprintOptions


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**properties_groups** | [**Dict[str, BlueprintPropertiesGroup]**](BlueprintPropertiesGroup.md) |  | [optional] 
**show_in_side_bar** | **bool** | Flag to show or hide blueprint in the sidebar | [optional] [default to True]

## Example

```python
from magneto_api_client.models.blueprint_options import BlueprintOptions

# TODO update the JSON string below
json = "{}"
# create an instance of BlueprintOptions from a JSON string
blueprint_options_instance = BlueprintOptions.from_json(json)
# print the JSON string representation of the object
print(BlueprintOptions.to_json())

# convert the object into a dict
blueprint_options_dict = blueprint_options_instance.to_dict()
# create an instance of BlueprintOptions from a dict
blueprint_options_from_dict = BlueprintOptions.from_dict(blueprint_options_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


