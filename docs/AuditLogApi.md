# magneto_api_client.AuditLogApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**create_audit_log_entry_api_v1_audit_log_post**](AuditLogApi.md#create_audit_log_entry_api_v1_audit_log_post) | **POST** /api/v1/audit-log | Create Audit Log Entry
[**get_audit_log_api_v1_audit_log_get**](AuditLogApi.md#get_audit_log_api_v1_audit_log_get) | **GET** /api/v1/audit-log | Get Audit Log
[**get_audit_log_changes_api_v1_audit_log_sid_changes_get**](AuditLogApi.md#get_audit_log_changes_api_v1_audit_log_sid_changes_get) | **GET** /api/v1/audit-log/{sid}/changes | Get Audit Log Changes


# **create_audit_log_entry_api_v1_audit_log_post**
> AuditLog create_audit_log_entry_api_v1_audit_log_post(audit_log_create)

Create Audit Log Entry

### Example

* Bearer Authentication (bearerAuth):

```python
import magneto_api_client
from magneto_api_client.models.audit_log import AuditLog
from magneto_api_client.models.audit_log_create import AuditLogCreate
from magneto_api_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = magneto_api_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: bearerAuth
configuration = magneto_api_client.Configuration(
    access_token = os.environ["BEARER_TOKEN"]
)

# Enter a context with an instance of the API client
async with magneto_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = magneto_api_client.AuditLogApi(api_client)
    audit_log_create = magneto_api_client.AuditLogCreate() # AuditLogCreate | 

    try:
        # Create Audit Log Entry
        api_response = await api_instance.create_audit_log_entry_api_v1_audit_log_post(audit_log_create)
        print("The response of AuditLogApi->create_audit_log_entry_api_v1_audit_log_post:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling AuditLogApi->create_audit_log_entry_api_v1_audit_log_post: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **audit_log_create** | [**AuditLogCreate**](AuditLogCreate.md)|  | 

### Return type

[**AuditLog**](AuditLog.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**201** | Successful Response |  -  |
**422** | Validation Error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_audit_log_api_v1_audit_log_get**
> CursorPageAuditLog get_audit_log_api_v1_audit_log_get(sid=sid, target_id=target_id, target_type=target_type, cursor=cursor, size=size, include_total=include_total)

Get Audit Log

### Example

* Bearer Authentication (bearerAuth):

```python
import magneto_api_client
from magneto_api_client.models.cursor_page_audit_log import CursorPageAuditLog
from magneto_api_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = magneto_api_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: bearerAuth
configuration = magneto_api_client.Configuration(
    access_token = os.environ["BEARER_TOKEN"]
)

# Enter a context with an instance of the API client
async with magneto_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = magneto_api_client.AuditLogApi(api_client)
    sid = 56 # int |  (optional)
    target_id = 'target_id_example' # str |  (optional)
    target_type = 'target_type_example' # str |  (optional)
    cursor = 'cursor_example' # str |  (optional)
    size = 50 # int |  (optional) (default to 50)
    include_total = False # bool |  (optional) (default to False)

    try:
        # Get Audit Log
        api_response = await api_instance.get_audit_log_api_v1_audit_log_get(sid=sid, target_id=target_id, target_type=target_type, cursor=cursor, size=size, include_total=include_total)
        print("The response of AuditLogApi->get_audit_log_api_v1_audit_log_get:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling AuditLogApi->get_audit_log_api_v1_audit_log_get: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sid** | **int**|  | [optional] 
 **target_id** | **str**|  | [optional] 
 **target_type** | **str**|  | [optional] 
 **cursor** | **str**|  | [optional] 
 **size** | **int**|  | [optional] [default to 50]
 **include_total** | **bool**|  | [optional] [default to False]

### Return type

[**CursorPageAuditLog**](CursorPageAuditLog.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful Response |  -  |
**422** | Validation Error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_audit_log_changes_api_v1_audit_log_sid_changes_get**
> object get_audit_log_changes_api_v1_audit_log_sid_changes_get(sid)

Get Audit Log Changes

### Example

* Bearer Authentication (bearerAuth):

```python
import magneto_api_client
from magneto_api_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = magneto_api_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: bearerAuth
configuration = magneto_api_client.Configuration(
    access_token = os.environ["BEARER_TOKEN"]
)

# Enter a context with an instance of the API client
async with magneto_api_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = magneto_api_client.AuditLogApi(api_client)
    sid = 56 # int | 

    try:
        # Get Audit Log Changes
        api_response = await api_instance.get_audit_log_changes_api_v1_audit_log_sid_changes_get(sid)
        print("The response of AuditLogApi->get_audit_log_changes_api_v1_audit_log_sid_changes_get:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling AuditLogApi->get_audit_log_changes_api_v1_audit_log_sid_changes_get: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sid** | **int**|  | 

### Return type

**object**

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful Response |  -  |
**422** | Validation Error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

