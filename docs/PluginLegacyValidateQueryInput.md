# PluginLegacyValidateQueryInput


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**query** | **object** |  | [optional] 
**data_format** | [**QueryDataFormat**](QueryDataFormat.md) |  | 

## Example

```python
from magneto_api_client.models.plugin_legacy_validate_query_input import PluginLegacyValidateQueryInput

# TODO update the JSON string below
json = "{}"
# create an instance of PluginLegacyValidateQueryInput from a JSON string
plugin_legacy_validate_query_input_instance = PluginLegacyValidateQueryInput.from_json(json)
# print the JSON string representation of the object
print(PluginLegacyValidateQueryInput.to_json())

# convert the object into a dict
plugin_legacy_validate_query_input_dict = plugin_legacy_validate_query_input_instance.to_dict()
# create an instance of PluginLegacyValidateQueryInput from a dict
plugin_legacy_validate_query_input_from_dict = PluginLegacyValidateQueryInput.from_dict(plugin_legacy_validate_query_input_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


