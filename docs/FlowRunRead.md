# FlowRunRead

FlowRun Read Model.

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** |  | [optional] 
**flow_id** | **str** |  | 
**flow_type** | [**FlowType**](FlowType.md) |  | 
**args** | **object** |  | [optional] 
**phase** | [**StatusPhase**](StatusPhase.md) |  | [optional] 
**start_time** | **datetime** |  | [optional] 
**end_time** | **datetime** |  | [optional] 
**actions** | [**List[ActionDetails]**](ActionDetails.md) |  | [optional] 
**outputs** | [**Dict[str, RunOutput]**](RunOutput.md) |  | [optional] 
**logs** | [**List[LogEntry]**](LogEntry.md) |  | [optional] 
**tags** | [**List[Tag]**](Tag.md) |  | [optional] 
**created_at** | **datetime** |  | 
**created_by** | **int** |  | [optional] 
**updated_at** | **datetime** |  | 
**updated_by** | **int** |  | [optional] 
**reviewed_by** | **int** |  | [optional] 
**reviewed_at** | **datetime** |  | [optional] 

## Example

```python
from magneto_api_client.models.flow_run_read import FlowRunRead

# TODO update the JSON string below
json = "{}"
# create an instance of FlowRunRead from a JSON string
flow_run_read_instance = FlowRunRead.from_json(json)
# print the JSON string representation of the object
print(FlowRunRead.to_json())

# convert the object into a dict
flow_run_read_dict = flow_run_read_instance.to_dict()
# create an instance of FlowRunRead from a dict
flow_run_read_from_dict = FlowRunRead.from_dict(flow_run_read_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


