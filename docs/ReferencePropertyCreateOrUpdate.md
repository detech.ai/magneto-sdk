# ReferencePropertyCreateOrUpdate

Base class for models that exclude unset fields.  **Note**: This overrides any `exclude_unset` kwarg passed for the model and its nested models.

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**title** | **str** |  | 
**description** | **str** |  | [optional] 
**relation** | **str** |  | 
**var_property** | **str** |  | 

## Example

```python
from magneto_api_client.models.reference_property_create_or_update import ReferencePropertyCreateOrUpdate

# TODO update the JSON string below
json = "{}"
# create an instance of ReferencePropertyCreateOrUpdate from a JSON string
reference_property_create_or_update_instance = ReferencePropertyCreateOrUpdate.from_json(json)
# print the JSON string representation of the object
print(ReferencePropertyCreateOrUpdate.to_json())

# convert the object into a dict
reference_property_create_or_update_dict = reference_property_create_or_update_instance.to_dict()
# create an instance of ReferencePropertyCreateOrUpdate from a dict
reference_property_create_or_update_from_dict = ReferencePropertyCreateOrUpdate.from_dict(reference_property_create_or_update_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


