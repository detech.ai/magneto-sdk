# SuggestionApplyBulk

Suggestion Apply Result Model.

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**suggestion_ids** | **List[str]** |  | 

## Example

```python
from magneto_api_client.models.suggestion_apply_bulk import SuggestionApplyBulk

# TODO update the JSON string below
json = "{}"
# create an instance of SuggestionApplyBulk from a JSON string
suggestion_apply_bulk_instance = SuggestionApplyBulk.from_json(json)
# print the JSON string representation of the object
print(SuggestionApplyBulk.to_json())

# convert the object into a dict
suggestion_apply_bulk_dict = suggestion_apply_bulk_instance.to_dict()
# create an instance of SuggestionApplyBulk from a dict
suggestion_apply_bulk_from_dict = SuggestionApplyBulk.from_dict(suggestion_apply_bulk_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


