# UserLegacyViewConfigRead


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**config** | **object** |  | 

## Example

```python
from magneto_api_client.models.user_legacy_view_config_read import UserLegacyViewConfigRead

# TODO update the JSON string below
json = "{}"
# create an instance of UserLegacyViewConfigRead from a JSON string
user_legacy_view_config_read_instance = UserLegacyViewConfigRead.from_json(json)
# print the JSON string representation of the object
print(UserLegacyViewConfigRead.to_json())

# convert the object into a dict
user_legacy_view_config_read_dict = user_legacy_view_config_read_instance.to_dict()
# create an instance of UserLegacyViewConfigRead from a dict
user_legacy_view_config_read_from_dict = UserLegacyViewConfigRead.from_dict(user_legacy_view_config_read_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


