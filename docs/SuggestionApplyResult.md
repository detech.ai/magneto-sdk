# SuggestionApplyResult

Suggestion Apply Result Model.

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**dry_run** | **bool** |  | [optional] 
**ignored** | **bool** |  | [optional] 
**previous_state** | **object** |  | [optional] 
**new_state** | **object** |  | [optional] 

## Example

```python
from magneto_api_client.models.suggestion_apply_result import SuggestionApplyResult

# TODO update the JSON string below
json = "{}"
# create an instance of SuggestionApplyResult from a JSON string
suggestion_apply_result_instance = SuggestionApplyResult.from_json(json)
# print the JSON string representation of the object
print(SuggestionApplyResult.to_json())

# convert the object into a dict
suggestion_apply_result_dict = suggestion_apply_result_instance.to_dict()
# create an instance of SuggestionApplyResult from a dict
suggestion_apply_result_from_dict = SuggestionApplyResult.from_dict(suggestion_apply_result_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


