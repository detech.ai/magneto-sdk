# FlowRead

Flow Read Model.

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** |  | 
**title** | **str** |  | 
**description** | **str** |  | [optional] 
**icon** | **str** |  | [optional] 
**is_active** | **bool** |  | [optional] [default to True]
**needs_approval** | **bool** |  | [optional] [default to False]
**order** | **int** |  | [optional] [default to 0]
**type** | [**FlowType**](FlowType.md) |  | 
**arguments** | **object** |  | [optional] 
**secrets** | **object** |  | [optional] 
**triggers** | [**List[FlowTrigger]**](FlowTrigger.md) |  | [optional] 
**actions** | [**List[FlowAction]**](FlowAction.md) |  | [optional] 
**outputs** | [**Dict[str, FlowOutput]**](FlowOutput.md) |  | [optional] 
**tags** | [**List[Tag]**](Tag.md) |  | [optional] 
**created_at** | **datetime** |  | 
**created_by** | **int** |  | [optional] 
**updated_at** | **datetime** |  | 
**updated_by** | **int** |  | [optional] 

## Example

```python
from magneto_api_client.models.flow_read import FlowRead

# TODO update the JSON string below
json = "{}"
# create an instance of FlowRead from a JSON string
flow_read_instance = FlowRead.from_json(json)
# print the JSON string representation of the object
print(FlowRead.to_json())

# convert the object into a dict
flow_read_dict = flow_read_instance.to_dict()
# create an instance of FlowRead from a dict
flow_read_from_dict = FlowRead.from_dict(flow_read_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


