# FlowTriggerEventData


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**resource** | [**EventResource**](EventResource.md) |  | 
**action** | [**List[EventAction]**](EventAction.md) |  | 

## Example

```python
from magneto_api_client.models.flow_trigger_event_data import FlowTriggerEventData

# TODO update the JSON string below
json = "{}"
# create an instance of FlowTriggerEventData from a JSON string
flow_trigger_event_data_instance = FlowTriggerEventData.from_json(json)
# print the JSON string representation of the object
print(FlowTriggerEventData.to_json())

# convert the object into a dict
flow_trigger_event_data_dict = flow_trigger_event_data_instance.to_dict()
# create an instance of FlowTriggerEventData from a dict
flow_trigger_event_data_from_dict = FlowTriggerEventData.from_dict(flow_trigger_event_data_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


