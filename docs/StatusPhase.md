# StatusPhase

An enumeration.

## Enum

* `PENDING` (value: `'pending'`)

* `RUNNING` (value: `'running'`)

* `SUCCEEDED` (value: `'succeeded'`)

* `FAILED` (value: `'failed'`)

* `WAITINGAPPROVAL` (value: `'waitingApproval'`)

* `DENIED` (value: `'denied'`)

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


