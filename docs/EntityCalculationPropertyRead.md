# EntityCalculationPropertyRead

Base class for models that exclude unset fields.  **Note**: This overrides any `exclude_unset` kwarg passed for the model and its nested models.

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**timeseries** | **List[List[TimeseriesInnerInner]]** |  | [optional] 
**unit** | **str** |  | [optional] 

## Example

```python
from magneto_api_client.models.entity_calculation_property_read import EntityCalculationPropertyRead

# TODO update the JSON string below
json = "{}"
# create an instance of EntityCalculationPropertyRead from a JSON string
entity_calculation_property_read_instance = EntityCalculationPropertyRead.from_json(json)
# print the JSON string representation of the object
print(EntityCalculationPropertyRead.to_json())

# convert the object into a dict
entity_calculation_property_read_dict = entity_calculation_property_read_instance.to_dict()
# create an instance of EntityCalculationPropertyRead from a dict
entity_calculation_property_read_from_dict = EntityCalculationPropertyRead.from_dict(entity_calculation_property_read_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


