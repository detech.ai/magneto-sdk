# AggregationCalculateBy

An enumeration.

## Enum

* `ENTITIES` (value: `'entities'`)

* `PROPERTIES` (value: `'properties'`)

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


