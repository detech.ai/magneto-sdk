# RunOutput

Base class for models that exclude unset fields.  **Note**: This overrides any `exclude_unset` kwarg passed for the model and its nested models.

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**value** | **object** |  | [optional] 
**error** | **str** |  | [optional] 

## Example

```python
from magneto_api_client.models.run_output import RunOutput

# TODO update the JSON string below
json = "{}"
# create an instance of RunOutput from a JSON string
run_output_instance = RunOutput.from_json(json)
# print the JSON string representation of the object
print(RunOutput.to_json())

# convert the object into a dict
run_output_dict = run_output_instance.to_dict()
# create an instance of RunOutput from a dict
run_output_from_dict = RunOutput.from_dict(run_output_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


