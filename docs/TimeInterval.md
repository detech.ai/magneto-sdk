# TimeInterval

An enumeration.

## Enum

* `ENUM_1H` (value: `'1h'`)

* `ENUM_1D` (value: `'1d'`)

* `ENUM_1W` (value: `'1w'`)

* `ENUM_1M` (value: `'1m'`)

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


