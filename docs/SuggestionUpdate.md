# SuggestionUpdate

Suggestion Update Model.

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** |  | [optional] 
**title** | **str** |  | 
**description** | **str** |  | [optional] 
**origin_type** | [**OriginType**](OriginType.md) |  | 
**origin_id** | **str** |  | 
**source_type** | [**SourceType**](SourceType.md) |  | 
**source_id** | **str** |  | 
**source_context** | **object** |  | [optional] 
**target_type** | [**TargetType**](TargetType.md) |  | 
**target_id** | **str** |  | 
**target_context** | **object** |  | [optional] 
**target_id_override** | **str** |  | [optional] 
**data** | **object** |  | [optional] 
**ignored** | **bool** |  | [optional] 
**auto_apply** | **bool** |  | [optional] 
**last_applied_at** | **datetime** |  | [optional] 

## Example

```python
from magneto_api_client.models.suggestion_update import SuggestionUpdate

# TODO update the JSON string below
json = "{}"
# create an instance of SuggestionUpdate from a JSON string
suggestion_update_instance = SuggestionUpdate.from_json(json)
# print the JSON string representation of the object
print(SuggestionUpdate.to_json())

# convert the object into a dict
suggestion_update_dict = suggestion_update_instance.to_dict()
# create an instance of SuggestionUpdate from a dict
suggestion_update_from_dict = SuggestionUpdate.from_dict(suggestion_update_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


