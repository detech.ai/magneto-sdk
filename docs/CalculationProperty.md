# CalculationProperty

Base class for models that exclude unset fields.  **Note**: This overrides any `exclude_unset` kwarg passed for the model and its nested models.

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**title** | **str** |  | [optional] 
**description** | **str** |  | [optional] 
**icon** | **str** |  | [optional] 
**unit** | **str** |  | [optional] 
**output_type** | [**CalculationPropertyOutputType**](CalculationPropertyOutputType.md) |  | 
**method** | [**CalculationPropertyMethod**](CalculationPropertyMethod.md) |  | 
**aggregation** | [**Aggregation**](Aggregation.md) |  | [optional] 
**sql** | **str** |  | [optional] 

## Example

```python
from magneto_api_client.models.calculation_property import CalculationProperty

# TODO update the JSON string below
json = "{}"
# create an instance of CalculationProperty from a JSON string
calculation_property_instance = CalculationProperty.from_json(json)
# print the JSON string representation of the object
print(CalculationProperty.to_json())

# convert the object into a dict
calculation_property_dict = calculation_property_instance.to_dict()
# create an instance of CalculationProperty from a dict
calculation_property_from_dict = CalculationProperty.from_dict(calculation_property_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


