# coding: utf-8

"""
    Magneto API

    Magneto API is the RESTful API for the Rely.io platform.

    The version of the OpenAPI document: v0.0.1
    Contact: contact@rely.io
    Generated by OpenAPI Generator (https://openapi-generator.tech)

    Do not edit the class manually.
"""  # noqa: E501


import unittest

from magneto_api_client.models.calculation_property_method import CalculationPropertyMethod

class TestCalculationPropertyMethod(unittest.TestCase):
    """CalculationPropertyMethod unit test stubs"""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def testCalculationPropertyMethod(self):
        """Test CalculationPropertyMethod"""
        # inst = CalculationPropertyMethod()

if __name__ == '__main__':
    unittest.main()
