# coding: utf-8

"""
    Magneto API

    Magneto API is the RESTful API for the Rely.io platform.

    The version of the OpenAPI document: v0.0.1
    Contact: contact@rely.io
    Generated by OpenAPI Generator (https://openapi-generator.tech)

    Do not edit the class manually.
"""  # noqa: E501


import unittest

from magneto_api_client.api.audit_log_api import AuditLogApi


class TestAuditLogApi(unittest.IsolatedAsyncioTestCase):
    """AuditLogApi unit test stubs"""

    async def asyncSetUp(self) -> None:
        self.api = AuditLogApi()

    async def asyncTearDown(self) -> None:
        await self.api.api_client.close()

    async def test_create_audit_log_entry_api_v1_audit_log_post(self) -> None:
        """Test case for create_audit_log_entry_api_v1_audit_log_post

        Create Audit Log Entry
        """
        pass

    async def test_get_audit_log_api_v1_audit_log_get(self) -> None:
        """Test case for get_audit_log_api_v1_audit_log_get

        Get Audit Log
        """
        pass

    async def test_get_audit_log_changes_api_v1_audit_log_sid_changes_get(self) -> None:
        """Test case for get_audit_log_changes_api_v1_audit_log_sid_changes_get

        Get Audit Log Changes
        """
        pass


if __name__ == '__main__':
    unittest.main()
