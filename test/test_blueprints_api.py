# coding: utf-8

"""
    Magneto API

    Magneto API is the RESTful API for the Rely.io platform.

    The version of the OpenAPI document: v0.0.1
    Contact: contact@rely.io
    Generated by OpenAPI Generator (https://openapi-generator.tech)

    Do not edit the class manually.
"""  # noqa: E501


import unittest

from magneto_api_client.api.blueprints_api import BlueprintsApi


class TestBlueprintsApi(unittest.IsolatedAsyncioTestCase):
    """BlueprintsApi unit test stubs"""

    async def asyncSetUp(self) -> None:
        self.api = BlueprintsApi()

    async def asyncTearDown(self) -> None:
        await self.api.api_client.close()

    async def test_create_blueprint_api_v1_blueprints_post(self) -> None:
        """Test case for create_blueprint_api_v1_blueprints_post

        Create Blueprint
        """
        pass

    async def test_create_blueprint_calculation_properties_api_v1_blueprints_id_calculation_properties_post(self) -> None:
        """Test case for create_blueprint_calculation_properties_api_v1_blueprints_id_calculation_properties_post

        Create Blueprint Calculation Properties
        """
        pass

    async def test_create_blueprint_reference_properties_api_v1_blueprints_id_reference_properties_post(self) -> None:
        """Test case for create_blueprint_reference_properties_api_v1_blueprints_id_reference_properties_post

        Create Blueprint Reference Properties
        """
        pass

    async def test_create_blueprint_relations_api_v1_blueprints_id_relations_post(self) -> None:
        """Test case for create_blueprint_relations_api_v1_blueprints_id_relations_post

        Create Blueprint Relations
        """
        pass

    async def test_create_blueprint_schema_properties_api_v1_blueprints_id_schema_properties_post(self) -> None:
        """Test case for create_blueprint_schema_properties_api_v1_blueprints_id_schema_properties_post

        Create Blueprint Schema Properties
        """
        pass

    async def test_create_or_update_blueprint_api_v1_blueprints_put(self) -> None:
        """Test case for create_or_update_blueprint_api_v1_blueprints_put

        Create Or Update Blueprint
        """
        pass

    async def test_create_or_update_blueprints_bulk_api_v1_blueprints_bulk_put(self) -> None:
        """Test case for create_or_update_blueprints_bulk_api_v1_blueprints_bulk_put

        Create Or Update Blueprints Bulk
        """
        pass

    async def test_delete_blueprint_api_v1_blueprints_id_delete(self) -> None:
        """Test case for delete_blueprint_api_v1_blueprints_id_delete

        Delete Blueprint
        """
        pass

    async def test_delete_blueprint_calculation_property_api_v1_blueprints_id_calculation_properties_calculation_property_key_delete(self) -> None:
        """Test case for delete_blueprint_calculation_property_api_v1_blueprints_id_calculation_properties_calculation_property_key_delete

        Delete Blueprint Calculation Property
        """
        pass

    async def test_delete_blueprint_reference_property_api_v1_blueprints_id_reference_properties_ref_property_key_delete(self) -> None:
        """Test case for delete_blueprint_reference_property_api_v1_blueprints_id_reference_properties_ref_property_key_delete

        Delete Blueprint Reference Property
        """
        pass

    async def test_delete_blueprint_relations_api_v1_blueprints_id_relations_relation_key_delete(self) -> None:
        """Test case for delete_blueprint_relations_api_v1_blueprints_id_relations_relation_key_delete

        Delete Blueprint Relations
        """
        pass

    async def test_delete_blueprint_schema_properties_api_v1_blueprints_id_schema_properties_schema_property_key_delete(self) -> None:
        """Test case for delete_blueprint_schema_properties_api_v1_blueprints_id_schema_properties_schema_property_key_delete

        Delete Blueprint Schema Properties
        """
        pass

    async def test_get_blueprint_api_v1_blueprints_id_get(self) -> None:
        """Test case for get_blueprint_api_v1_blueprints_id_get

        Get Blueprint
        """
        pass

    async def test_get_blueprint_calculation_properties_api_v1_blueprints_id_calculation_properties_get(self) -> None:
        """Test case for get_blueprint_calculation_properties_api_v1_blueprints_id_calculation_properties_get

        Get Blueprint Calculation Properties
        """
        pass

    async def test_get_blueprint_reference_properties_api_v1_blueprints_id_reference_properties_get(self) -> None:
        """Test case for get_blueprint_reference_properties_api_v1_blueprints_id_reference_properties_get

        Get Blueprint Reference Properties
        """
        pass

    async def test_get_blueprint_relations_api_v1_blueprints_id_relations_get(self) -> None:
        """Test case for get_blueprint_relations_api_v1_blueprints_id_relations_get

        Get Blueprint Relations
        """
        pass

    async def test_get_blueprint_schema_properties_api_v1_blueprints_id_schema_properties_get(self) -> None:
        """Test case for get_blueprint_schema_properties_api_v1_blueprints_id_schema_properties_get

        Get Blueprint Schema Properties
        """
        pass

    async def test_get_blueprints_api_v1_blueprints_get(self) -> None:
        """Test case for get_blueprints_api_v1_blueprints_get

        Get Blueprints
        """
        pass

    async def test_update_blueprint_api_v1_blueprints_id_put(self) -> None:
        """Test case for update_blueprint_api_v1_blueprints_id_put

        Update Blueprint
        """
        pass

    async def test_update_blueprint_calculation_properties_api_v1_blueprints_id_calculation_properties_put(self) -> None:
        """Test case for update_blueprint_calculation_properties_api_v1_blueprints_id_calculation_properties_put

        Update Blueprint Calculation Properties
        """
        pass

    async def test_update_blueprint_reference_properties_api_v1_blueprints_id_reference_properties_put(self) -> None:
        """Test case for update_blueprint_reference_properties_api_v1_blueprints_id_reference_properties_put

        Update Blueprint Reference Properties
        """
        pass

    async def test_update_blueprint_relations_api_v1_blueprints_id_relations_put(self) -> None:
        """Test case for update_blueprint_relations_api_v1_blueprints_id_relations_put

        Update Blueprint Relations
        """
        pass

    async def test_update_blueprint_schema_properties_api_v1_blueprints_id_schema_properties_put(self) -> None:
        """Test case for update_blueprint_schema_properties_api_v1_blueprints_id_schema_properties_put

        Update Blueprint Schema Properties
        """
        pass


if __name__ == '__main__':
    unittest.main()
