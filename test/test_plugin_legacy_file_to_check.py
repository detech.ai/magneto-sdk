# coding: utf-8

"""
    Magneto API

    Magneto API is the RESTful API for the Rely.io platform.

    The version of the OpenAPI document: v0.0.1
    Contact: contact@rely.io
    Generated by OpenAPI Generator (https://openapi-generator.tech)

    Do not edit the class manually.
"""  # noqa: E501


import unittest

from magneto_api_client.models.plugin_legacy_file_to_check import PluginLegacyFileToCheck

class TestPluginLegacyFileToCheck(unittest.TestCase):
    """PluginLegacyFileToCheck unit test stubs"""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def make_instance(self, include_optional) -> PluginLegacyFileToCheck:
        """Test PluginLegacyFileToCheck
            include_optional is a boolean, when False only required
            params are included, when True both required and
            optional params are included """
        # uncomment below to create an instance of `PluginLegacyFileToCheck`
        """
        model = PluginLegacyFileToCheck()
        if include_optional:
            return PluginLegacyFileToCheck(
                path = '',
                destination = '',
                regex = ''
            )
        else:
            return PluginLegacyFileToCheck(
                path = '',
                destination = '',
        )
        """

    def testPluginLegacyFileToCheck(self):
        """Test PluginLegacyFileToCheck"""
        # inst_req_only = self.make_instance(include_optional=False)
        # inst_req_and_optional = self.make_instance(include_optional=True)

if __name__ == '__main__':
    unittest.main()
