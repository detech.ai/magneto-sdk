# coding: utf-8

"""
    Magneto API

    Magneto API is the RESTful API for the Rely.io platform.

    The version of the OpenAPI document: v0.0.1
    Contact: contact@rely.io
    Generated by OpenAPI Generator (https://openapi-generator.tech)

    Do not edit the class manually.
"""  # noqa: E501


import unittest

from magneto_api_client.api.time_series_api import TimeSeriesApi


class TestTimeSeriesApi(unittest.IsolatedAsyncioTestCase):
    """TimeSeriesApi unit test stubs"""

    async def asyncSetUp(self) -> None:
        self.api = TimeSeriesApi()

    async def asyncTearDown(self) -> None:
        await self.api.api_client.close()

    async def test_timeseries_query_api_v1_timeseries_query_post(self) -> None:
        """Test case for timeseries_query_api_v1_timeseries_query_post

        Timeseries Query
        """
        pass

    async def test_timeseries_write_api_v1_timeseries_write_post(self) -> None:
        """Test case for timeseries_write_api_v1_timeseries_write_post

        Timeseries Write
        """
        pass


if __name__ == '__main__':
    unittest.main()
