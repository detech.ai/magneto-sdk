# coding: utf-8

"""
    Magneto API

    Magneto API is the RESTful API for the Rely.io platform.

    The version of the OpenAPI document: v0.0.1
    Contact: contact@rely.io
    Generated by OpenAPI Generator (https://openapi-generator.tech)

    Do not edit the class manually.
"""  # noqa: E501


import unittest

from magneto_api_client.api.agents_api import AgentsApi


class TestAgentsApi(unittest.IsolatedAsyncioTestCase):
    """AgentsApi unit test stubs"""

    async def asyncSetUp(self) -> None:
        self.api = AgentsApi()

    async def asyncTearDown(self) -> None:
        await self.api.api_client.close()

    async def test_get_agents_api_v1_agents_get(self) -> None:
        """Test case for get_agents_api_v1_agents_get

        Get Agents
        """
        pass

    async def test_register_agent_api_v1_agents_post(self) -> None:
        """Test case for register_agent_api_v1_agents_post

        Register Agent
        """
        pass

    async def test_update_agent_heartbeat_api_v1_agents_id_heartbeat_post(self) -> None:
        """Test case for update_agent_heartbeat_api_v1_agents_id_heartbeat_post

        Update Agent Heartbeat
        """
        pass


if __name__ == '__main__':
    unittest.main()
