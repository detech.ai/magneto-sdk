# coding: utf-8

"""
    Magneto API

    Magneto API is the RESTful API for the Rely.io platform.

    The version of the OpenAPI document: v0.0.1
    Contact: contact@rely.io
    Generated by OpenAPI Generator (https://openapi-generator.tech)

    Do not edit the class manually.
"""  # noqa: E501


import unittest

from magneto_api_client.models.entity_scorecards_insights import EntityScorecardsInsights

class TestEntityScorecardsInsights(unittest.TestCase):
    """EntityScorecardsInsights unit test stubs"""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def make_instance(self, include_optional) -> EntityScorecardsInsights:
        """Test EntityScorecardsInsights
            include_optional is a boolean, when False only required
            params are included, when True both required and
            optional params are included """
        # uncomment below to create an instance of `EntityScorecardsInsights`
        """
        model = EntityScorecardsInsights()
        if include_optional:
            return EntityScorecardsInsights(
                scorecard_count = 56,
                scorecard_ids = [
                    ''
                    ],
                median_rank = '',
                gold_count = 56,
                silver_count = 56,
                bronze_count = 56,
                no_rank_count = 56,
                no_data_count = 56
            )
        else:
            return EntityScorecardsInsights(
                scorecard_count = 56,
                scorecard_ids = [
                    ''
                    ],
                median_rank = '',
                gold_count = 56,
                silver_count = 56,
                bronze_count = 56,
                no_rank_count = 56,
                no_data_count = 56,
        )
        """

    def testEntityScorecardsInsights(self):
        """Test EntityScorecardsInsights"""
        # inst_req_only = self.make_instance(include_optional=False)
        # inst_req_and_optional = self.make_instance(include_optional=True)

if __name__ == '__main__':
    unittest.main()
